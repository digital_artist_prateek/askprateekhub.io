---
title: UX-Theme
author: Ask Prateek
layout: page
sfw_comment_form_password:
  - z39ROI8tYLfO
  - z39ROI8tYLfO
  - z39ROI8tYLfO
  - z39ROI8tYLfO
  - z39ROI8tYLfO
  - z39ROI8tYLfO
  - z39ROI8tYLfO
  - z39ROI8tYLfO
  - z39ROI8tYLfO
  - z39ROI8tYLfO
  - z39ROI8tYLfO
  - z39ROI8tYLfO
  - z39ROI8tYLfO
  - z39ROI8tYLfO
  - z39ROI8tYLfO
  - z39ROI8tYLfO
---
> **Update: **Now these link don&#8217;t work so please use other applications to patch Uxtheme.dll . we are posting links for those Application:
> 
>   * [**UxStyle: A small utility to patch uxtheme.dll to use 3rd party Themes**.][1]
>   * [**Universal Theme Patcher: Yet another tool to patch system files to use themes in Windows XP, Vista and 7.**][2]

The theme manager (implemented in Uxtheme.dll), introduced back in [Windows Codename: Whistler][3]days, is responsible for drawing non-client parts of every window displayed within Microsoft Windows according to the styles defined in your active theme.

To keep the Microsoft Windows branding intact, Microsoft added digital signatures to their themes, of which are validated at theme installation and application (i.e. at boot). Due to this limitation, only a handful of themes can officially be used within Microsoft Windows (i.e. [Zune theme][4]).

Since the introduction of digital signatures, however, I have removed the aforementioned checks to provide the Microsoft Windows enthusiast community a **free** and **easy** way to enable third-party customization of the entire Microsoft Windows user interface.

This repository represents every patch I have made to date, minus a few that may have gotten lost in transit.

<a><br /> </a>

<div>
  <ul>
    <li>
      Windows 7 <ul>
        <li>
          <a href="http://www.withinwindows.com/uxthemes/Windows%207/6.1.6519.1_XX.rar">6.1.6519.1_XX.rar</a>
        </li>
        <li>
          <a href="http://www.withinwindows.com/uxthemes/Windows%207/6.1.6519.1_XX_AMD64.rar">6.1.6519.1_XX_AMD64.rar</a>
        </li>
        <li>
          <a href="http://www.withinwindows.com/uxthemes/Windows%207/6.1.7000.0_XX.rar">6.1.7000.0_XX.rar</a>
        </li>
        <li>
          <a href="http://www.withinwindows.com/uxthemes/Windows%207/6.1.7000.0_XX_AMD64.rar">6.1.7000.0_XX_AMD64.rar</a>
        </li>
        <li>
          <a href="http://www.withinwindows.com/uxthemes/Windows%207/6.1.7022.0_XX.rar">6.1.7022.0_XX.rar</a>
        </li>
        <li>
          <a href="http://www.withinwindows.com/uxthemes/Windows%207/6.1.7057.0_XX.rar">6.1.7057.0_XX.rar</a>
        </li>
        <li>
          <a href="http://www.withinwindows.com/uxthemes/Windows%207/6.1.7068.0_XX.rar">6.1.7068.0_XX.rar</a>
        </li>
        <li>
          <a href="http://www.withinwindows.com/uxthemes/Windows%207/6.1.7068.0_XX_AMD64.rar">6.1.7068.0_XX_AMD64.rar</a>
        </li>
        <li>
          <a href="http://www.withinwindows.com/uxthemes/Windows%207/6.1.7100.0_XX.rar">6.1.7100.0_XX.rar</a>
        </li>
        <li>
          <a href="http://www.withinwindows.com/uxthemes/Windows%207/6.1.7100.0_XX_AMD64.rar">6.1.7100.0_XX_AMD64.rar</a>
        </li>
        <li>
          <a href="http://www.withinwindows.com/uxthemes/Windows%207/6.1.7127.0_AMD64_XX.rar">6.1.7127.0_AMD64_XX.rar</a>
        </li>
        <li>
          <a href="http://www.withinwindows.com/uxthemes/Windows%207/6.1.7127.0_XX.rar">6.1.7127.0_XX.rar</a>
        </li>
      </ul>
    </li>
    
    <li>
      Windows Server 2003 <ul>
        <li>
          SP0 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP0/6.0.3790.0_EN.rar">6.0.3790.0_EN.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP0 Beta <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP0%20Beta/5.2.3678_EN.rar">5.2.3678_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP0%20Beta/5.2.3714_EN.rar">5.2.3714_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP0%20Beta/5.2.3716_EN.rar">5.2.3716_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP0%20Beta/5.2.3718_EN.rar">5.2.3718_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP0%20Beta/5.2.3742_EN.rar">5.2.3742_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP0%20Beta/5.2.3757_EN.rar">5.2.3757_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP0%20Beta/5.2.3763_EN.rar">5.2.3763_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP0%20Beta/5.2.3765_EN.rar">5.2.3765_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP0%20Beta/5.2.3771_EN.rar">5.2.3771_EN.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP1 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1/6.0.3790.1812_EN.rar">6.0.3790.1812_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1/6.0.3790.1830_DE.rar">6.0.3790.1830_DE.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1/6.0.3790.1830_EN.rar">6.0.3790.1830_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1/6.0.3790.1830_EN_AMD64.rar">6.0.3790.1830_EN_AMD64.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1/6.0.3790.2615_EN_AMD64.rar">6.0.3790.2615_EN_AMD64.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP1 Beta <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20Beta/6.0.3790.1069_EN.rar">6.0.3790.1069_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20Beta/6.0.3790.1137_EN.rar">6.0.3790.1137_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20Beta/6.0.3790.1159_EN.rar">6.0.3790.1159_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20Beta/6.0.3790.1159_EN_AMD64.rar">6.0.3790.1159_EN_AMD64.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20Beta/6.0.3790.1184_EN.rar">6.0.3790.1184_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20Beta/6.0.3790.1218_DE.rar">6.0.3790.1218_DE.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20Beta/6.0.3790.1218_EN.rar">6.0.3790.1218_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20Beta/6.0.3790.1247_EN.rar">6.0.3790.1247_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20Beta/6.0.3790.1260_EN.rar">6.0.3790.1260_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20Beta/6.00.3790.1069_EN_AMD64.rar">6.00.3790.1069_EN_AMD64.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP1 RC1 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20RC1/6.0.3790.1289_AMD64_EN.rar">6.0.3790.1289_AMD64_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20RC1/6.0.3790.1289_DE.rar">6.0.3790.1289_DE.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20RC1/6.0.3790.1289_EN.rar">6.0.3790.1289_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20RC1/6.0.3790.1421_EN.rar">6.0.3790.1421_EN.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP1 RC2 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20RC2/6.0.3790.1433_AMD64_EN.rar">6.0.3790.1433_AMD64_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20RC2/6.0.3790.1433_DE.rar">6.0.3790.1433_DE.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP1%20RC2/6.0.3790.1433_EN.rar">6.0.3790.1433_EN.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP2 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP2/6.0.3790.3959_DE.rar">6.0.3790.3959_DE.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP2/6.0.3790.3959_EN.rar">6.0.3790.3959_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP2/6.0.3790.3959_EN_AMD64.rar">6.0.3790.3959_EN_AMD64.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP2 Beta <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP2%20Beta/6.0.3790.2725_EN.rar">6.0.3790.2725_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP2%20Beta/6.0.3790.2786_EN_AMD64.rar">6.0.3790.2786_EN_AMD64.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP2 RC1 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP2%20RC1/6.0.3790.2825_EN.rar">6.0.3790.2825_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202003/SP2%20RC1/6.0.3790.2825_EN_AMD64.rar">6.0.3790.2825_EN_AMD64.rar</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    
    <li>
      Windows Server 2008 <ul>
        <li>
          SP1 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202008/SP1/6.0.6001.18000_AMD64_XX.rar">6.0.6001.18000_AMD64_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202008/SP1/6.0.6001.18000_XX.rar">6.0.6001.18000_XX.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP1 Beta <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202008/SP1%20Beta/6.0.6001.16510_XX.rar">6.0.6001.16510_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Server%202008/SP1%20Beta/6.0.6001.16606_XX.rar">6.0.6001.16606_XX.rar</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    
    <li>
      Windows Vista <ul>
        <li>
          SP0 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP0/6.0.6000.16386_XX.rar">6.0.6000.16386_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP0/6.0.6000.16386_XX_AMD64.rar">6.0.6000.16386_XX_AMD64.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP0 Beta <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP0%20Beta/6.0.5308.17_XX.rar">6.0.5308.17_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP0%20Beta/6.0.5342.2_XX.rar">6.0.5342.2_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP0%20Beta/6.0.5365.8_XX.rar">6.0.5365.8_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP0%20Beta/6.0.5381.1_XX.rar">6.0.5381.1_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP0%20Beta/6.0.5384.4_XX.rar">6.0.5384.4_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP0%20Beta/6.0.5384.4_XX_AMD64.rar">6.0.5384.4_XX_AMD64.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP0%20Beta/6.0.5456.5_XX.rar">6.0.5456.5_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP0%20Beta/6.0.5744.16384_XX.rar">6.0.5744.16384_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP0%20Beta/7.0.5259.0_XX.rar">7.0.5259.0_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP0%20Beta/7.0.5259.3_XX.rar">7.0.5259.3_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP0%20Beta/7.0.5270.9_XX.rar">7.0.5270.9_XX.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP1 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1/6.0.6001.18000_AMD64_XX.rar">6.0.6001.18000_AMD64_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1/6.0.6001.18000_XX.rar">6.0.6001.18000_XX.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP1 Beta <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1%20Beta/6.0.6001.16549_XX.rar">6.0.6001.16549_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1%20Beta/6.0.6001.16549_XX_AMD64.rar">6.0.6001.16549_XX_AMD64.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1%20Beta/6.0.6001.16633_XX.rar">6.0.6001.16633_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1%20Beta/6.0.6001.16655_XX_AMD64.rar">6.0.6001.16655_XX_AMD64.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1%20Beta/6.0.6001.16659_XX.rar">6.0.6001.16659_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1%20Beta/6.0.6001.16659_XX_AMD64.rar">6.0.6001.16659_XX_AMD64.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP1 RC1 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1%20RC1/6.0.6001.17036_AMD64_XX.rar">6.0.6001.17036_AMD64_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1%20RC1/6.0.6001.17036_XX.rar">6.0.6001.17036_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1%20RC1/6.0.6001.17042_AMD64_XX.rar">6.0.6001.17042_AMD64_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1%20RC1/6.0.6001.17042_XX.rar">6.0.6001.17042_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1%20RC1/6.0.6001.17052_AMD64_XX.rar">6.0.6001.17052_AMD64_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1%20RC1/6.0.6001.17052_XX.rar">6.0.6001.17052_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1%20RC1/6.0.6001.17128_AMD64_XX.rar">6.0.6001.17128_AMD64_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1%20RC1/6.0.6001.17128_XX.rar">6.0.6001.17128_XX.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP2 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP2/6.0.6002.18005_XX.rar">6.0.6002.18005_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP2/6.0.6002.18005_XX_AMD64.rar">6.0.6002.18005_XX_AMD64.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP2 Beta <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP2%20Beta/6.0.6002.16489_XX.rar">6.0.6002.16489_XX.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP2%20Beta/6.0.6002.16489_XX_AMD64.rar">6.0.6002.16489_XX_AMD64.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20Vista/SP2%20Beta/6.0.6002.16497_XX_AMD64.rar">6.0.6002.16497_XX_AMD64.rar</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
    
    <li>
      Windows XP <ul>
        <li>
          SP0 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP0/6.0.3790.1260_EN_AMD64.rar">6.0.3790.1260_EN_AMD64.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP0/6.0.3790.1289_EN_AMD64.rar">6.0.3790.1289_EN_AMD64.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP0/6.0.3790.1421_EN_AMD64.rar">6.0.3790.1421_EN_AMD64.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP0/6.0.3790.1830_EN_AMD64.rar">6.0.3790.1830_EN_AMD64.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP0/6.0.3790.2615_EN_AMD64.rar">6.0.3790.2615_EN_AMD64.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP1 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP1/6.0.2600.0_TAI.rar">6.0.2600.0_TAI.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP1/6.0.2800.1106_EN.rar">6.0.2800.1106_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP1/6.0.2800.1106_IT.rar">6.0.2800.1106_IT.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP2 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2/6.0.2900.2180-1_PRC.rar">6.0.2900.2180-1_PRC.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2/6.0.2900.2180_DAN.rar">6.0.2900.2180_DAN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2/6.0.2900.2180_DE.rar">6.0.2900.2180_DE.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2/6.0.2900.2180_EN.rar">6.0.2900.2180_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2/6.0.2900.2180_FR.rar">6.0.2900.2180_FR.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2/6.0.2900.2180_KOR.rar">6.0.2900.2180_KOR.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2/6.0.2900.2180_NL.rar">6.0.2900.2180_NL.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2/6.0.2900.2180_POL.rar">6.0.2900.2180_POL.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2/6.0.2900.2180_PRC.rar">6.0.2900.2180_PRC.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2/6.0.2900.2180_SVE.rar">6.0.2900.2180_SVE.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP2 Alpha <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20Alpha/6.0.2800.1154_EN.rar">6.0.2800.1154_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20Alpha/6.0.2800.1157_EN.rar">6.0.2800.1157_EN.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP2 Beta <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20Beta/6.0.2900.2055_EN.rar">6.0.2900.2055_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20Beta/6.0.2900.2082_EN.rar">6.0.2900.2082_EN.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP2 RC1 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC1/6.0.2900.2096_EN.rar">6.0.2900.2096_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC1/6.0.2900.2108_EN.rar">6.0.2900.2108_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC1/6.0.2900.2111_EN.rar">6.0.2900.2111_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC1/6.0.2900.2120_EN.rar">6.0.2900.2120_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC1/6.0.2900.2126_EN.rar">6.0.2900.2126_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC1/6.0.2900.2133_FR.rar">6.0.2900.2133_FR.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC1/6.0.2900.2135_EN.rar">6.0.2900.2135_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC1/6.0.2900.2135_PRC.rar">6.0.2900.2135_PRC.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC1/6.0.2900.2138_EN.rar">6.0.2900.2138_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC1/6.0.2900.2142_EN.rar">6.0.2900.2142_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC1/6.0.2900.2144_EN.rar">6.0.2900.2144_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC1/6.0.2900.2144_PRC.rar">6.0.2900.2144_PRC.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP2 RC2 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC2/6.0.2900.2149_DE.rar">6.0.2900.2149_DE.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC2/6.0.2900.2149_EN.rar">6.0.2900.2149_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC2/6.0.2900.2149_FR.rar">6.0.2900.2149_FR.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC2/6.0.2900.2149_PRC.rar">6.0.2900.2149_PRC.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC2/6.0.2900.2160_FR.rar">6.0.2900.2160_FR.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC2/6.0.2900.2162_DE.rar">6.0.2900.2162_DE.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC2/6.0.2900.2162_EN.rar">6.0.2900.2162_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC2/6.0.2900.2163_FR.rar">6.0.2900.2163_FR.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC2/6.0.2900.2178_PRC.rar">6.0.2900.2178_PRC.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP2%20RC2/6.0.2900.2179_EN.rar">6.0.2900.2179_EN.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP3 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3/6.0.2900.5512_DE.rar">6.0.2900.5512_DE.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3/6.0.2900.5512_EN.rar">6.0.2900.5512_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3/6.0.2900.5512_ES.rar">6.0.2900.5512_ES.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP3 Alpha <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3%20Alpha/6.0.2900.2523_EN.rar">6.0.2900.2523_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3%20Alpha/6.0.2900.2845_EN.rar">6.0.2900.2845_EN.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP3 Beta <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3%20Beta/6.0.2900.3180_EN.rar">6.0.2900.3180_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3%20Beta/6.0.2900.3205_DE.rar">6.0.2900.3205_DE.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3%20Beta/6.0.2900.3205_EN.rar">6.0.2900.3205_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3%20Beta/6.0.2900.3244_EN.rar">6.0.2900.3244_EN.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP3 RC1 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3%20RC1/6.0.2900.3264_EN.rar">6.0.2900.3264_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3%20RC1/6.0.2900.3282_EN.rar">6.0.2900.3282_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3%20RC1/6.0.2900.3300_DE.rar">6.0.2900.3300_DE.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3%20RC1/6.0.2900.3300_EN.rar">6.0.2900.3300_EN.rar</a>
            </li>
          </ul>
        </li>
        
        <li>
          SP3 RC2 <ul>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3%20RC2/6.0.2900.3311_EN.rar">6.0.2900.3311_EN.rar</a>
            </li>
            <li>
              <a href="http://www.withinwindows.com/uxthemes/Windows%20XP/SP3%20RC2/6.0.2900.5508_EN.rar">6.0.2900.5508_EN.rar</a>
            </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</div>

<div>
  Source : WithinWindows.com
</div>

 [1]: http://www.askprateek.tk/2011/10/04/uxstyle-a-small-utility-to-patch-uxtheme-dll-to-use-3rd-party-themes/ "UxStyle: A small utility to patch uxtheme.dll to use 3rd party Themes."
 [2]: http://www.askprateek.tk/2011/10/04/universal-theme-patcher-yet-another-tool-to-patch-system-files-to-use-themes-in-windows-xp-vista-and-7/ "Universal Theme Patcher: Yet another tool to patch system files to use themes in Windows XP, Vista and 7."
 [3]: http://www.winsupersite.com/faq/whistler.asp
 [4]: http://go.microsoft.com/fwlink/?LinkID=75078