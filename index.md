---
title: Home
author: Ask Prateek
layout: page
sfw_comment_form_password:
  - OncLINq4htsE
  - OncLINq4htsE
  - OncLINq4htsE
  - OncLINq4htsE
socialize_text:
  - |
    <div id="presetListDiv">This article was posted by <a href="http://www.askpk123.tk">Prateek Kumar</a> in following section:  [post_list name="categories"]</div>
    <div>

    If you enjoyed this article, then <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Like us on Facebook</a> and <a title="Follow us on Twitter" href="http://twitter.com/AskPK123" target="_blank">Follow us on Twitter</a> so that you won't miss any of the great Article. Also you can checkout our <a title="Our DeviantART Gallery" href="http://prateek-kumar.deviantart.com/" target="_blank">DeviantART Gallery</a> and browse for some Art Work by Me.

    Thanks for Reading and It is requested to leave a comment if you want to Discuss something or simply say Thanks

    </div>
  - |
    <div id="presetListDiv">This article was posted by <a href="http://www.askpk123.tk">Prateek Kumar</a> in following section:  [post_list name="categories"]</div>
    <div>

    If you enjoyed this article, then <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Like us on Facebook</a> and <a title="Follow us on Twitter" href="http://twitter.com/AskPK123" target="_blank">Follow us on Twitter</a> so that you won't miss any of the great Article. Also you can checkout our <a title="Our DeviantART Gallery" href="http://prateek-kumar.deviantart.com/" target="_blank">DeviantART Gallery</a> and browse for some Art Work by Me.

    Thanks for Reading and It is requested to leave a comment if you want to Discuss something or simply say Thanks

    </div>
socialize:
  -
  -
---
## Welcome to The Windows Explorer

<img class="alignright" title="The Windows Explorer" alt="" src="https://fbcdn-profile-a.akamaihd.net/hprofile-ak-snc6/188050_115016338558605_4342674_n.jpg" width="189" height="222" />

Welcome to AskPrateek.tk. A technology blog on blogging which focus on [Windows XP][1], [Vista][2], [Se7en][3]  tutorials, [themes][4], [Games Tools][5], some [cracks][6], [E-Books][7] etc.

Below we are featuring our Most Popular Articles on Windows 8, you can check them out to see the new and awesome features provided by Microsoft . Then there is a list of latest articles of our blog followed by Popular Articles.

It is recommended to Like our <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Facebook Page</a> and follow our <a title="Follow us on Twitter" href="http://www.twitter.com/askpk123" target="_blank">Twitter channel</a> so that you don&#8217;t miss any great Article.

&nbsp;

## Featured on The Windows Explorer

Here are the articles which covers each and every detail you want about Windows 8. Just click on the image to read the article:

[<img class="wp-image-2045 alignnone" title="[Windows 8 Review] All about Mircosoft new Operating System Windows 8." alt="[Windows 8 Review] All about Mircosoft new Operating System Windows 8." src="http://www.askprateek.tk/wp-content/uploads/2012/08/Windows-8-Review.jpg" width="297" height="198" />][8]      [<img class="wp-image-2046 alignnone" title="[Windows 8 Features] All about new additions in Windows 8" alt="" src="http://www.askprateek.tk/wp-content/uploads/2012/08/Windows-8-Features.jpg" width="297" height="198" />][9]

<div>
  <h2>
  </h2>

  <h2>
    Latest Articles:
  </h2>

  <p>
    Please scroll down to read our recently published articles . Click on a link to read the complete article:
  </p>

  <p>
  <ul class="tags-box">

  {% if site.posts != empty %}

  {% for post in site.posts %}
  {% capture this_year %}{{ post.date | date: "%Y" }}{% endcapture %}
  {% unless year == this_year %}
  {% assign year = this_year %}
  {% unless post == site.posts.first %}
  {% endunless %}
  {% endunless %}
  <li> <a href="{{ site.baseurl }}{{ post.url }}">{{ post.title | capitalize }}</a><br /></li>
  {% endfor %}

  {% else %}

  <span>No posts</span>

  {% endif %}

  </ul>
  </p>

  <h2>
    Most Popular Articles
  </h2>

  <p>
    Here is a list of Most popular Articles which we have written in the past, So take your time and Make sure you don&#8217;t miss any of the Article
  </p>

  <ul>
    <li>
      <a title="[Windows 8 Review] All about Mircosoft new Operating System Windows 8." href="http://www.askprateek.tk/windows-8-review-all-about-mircosoft-new-operating-system-windows-8/">[Windows 8 Review] All about Microsoft New Operating System Windows 8</a>
    </li>
    <li>
      <a title="[Windows 8 Features] All about new additions in Windows 8" href="http://www.askprateek.tk/windows-8-features-all-about-new-additions-in-windows-8/">[Windows 8 Features] All about the new Additions in Windows 8</a>
    </li>
    <li>
      <a title="Windows 7 Loader: Activate any version of Windows 7 with Genuine Key" href="http://www.askprateek.tk/windows-7-loader-activate-any-version-of-windows-7-with-genuine-key/">Windows 7 Loader: Activate Windows 7 with just one click</a>
    </li>
    <li>
      <a title="Download Windows 7 Anytime Upgrade Keygen." href="http://www.askprateek.tk/download-windows-7-anytime-upgrade-keygen/">Windows 7 AnyTime Upgrade Keygen: Upgrade your Windows 7 Edition</a>
    </li>
    <li>
      <a title="How to make a copy of Windows XP Geniune [ Activate Windows XP ]." href="http://www.askprateek.tk/how-to-make-a-copy-of-windows-xp-geniune/">How to make a copy of Windows XP Genuine</a>
    </li>
    <li>
      <a title="Transform Windows XP into Windows XP DarkEdition V7 without using Transformation Pack." href="http://www.askprateek.tk/transform-windows-xp-into-windows-xp-darkedition-v7-without-using-transformation-pack/">Transform XP into Windows XP Dark Edition without using Customization Pack</a>
    </li>
    <li>
      <a title="Download Three Windows 8 Metro UI Themes for Windows Seven" href="http://www.askprateek.tk/download-windows-8-metro-ui-theme-for-seve/">Download Windows 8 MetroUI Theme for Windows 7</a>
    </li>
    <li>
      <a title="Download Windows 8 RC Theme for Windows VISTA" href="http://www.askprateek.tk/download-windows-8-rc-theme-for-windows-vista/">Download Windows 8 MetroUI Theme for Windows Vista</a>
    </li>
    <li>
      <a title="Download Best Windows 8 MetroUI Theme for Windows XP." href="http://www.askprateek.tk/download-best-windows-8-metroui-theme-for-windows-xp/">Download Windows 8 MetroUI Theme for Windows XP</a>
    </li>
    <li>
      <a title="How to Root your Android Smart Phone or Tablet in one click" href="http://www.askprateek.tk/how-to-root-your-android-smart-phone-or-tablet-in-one-click/">How to Root your Android Device with one click</a>
    </li>
  </ul>

  <blockquote>
    <p>
      Ask Prateek | The Windows Explorer
    </p>
  </blockquote>
</div>

 [1]: http://www.askprateek.tk/category/windows-xp/
 [2]: http://www.askprateek.tk/category/windows-vista/
 [3]: http://www.askprateek.tk/category/windows-se7en/
 [4]: http://www.askprateek.tk/category/visual-styles/
 [5]: http://www.askprateek.tk/category/games/
 [6]: http://www.askprateek.tk/category/cracks-and-keygens/
 [7]: http://www.askprateek.tk/category/e-books/
 [8]: http://www.askprateek.tk/windows-8-review-all-about-mircosoft-new-operating-system-windows-8/
 [9]: http://www.askprateek.tk/windows-8-features-all-about-new-additions-in-windows-8/
