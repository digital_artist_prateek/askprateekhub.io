---
title: How to Create UEFA Champions League in FIFA 12, 13 | Offline | PC, PS3, XBox
author: Ask Prateek
layout: post
permalink: /how-to-create-uefa-champions-league-in-fifa-12-13-offline-hd-pc-ps3-xbox/
categories:
  - FIFA 12 Tutorials
tags:
  - 11
  - 12
  - 13
  - champions
  - chelsea
  - create
  - fifa
  - league
  - madrid
  - uefa
---
<!--more-->

You all must be aware of the fact that FIFA 12 doesn&#8217;t contain any Actual UEFA Champions league. It is only available to Online users, who buy it from EA Sports. But Today I am going to show you **how you can create UEFA Champions league in FIFA 12, 13** and Start you carrier with Any of the 5 star clubs playing in one league.

Here is a video Tutorial which will provide you step by step instructions to **create UEfA Champions League in FIFA 12, 13**:



Watch the Video in 360p Quality. This will be output after starting a new Carrier in FIFA 12:

<img class="alignnone size-full wp-image-2216" alt="Champians League" src="http://www.askprateek.tk/wp-content/uploads/2013/01/Champians-League2.jpg" width="578" height="563" />

If you don&#8217;t want to Watch the Whole Video then here are the instructions which you can read to create UEFA Champions League in FIFA 12, 13:

> <p style="text-align: center;">
>   <strong>How to Create UEFA Champions League in FIFA 12/13?</strong>
> </p>

**1.**Start a New Carrier.

**2.**Reach till Club Selection Page.

**3.**Press Y button( In case of XBox ) or Triangle( in case of PS).Which we use for Through Ball Or you can use mouse to click on Y(Swap) button shown on the screen

Now the selected Club can be replaced by Other clubs in the different leagues.

**Note**: It is advised to replace clubs of Lower Leagues in case of England, because  
after one season clubs of lower league will come in Bigger league, like Npower league. Serie A, B.

If you like this trick then it is Recommended to Subscribe to our youtube channel so that you don&#8217;t miss future tricks like this.

<a title="Our Youtube channel" href="http://www.youtube.com/user/AskPk123" target="_blank"></p> 

<blockquote>
  <p>
    <span style="text-decoration: underline;"><strong>Ask Prateek on Youtube</strong></span>
  </p>
</blockquote>

<p>
  </a>
</p>