---
title: '[DarkVersions] Download Windows XP DarkPK Icons for XP.'
author: Ask Prateek
layout: post
permalink: /darkversions-download-windows-xp-darkpk-icons-for-xp/
jabber_published:
  - 1321085492
  - 1321085492
  - 1321085492
  - 1321085492
  - 1321085492
  - 1321085492
  - 1321085492
  - 1321085492
  - 1321085492
  - 1321085492
  - 1321085492
  - 1321085492
  - 1321085492
  - 1321085492
  - 1321085492
  - 1321085492
email_notification:
  - 1321085495
  - 1321085495
  - 1321085495
  - 1321085495
  - 1321085495
  - 1321085495
  - 1321085495
  - 1321085495
  - 1321085495
  - 1321085495
  - 1321085495
  - 1321085495
  - 1321085495
  - 1321085495
  - 1321085495
  - 1321085495
yourls_shorturl:
  - http://ask-pra.tk/2a
  - http://ask-pra.tk/2a
  - http://ask-pra.tk/2a
  - http://ask-pra.tk/2a
categories:
  - Softwares
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - cursors
  - dark
  - darklite
  - edition
  - icons
  - theme
  - tweakes
  - windows
  - Xp
---
<!--more-->

<span style="color: #000000;">Finally we have created V1 of Windows XP DarkEdition and DarkLite V2 icon pack for Windows XP. It will change almost all Explorer icons with the DarkVersions one.</span>

![][1]

<span style="color: #000000;">It will also <strong>change</strong> some <strong>cursors of Windows</strong> with the one in Windows XP DarkEdition and DarkLite. You can download it from the following link:</span>

> <span style="text-decoration: underline;"><strong><a href="http://www.deviantart.com/download/308856496/windows_xp_darkpk_icons_for_xp_by_prateek_kumar-d53vv74.rar">Download Windows XP DarkPK  Icon pack</a> | <a href="http://www.mediafire.com/?bm3euiuwei2m6dt" target="_blank">Mirror</a></strong></span>

> <span style="color: #000000;"><strong>How to Use ?</strong></span>

<span style="color: #000000;">After downloading it you will need to extract the icon theme using</span>** <a title="Our Downloads Section" href="http://www.askprateek.tk//downloads/" target="_blank">WinRAR or 7- zip. </a>**

<span style="color: #000000;">You&#8217;ll also need &#8220;<strong>IconTweaker</strong>&#8221; to apply this icon theme. IconTweaker setup is included in this pack. Install IconTweaker and then double-click on &#8220;<strong>Windows XP DarkPK Icons and Cursor Pack.itt</strong>&#8221; file, it&#8217;ll open IconTweaker, click on<strong> Apply</strong> button.</span>

<span style="color: #000000;">If you want to completely change your Desktop GUI then you can also <strong>Download Orignal DarkVersion Themes</strong> from Here:</span>

<span style="text-decoration: underline;"><strong><a title="Transform Windows XP into Windows XP DarkEdition V7 without using Transformation Pack." href="http://www.askprateek.tk//transform-windows-xp-into-windows-xp-darkedition-v7-without-using-transformation-pack/" target="_blank">Download Windows XP Dark Edition Rebirth Refix V7 Theme</a></strong></span>

<span style="text-decoration: underline;"><strong><a title="Download Windows XP DarkLite V2 Theme for XP Professional" href="http://www.askprateek.tk//download-windows-xp-darklite-v2-theme-for-xp-professional/" target="_blank">Download Windows XP DarkLite V2 Theme for XP Professional</a> </strong></span>

You can also use this icon pack with Windows Vista Theme which can be downloaded from **<a title="Download the Best Windows Vista Theme “VistaVG Ultimate” for Windows XP" href="http://www.askprateek.tk//download-the-best-windows-vista-theme-vistavg-ultimate-for-windows-xp/" target="_blank">Here</a>**

 [1]: http://fc04.deviantart.net/fs70/i/2012/168/0/1/windows_xp_darkpk_icons_for_xp_by_prateek_kumar-d53vv74.png