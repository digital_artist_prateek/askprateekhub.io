---
title: 'Close All: Close All Running Application With One Click'
author: Ask Prateek
layout: post
permalink: /how-to-close-all-running-application-with-one-click/
blogger_blog:
  - askpk123.blogspot.com
  - askpk123.blogspot.com
  - askpk123.blogspot.com
  - askpk123.blogspot.com
  - askpk123.blogspot.com
  - askpk123.blogspot.com
  - askpk123.blogspot.com
  - askpk123.blogspot.com
  - askpk123.blogspot.com
  - askpk123.blogspot.com
  - askpk123.blogspot.com
  - askpk123.blogspot.com
  - askpk123.blogspot.com
  - askpk123.blogspot.com
  - askpk123.blogspot.com
  - askpk123.blogspot.com
blogger_permalink:
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
  - /2011/01/how-to-close-all-running-application.html
sfw_comment_form_password:
  - NqHkxLcriDPz
  - NqHkxLcriDPz
  - NqHkxLcriDPz
  - NqHkxLcriDPz
  - NqHkxLcriDPz
  - NqHkxLcriDPz
  - NqHkxLcriDPz
  - NqHkxLcriDPz
  - NqHkxLcriDPz
  - NqHkxLcriDPz
  - NqHkxLcriDPz
  - NqHkxLcriDPz
  - NqHkxLcriDPz
  - NqHkxLcriDPz
  - NqHkxLcriDPz
  - NqHkxLcriDPz
yourls_shorturl:
  - http://ask-pra.tk/1w
  - http://ask-pra.tk/1w
  - http://ask-pra.tk/1w
  - http://ask-pra.tk/1w
socialize_text:
  - |
    <div id="presetListDiv">This article was posted by <a href="http://www.askpk123.tk">Prateek Kumar</a> in following section:  [post_list name="categories"]</div>
    <div>
    
    If you enjoyed this article, then <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Like us on Facebook</a> and <a title="Follow us on Twitter" href="http://twitter.com/AskPK123" target="_blank">Follow us on Twitter</a> so that you won't miss any of the great Article. Also you can checkout our <a title="Our DeviantART Gallery" href="http://prateek-kumar.deviantart.com/" target="_blank">DeviantART Gallery</a> and browse for some Art Work by Me.
    
    Thanks for Reading and It is requested to leave a comment if you want to Discuss something or simply say Thanks
    
    </div>
  - |
    <div id="presetListDiv">This article was posted by <a href="http://www.askpk123.tk">Prateek Kumar</a> in following section:  [post_list name="categories"]</div>
    <div>
    
    If you enjoyed this article, then <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Like us on Facebook</a> and <a title="Follow us on Twitter" href="http://twitter.com/AskPK123" target="_blank">Follow us on Twitter</a> so that you won't miss any of the great Article. Also you can checkout our <a title="Our DeviantART Gallery" href="http://prateek-kumar.deviantart.com/" target="_blank">DeviantART Gallery</a> and browse for some Art Work by Me.
    
    Thanks for Reading and It is requested to leave a comment if you want to Discuss something or simply say Thanks
    
    </div>
socialize:
  - 
  - 
categories:
  - Softwares
tags:
  - all
  - applications
  - click
  - close
  - download
  - one
  - with
---
<a name="more"></a>

<!--more-->In the end of the day I always like to close the open applications to prepare my computer for the next day. Since I rarely reboot my desktop, I manually close each application. Depending on the number of open applications this can be a waste of time.   The people at NTWind Software have a perfect utility for this situation aptly called Close All Windows. Instead of manually closing each application just click the Close All Windows button on your taskbar.

  
In this article I will show you how to install and configure Close All Windows on your computer for easy access on the taskbar.  
To get started head over to NTWind Software and [download the latest version][1] of Close All Windows.  
After the file is downloaded extract the CloseAll folder to a permanent location on your computer.  Since Close All Windows is an application I extracted the folder to C:Program Files where applications are typically installed.  
<img src="http://static.advancedpcmedia.com/img/article/closeall1.png" alt="" width="465" height="269" />  
For easy access it is best to pin the application to the taskbar. Drag the Close All Windows application from the CloseAll folder onto the taskbar.  
<img src="http://static.advancedpcmedia.com/img/article/closeall2.png" alt="" width="239" height="124" />  
Congratulations, you are now more efficient.

 [1]: http://www.ntwind.com/software/utilities/close-all.html