---
title: How toTransform XP to Ubuntu without using Customization Pack
author: Ask Prateek
layout: post
permalink: /how-to-transform-xp-to-ubuntu-without-using-customization-pack/
yourls_shorturl:
  - http://ask-pra.tk/20
  - http://ask-pra.tk/20
  - http://ask-pra.tk/20
  - http://ask-pra.tk/20
categories:
  - Softwares
  - Windows XP
tags:
  - customization
  - into
  - pack
  - transform
  - ubuntu
  - using
  - windows
  - without
  - Xp
---
<!--more-->

Linux Ubuntu is an Open-Source operating System and it have very unique and sleek look. You may find many interesting thing in “Linux Ubuntu” i.e. its look, new icons, theme, sounds, logon screen, boot screen, etc. If you are using Windows XP but want to enjoy the “Linux Ubuntu look” then this tutorial will sure help you.

Below is a list of things which are going to change after in this tutorial:

**1.) Linux Visual Style  
2.) Icons  
3.) Boot Screen  
4.) Cursors  
5.) About Ubuntu Dialog Box  
6.) Shutdown Dialog Box  
7.) Ubuntu Firefox Skin  
8.) Ubuntu Styler  
9.) Ubuntu System Properties  
10.) Wallpapers  
11.) Ubuntu Logon Screen  
12.) Few other Changes…**

> <p style="text-align:center;">
>   <strong>1. Linux Visual Style</strong>
> </p>

<img title="ubuntu_theme" src="http://www.rajeshpatel.net/wp-content/uploads/2009/05/ubuntu_theme.png" alt="ubuntu theme Transform Windows XP into Linux Ubuntu without using Customization Pack" width="525" height="395" />

[**Download Linux Ubuntu Theme for Windows XP**][1]

> <p style="text-align:center;">
>   <strong>2. Linux Icons</strong>
> </p>

<img title="ubuntu_icons" src="http://www.rajeshpatel.net/wp-content/uploads/2010/03/ubuntu_icons.jpg" alt="ubuntu icons Transform Windows XP into Linux Ubuntu without using Customization Pack" width="525" height="380" />

[**Download Linux Ubuntu icon pack for XP**][2]

> <p style="text-align:center;">
>   <strong>3. Boot Screen</strong>
> </p>

<p style="text-align:left;">
  <img title="ubuntu_boot" src="http://www.rajeshpatel.net/wp-content/uploads/2010/03/ubuntu_boot.jpg" alt="ubuntu boot Transform Windows XP into Linux Ubuntu without using Customization Pack" width="525" height="374" />
</p>

[**Download Linux Ubuntu Boot Screen for XP**][3]

> <p style="text-align:center;">
>   <strong>4. Ubuntu Cursor</strong>
> </p>

<img title="Ubuntu Cursor" src="http://www.rajeshpatel.net/wp-content/uploads/2010/03/CursorsPreview2-1.jpg" alt="Ubuntu Cursor" width="525" height="372" />

[**Download Linux Ubuntu Cursors for XP**][4]

> <p style="text-align:center;">
>   <strong>5. </strong><strong>About Ubuntu Dialog Box</strong>
> </p>

<img title="About Ubuntu Dialog Box" src="http://www.rajeshpatel.net/wp-content/uploads/2010/03/About_Ubuntu.jpg" alt="About Ubuntu Dialog Box" width="525" height="80" />

[**Get About Ubuntu Dialog Box in Windows XP**][5]

> <p style="text-align:center;">
>   <strong>6. </strong><strong>Shutdown Dialog Box</strong>
> </p>

<img title="SystemPreview3-1" src="http://www.rajeshpatel.net/wp-content/uploads/2009/03/SystemPreview3-11.png" alt="SystemPreview3 11 Transform Windows XP into Linux Ubuntu without using Customization Pack" width="525" height="372" />

[**Get Ubuntu Shutdown Dialog Box for XP**][6]

> <p style="text-align:center;">
>   <strong>7. </strong><strong>Ubuntu Firefox Skin</strong>
> </p>

<img title="FirefoxPreview-1" src="http://www.rajeshpatel.net/wp-content/uploads/2010/03/FirefoxPreview-1.jpg" alt="FirefoxPreview 1 Transform Windows XP into Linux Ubuntu without using Customization Pack" width="525" height="372" />

[**Download the Firefox theme file**][7]

> <p style="text-align:center;">
>   <strong>8. </strong><strong>Ubuntu Styler</strong>
> </p>

<img title="StylerPreview-1" src="http://www.rajeshpatel.net/wp-content/uploads/2010/03/StylerPreview-1.jpg" alt="StylerPreview 1 Transform Windows XP into Linux Ubuntu without using Customization Pack" width="525" height="372" />

[**Download the ubuntu Styler theme**][8]

> <p style="text-align:center;">
>   <strong>9. </strong><strong>Ubuntu System Properties</strong>
> </p>

<img title="SystemPreview3-1" src="http://www.rajeshpatel.net/wp-content/uploads/2009/03/SystemPreview3-1.png" alt="SystemPreview3 1 Transform Windows XP into Linux Ubuntu without using Customization Pack" width="525" height="372" />

[**Get ****Ubuntu System Properties**** for XP from here**][9]

> <p style="text-align:center;">
>   <strong>10. Ubuntu Wallpapers</strong>
> </p>

<img title="Ubantu01-1" src="http://www.rajeshpatel.net/wp-content/uploads/2009/03/Ubantu01-1.jpg" alt="Ubantu01 1 Transform Windows XP into Linux Ubuntu without using Customization Pack" width="525" height="372" />

[**Click here to Download 35 stunning linux Wallpapers**][10]

> <p style="text-align:center;">
>   <strong>11. </strong><strong>Ubuntu Logon Screen</strong>
> </p>

<img title="ubuntu_logon" src="http://www.rajeshpatel.net/wp-content/uploads/2010/03/ubuntu_logon.jpg" alt="ubuntu logon Transform Windows XP into Linux Ubuntu without using Customization Pack" width="525" height="400" />

**[Download Ubuntu Logon Screen For XP][11]**

<div>
  <strong>Note:</strong> If you want to Transform your Windows XP to MacOS then follow thi article:
</div>

<div>
  <p>
    <span style="text-decoration:underline;"><strong><a href="http://www.askprateek.tk/2011/08/19/how-to-transform-xp-into-mac-os-leopard/" rel="bookmark">How to transform XP into Mac OS Leopard without using Transformation pack</a></strong></span>
  </p>
</div>

 [1]: http://www.rajeshpatel.net/download-linux-visual-style-for-windows-xp/
 [2]: http://www.rajeshpatel.net/download-linux-icons-or-windows-xp/
 [3]: http://www.rajeshpatel.net/download-ubuntu-boot-screen-for-windows-xp/
 [4]: http://www.rajeshpatel.net/download-ubuntu-cursor-for-windows-xp/
 [5]: http://www.rajeshpatel.net/get-ubuntu-about-dialog-box-for-windows-xp/
 [6]: http://www.rajeshpatel.net/ubuntu-shutdown-dialog-box-for-windows-xp/
 [7]: http://www.rajeshpatel.net/download-ubuntu-firefox-skin-for-windows-xp/
 [8]: http://www.rajeshpatel.net/download-ubuntu-styler-for-windows-xp/
 [9]: http://www.rajeshpatel.net/ubuntu-system-properties-for-xp/
 [10]: http://www.rajeshpatel.net/download-35-stunning-ubuntu-wallpapers/
 [11]: http://www.rajeshpatel.net/download-ubuntu-logon-screen-for-windows-xp/