---
title: 'UxStyle: A small utility to patch uxtheme.dll to use 3rd party Themes.'
author: Ask Prateek
layout: post
permalink: /uxstyle-a-small-utility-to-patch-uxtheme-dll-to-use-3rd-party-themes/
jabber_published:
  - 1317739313
  - 1317739313
  - 1317739313
  - 1317739313
  - 1317739313
  - 1317739313
  - 1317739313
  - 1317739313
  - 1317739313
  - 1317739313
  - 1317739313
  - 1317739313
  - 1317739313
  - 1317739313
  - 1317739313
  - 1317739313
yourls_shorturl:
  - http://ask-pra.tk/1k
  - http://ask-pra.tk/1k
  - http://ask-pra.tk/1k
  - http://ask-pra.tk/1k
categories:
  - Softwares
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - custom
  - patch
  - themes
  - uxstyle
  - uxtheme.dll.use
---
<!--more-->

&#8220;**UxStyle**&#8221; is a very small utility created by our friend &#8220;**Rafael**&#8221; @ <a href="http://www.withinwindows.com/2009/06/19/uxstyle-core-beta-bits-now-available/" target="_blank">Withinwindows</a>.

> UxStyle is a light-weight system service named Unsigned Themes, complimentary to the Themes service, and a kernel driver, sizing in at ~500k and ~17kb respectfully (beta builds). The service handles the enabling/disabling of custom theme support and the kernel driver handles patching. For 64-bit platforms, the kernel driver is signed with a digital certificate, as required by Microsoft.

Simply download and install the tool and get ready to enjoy 3rd party themes. **It works in Windows XP, Vista and 7**.

Note that **it doesn&#8217;t have any UI**. It&#8217;ll run as a service in background and will allow you to use 3rd party themes.

<span style="text-decoration: underline;"><strong><a href="http://uxstyle.com/" target="_blank">Download Link</a> </strong></span>

Also Check:

  * [Download Windows XP Dark Edition Rebirth Refix V7 Theme for XP.][1]
  * [Download Windows 8 MetroUI Theme for XP.][2]
  * [Download Windows 8 AeroLite Theme for Windows Seven.][3]
  * [Universal  Theme Patcher: Yet another tool to patch stytem files for Aero and Themes][4]

&nbsp;

 [1]: http://www.askprateek.tk/download-windows-xp-dark-edition-rebirth-refix-v7-theme/ "Download Windows XP Dark Edition Rebirth Refix V7 Theme"
 [2]: http://www.askprateek.tk/download-windows-8-metroui-theme-for-windows-xp/ "Download Windows 8 MetroUI Theme for Windows XP."
 [3]: http://www.askprateek.tk/download-windows-8-metro-ui-theme-for-seve/ "Download Windows 8 Metro UI Theme for Windows Seven"
 [4]: http://www.askprateek.tk/universal-theme-patcher-yet-another-tool-to-patch-system-files-to-use-themes-in-windows-xp-vista-and-7/ "Universal Theme Patcher: Yet another tool to patch system files to use themes in Windows XP, Vista and 7."