---
title: 'Folder2MyPC: Add any Folders and applications in your Control Panel and My Computer.'
author: Ask Prateek
layout: post
permalink: /folder2mypc-add-folders-applications-control-panel-computer/
yourls_shorturl:
  - http://ask-pra.tk/3a
  - http://ask-pra.tk/3a
categories:
  - Softwares
  - Windows Vista
  - Windows XP
tags:
  - add
  - application
  - computer
  - folder
  - my
  - vista
  - Xp
---
<!--more-->

Hello Readers,

Have you ever thought of adding your favorite application shortcut in My Computer. Not yet, then think now. We are sharing a cool utility to add any Folder or Application shortcut in My Computer

The program&#8217;s interface is sleek and uncluttered, with separate tabs for creating both folder and application shortcuts. Users simply select the folder or application that they want to create a shortcut for, give it a name, and optionally select a custom icon for it. Folder shortcuts are created in My Computer, while application shortcuts can be added to My Computer or the Control Panel, including Control Panel subcategories. An additional tab gives users the option to edit or remove shortcuts they&#8217;ve already created, and another lists Windows folders with check boxes next to them. For the most part, the program is fairly intuitive and easy to use.

> **The main features:**

&#8211; Add/change/remove any folder and programs in a folders &#8220;My Computer&#8221; and &#8220;Control panel&#8221;,  
&#8211; You can chose how you want to open the folder (in a new window or not),  
&#8211; Customize the shortcut&#8217;s icon,  
&#8211; Create shortcut&#8217;s both for the current user and for all users of PC,  
&#8211; Can be translated to other languages,  
&#8211; Program loading for display of new folders, after creation of folders is not required.

<img src="http://blogwache.de/wp-content/uploads/2011/02/folder2mypc-explorer.jpg" alt="" width="560" height="405" />

This tool is only available for **<a title="Windows XP" href="http://www.askprateek.tk/category/windows-xp/" target="_blank">Windows XP</a>** and **<a title="Window Vista" href="http://www.askprateek.tk/category/windows-vista/" target="_blank">Vista</a>**. You can Download it From the following link:

> <span style="text-decoration: underline;"><strong><a href="http://eng.softq.org/folder2mypc" target="_blank">Download Folder2MyPC for Windows Vista </a></strong></span>
> 
> <span style="text-decoration: underline;"><strong><a href="http://shedko.googlepages.com/Shedko_Folder2MyPC_1.8.5.0.zip" target="_blank">Download Folder2MyPC for Windows XP</a></strong></span>