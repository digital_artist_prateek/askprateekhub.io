---
title: How to install Kloxo Panel in VPS or Dedicated Server running on Centos or RedHat
author: Ask Prateek
layout: post
permalink: /how-to-install-kloxo-panel-in-vps-or-dedicated-server-running-on-centos-or-redhat/
Hide SexyBookmarks:
  - 0
Hide OgTags:
  - 0
categories:
  - VPS
tags:
  - centos
  - install
  - kloxo
  - panel
  - redhat
---
<!--more-->

Hello Everyone!

We recently posted a Tutorial which deals with installing ZPanel and Counter Strike Server on your VPS or Dedicated Server. If you haven&#8217;t read them then you can check those articles using the following link:

<span style="text-decoration: underline;"><strong><a title="How to install ZPanel on Centos 6  on VPS or Dedicated Server" href="http://www.askprateek.tk/how-to-install-zpanel-on-centos-6/">How to install ZPanel on Centos 6 on VPS or Dedicated Server</a></strong></span>

<span style="text-decoration: underline;"><strong><a title="How to install Counter Strike 1.6 on VPS or Dedicated Server" href="http://www.askprateek.tk/how-to-install-counter-strike-1-6-on-vps-or-dedicated-server/">How to install Counter Strike 1.6 on VPS or Dedicated Server</a></strong></span>

Now today we are concentrating on another free alternative of CPanel called Kloxo Panel. Kloxo Panel is great alternative of CPanel, even better then ZPanel. Since <a title="How to install ZPanel on Centos 6  on VPS or Dedicated Server" href="http://www.askprateek.tk/how-to-install-zpanel-on-centos-6/" target="_blank">ZPanel</a> is new hence there are Security issues related to ZPanel. But Kloxo Panel is much secure then Zpanel.

So, Before we start Please take a look at the Requirements of Kloxo Panel:

**1)** A dedicated or virtual server running **CentOS or Red Hat EL 5.x. CentOS 6.x is not currently supported**.

**2)** At least 256 MB of RAM (enough to run Yum).

**3)** At least 2 GB of free disk space for Kloxo and related services.

**4)** If you partitioned your disks manually, make sure you have a large /tmp. Kloxo uses /tmp to create and store backups temporarily and the process will fail if there is not enough space.

**5) **Please Make sure that Selinux is Disabled. You can check Selinux status by typing **sestatus** in SSH client.

If it is not Disabled then use the following code to open Selinux file:

> vi /etc/sysconfig/selinux

Now change the value in Front of Selinux to disabled.

Then you must run the following command as root to disable SELinux for the current session:

> \# su &#8211; root
> 
> \# setenforce 0

Now you are ready for installing Kloxo Panel on your Server. Just Run the following command one by one to install

**Note**: The script will present you with a few questions and sometimes ask for a password (enter your root password).

**If you don&#8217;t have MySQL installed on your Server:**

> su &#8211; root
> 
> yum install -y wget
> 
> wget http://download.lxcenter.org/download/kloxo/production/kloxo-installer.sh

To install as Master (Default Single Server):

> <p dir="ltr">
>   sh ./kloxo-installer.sh &#8211;type=master
> </p>

To install as Slave:

> <p dir="ltr">
>   sh ./kloxo-installer.sh &#8211;type=slave
> </p>

<p dir="ltr">
  <strong>If you have already installed MySQL on your Server:</strong>
</p>

> <p dir="ltr">
>   su &#8211; root
> </p>
> 
> <p dir="ltr">
>    yum install -y wget
> </p>
> 
> <p dir="ltr">
>   wget http://download.lxcenter.org/download/kloxo/production/kloxo-installer.sh
> </p>
> 
> <p dir="ltr">
>    sh ./kloxo-installer.sh &#8211;type=<master/slave> &#8211;db-rootpassword=<strong>YourRootPasswordHere</strong>
> </p>

<p dir="ltr">
  Now you are done, if everything goes right then it will show this kind information in the end of install:
</p>

<p dir="ltr">
  <img alt="" src="https://fbcdn-sphotos-b-a.akamaihd.net/hphotos-ak-prn1/29630_504006002984137_724991468_n.jpg" />
</p>

<p dir="ltr">
  you can connect to http://<strong>YOUR_SERVER_IP</strong>:7778 and you will be presented with a login screen. Login as <strong><em>admin</em></strong> with password <strong><em>admin</em></strong> and once you are in, Kloxo will ask you to change the default password to a secure one.
</p>