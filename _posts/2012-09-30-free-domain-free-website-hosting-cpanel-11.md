---
title: How to get Free Domain and Free Website Hosting Along with CPanel 11
author: Ask Prateek
layout: post
permalink: /free-domain-free-website-hosting-cpanel-11/
yourls_shorturl:
  - http://ask-pra.tk/3b
  - http://ask-pra.tk/3b
socialize:
  - 
  - 
socialize_text:
  - |
    <div id="presetListDiv">This article was posted by <a href="http://www.askpk123.tk">Prateek Kumar</a> in following section:  [post_list name="categories"]</div>
    <div>
    
    If you enjoyed this article, then <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Like us on Facebook</a> and <a title="Follow us on Twitter" href="http://twitter.com/AskPK123" target="_blank">Follow us on Twitter</a> so that you won't miss any of the great Article. Also you can checkout our <a title="Our DeviantART Gallery" href="http://prateek-kumar.deviantart.com/" target="_blank">DeviantART Gallery</a> and browse for some Art Work by Me.
    
    Thanks for Reading and It is requested to leave a comment if you want to Discuss something or simply say Thanks
    
    </div>
  - |
    <div id="presetListDiv">This article was posted by <a href="http://www.askpk123.tk">Prateek Kumar</a> in following section:  [post_list name="categories"]</div>
    <div>
    
    If you enjoyed this article, then <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Like us on Facebook</a> and <a title="Follow us on Twitter" href="http://twitter.com/AskPK123" target="_blank">Follow us on Twitter</a> so that you won't miss any of the great Article. Also you can checkout our <a title="Our DeviantART Gallery" href="http://prateek-kumar.deviantart.com/" target="_blank">DeviantART Gallery</a> and browse for some Art Work by Me.
    
    Thanks for Reading and It is requested to leave a comment if you want to Discuss something or simply say Thanks
    
    </div>
categories:
  - Internet
tags:
  - 11
  - cpanel
  - domain.name
  - forever
  - free
  - hosting
  - life
  - web
  - with
---
Hello Everyone,

Todat we are posting a step by step Tutorial to create a Free Domain and Host it totally free of cost along with CPanel 11.  
**1.** First Download Dot.tk URL Shortner by clicking on the pic shown below:

<a href="http://my.dot.tk/cgi-bin/amb/landing.dottk?nr=534740::10202941::100" target="_top"><img src="http://images.dot.tk/tkit/0/234x60_tkitpromo_banner-1.gif" alt="" border="0" /></a>  
**2.** Now you need to register your account at Dot.TK so that you can renew your Domain for Free

> **<a href="http://my.dot.tk/cgi-bin/login01.taloha" target="_blank">Register at Dot.TK</a>**

Registration is Important otherwise you won&#8217;t be able renew your Domain after 1 year for Free.  
**3.** Login into you Dot.TK account, do to Domain panel >> Add a Domain:

<table cellspacing="0" cellpadding="0">
  <tr>
    <td>
      <a href="http://3.bp.blogspot.com/-8wqprng4vhI/UGP_2n6HA-I/AAAAAAAAAE8/1rc6oN5pgD0/s1600/Domain+panel.png"><img src="http://3.bp.blogspot.com/-8wqprng4vhI/UGP_2n6HA-I/AAAAAAAAAE8/1rc6oN5pgD0/s1600/Domain+panel.png" alt="" border="0" /></a>
    </td>
  </tr>
  
  <tr>
    <td>
    </td>
  </tr>
</table>

**4.** Now type Yourname or any domain name which you want and click **Add New Domain**. Select** Free Domain and click next**.  
Now you have you own domain and you will have to configure it

**5. **We are going to host this Domain at Host-Ed so We will update the Name Servers of Host-Ed.  
So, click on **Use DNS for this Domain>>Use My Own DNS Service** and add the two Name Servers in the text Box as shows in the Screenshot:

<div>
  <a href="http://2.bp.blogspot.com/-C2UFTLcGDdQ/UGQC8UWVTKI/AAAAAAAAAFM/utdbH1AFd30/s1600/DNS+CONFIG.png"><img src="http://2.bp.blogspot.com/-C2UFTLcGDdQ/UGQC8UWVTKI/AAAAAAAAAFM/utdbH1AFd30/s1600/DNS+CONFIG.png" alt="" border="0" /></a>
</div>

> **Name Servers:  **
> 
> ns1.host-ed.me  
> ns2.host-ed.me

> <p style="text-align: center;">
>   <strong>Now Register the Domain</strong>
> </p>

**6.** Now register an account at Host-Ed.net by visiting the following link. You can use Free Hosting or Paid Hosting according to your Budget. If you want a free website then go for** Free Web Hosting**

> **<a href="http://www.host-ed.me/members/aff.php?aff=874" target="_blank">Visit Host.Ed.net to create a Account</a>**

**Registration Instruction:**  
When you will click on sign up the take help from this screenshot:

<div>
  <a href="http://4.bp.blogspot.com/-2xW2qulkxQU/UGQFasbu2xI/AAAAAAAAAFc/llz1drF1wvc/s1600/Registration+instruction.png"><img src="http://4.bp.blogspot.com/-2xW2qulkxQU/UGQFasbu2xI/AAAAAAAAAFc/llz1drF1wvc/s1600/Registration+instruction.png" alt="" border="0" /></a><a href="http://4.bp.blogspot.com/-2xW2qulkxQU/UGQFasbu2xI/AAAAAAAAAFc/llz1drF1wvc/s1600/Registration+instruction.png"><br /> </a>
</div>

Now follow the on screen instruction and you are done. At the time of ORDER SUMMARY click checkout.

Now you have A Free Website along with Free CPanel Hosting