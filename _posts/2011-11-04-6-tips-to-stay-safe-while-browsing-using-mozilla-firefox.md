---
title: 6 Tips to Stay Safe While Browsing using Mozilla Firefox.
author: Ask Prateek
layout: post
permalink: /6-tips-to-stay-safe-while-browsing-using-mozilla-firefox/
jabber_published:
  - 1320427967
  - 1320427967
  - 1320427967
  - 1320427967
  - 1320427967
  - 1320427967
  - 1320427967
  - 1320427967
  - 1320427967
  - 1320427967
  - 1320427967
  - 1320427967
email_notification:
  - 1320428793
  - 1320428793
  - 1320428793
  - 1320428793
  - 1320428793
  - 1320428793
  - 1320428793
  - 1320428793
  - 1320428793
  - 1320428793
  - 1320428793
  - 1320428793
yourls_shorturl:
  - http://ask-pra.tk/1z
  - http://ask-pra.tk/1z
  - http://ask-pra.tk/1z
  - http://ask-pra.tk/1z
sfw_comment_form_password:
  - fzC5vz26wu8x
  - fzC5vz26wu8x
  - fzC5vz26wu8x
  - fzC5vz26wu8x
categories:
  - Mozilla Firefox
  - Softwares
tags:
  - browsing
  - firefox
  - malware
  - mozilla
  - phising
  - protect
  - safe
  - stay
  - tips
  - virus
---
<!--more-->

<img class="alignleft" src="https://a248.e.akamai.net/f/248/45320/14d/ig.rsys2.net/responsysimages/mozilla/2011MainstreamNews/2011_10_Mainstream_News_EN_Desktop/fflogo_125x125.jpg" alt="security" width="125" height="125" />There are two ways to stay secure using Firefox. OK, I know we just told you there are six, but they&#8217;re split into two camps: three things that Firefox does for you, and three things you can do to protect yourself. So we did some simple security math (and checked our work) to bring you six great tools and tips!  
First, here&#8217;s how Firefox has your back:

&nbsp;

> **Instant Website ID**

Easily see which sites you can trust and which you might need to be suspicious of with a color-coded system. Green is all good, while grey is more shady.** <a href="https://awesomeness.mozilla.org/pub/cc?_ri_=X0Gzc2X%3DUQpglLjHJlTQTtQyTQ7atQTYQcnzawQGQaQeK2VXtpKX%3DCDADR&_ei_=Gf6beX6990G11X%3DHksisSTUxnuHptQJhu." target="_blank">Learn to read the signs.</a> **

> **Anti-Phishing & Anti-Malware**

Trojan horses, spyware, fraudulent sites — these are all real online concerns, but Firefox helps warn you when you&#8217;re in danger. Go to the Firefox menu, select Preferences, click on Security and make sure that the &#8220;block reported web forgeries&#8221; checkbox is checked. Or **<a href="https://awesomeness.mozilla.org/pub/cc?_ri_=X0Gzc2X%3DUQpglLjHJlTQTtQyTQ7atQTYQcnzawQGQaQeK2VXtpKX%3DCDADS&_ei_=Gf6beX6990G11X%3DHksisSTUxnuHptQJhu." target="_blank">test out your own phishing detection.</a>**

> **Anti-Virus Integration **

If your computer gets a virus, it&#8217;s not something a little lie-down and a bowl of chicken soup is going to fix (liquids and computers generally don&#8217;t mix well). That&#8217;s why your computer&#8217;s anti-virus software integrates with Firefox to scan your downloads.

And now, a look at how you can have your own back:

> **Plugin Che****ck**

Plugins like Flash, Quicktime and Java can make your computer vulnerable if not up to date. Check and update them regularly with our** <a href="https://awesomeness.mozilla.org/pub/cc?_ri_=X0Gzc2X%3DUQpglLjHJlTQTtQyTQ7atQTYQcnzawQGQaQeK2VXtpKX%3DCDADT&_ei_=Gf6beX6990G11X%3DHksisSTUxnuHptQJhu." target="_blank">Plugin Check</a>** for happy computers and safe browsing.

> **Customized Security Settings**

**<a href="https://awesomeness.mozilla.org/pub/cc?_ri_=X0Gzc2X%3DUQpglLjHJlTQTtQyTQ7atQTYQcnzawQGQaQeK2VXtpKX%3DCDADU&_ei_=Gf6beX6990G11X%3DHksisSTUxnuHptQJhu." target="_blank">Dial the level of scrutiny</a>** you want Firefox to give sites up or down to match your tastes, from fairly lax to ultra paranoid.

> **Secure Passwords **

Hopefully none of your online passwords are &#8220;password.&#8221; And hopefully you didn&#8217;t just answer, &#8220;why not?&#8221; And hopefully you&#8217;ve chosen as strong a password as possible. **<a href="https://awesomeness.mozilla.org/pub/cc?_ri_=X0Gzc2X%3DUQpglLjHJlTQTtQyTQ7atQTYQcnzawQGQaQeK2VXtpKX%3DCDADW&_ei_=Gf6beX6990G11X%3DHksisSTUxnuHptQJhu." target="_blank">Here&#8217;s how.</a>**

If you don&#8217;t have Firefox then you can download the latest version from here

<span style="text-decoration:underline;"><strong><a title="[Mozilla Firefox Latest version Update] Firefox 7.0.1 released. Download link Inside." href="http://www.askprateek.tk/2011/09/29/mozilla-firefox-latest-version-update-firefox-7-0-released-download-link-inside/">Mozilla Firefox Latest version Update</a> </strong></span>

If you are a power user and want to try Future build of Firefox then here is somwthig for you

<span style="text-decoration:underline;"><strong><a title="[Mozilla Firefox Future Build Update] Mozilla Firefox 10.0 Alpha 1 Build Released, Download NOW" href="http://www.askprateek.tk/2011/09/29/mozilla-firefox-future-build-update-mozilla-firefox-10-0-alpha-1-build-released-download-now/">Mozilla Firefox Future Build Update</a></strong></span>