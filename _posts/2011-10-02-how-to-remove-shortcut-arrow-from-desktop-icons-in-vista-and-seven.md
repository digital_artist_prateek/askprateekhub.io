---
title: How to remove Shortcut Arrow from Desktop icons in Vista and Seven
author: Ask Prateek
layout: post
permalink: /how-to-remove-shortcut-arrow-from-desktop-icons-in-vista-and-seven/
jabber_published:
  - 1317552658
  - 1317552658
  - 1317552658
  - 1317552658
  - 1317552658
  - 1317552658
  - 1317552658
  - 1317552658
  - 1317552658
  - 1317552658
  - 1317552658
  - 1317552658
  - 1317552658
  - 1317552658
  - 1317552658
  - 1317552658
yourls_shorturl:
  - http://ask-pra.tk/21
  - http://ask-pra.tk/21
  - http://ask-pra.tk/21
  - http://ask-pra.tk/21
categories:
  - Softwares
  - Troubleshooting
  - Windows Se7en
  - Windows Vista
tags:
  - arrow
  - desktop
  - icons
  - remove
  - shortcut
  - vista
  - windows.seven.fix
---
<!--more-->Recently we posted a Tutorial to completely hide Shortcut Arrow from Desktop Icons for XP users.

<span style="text-decoration: underline;"><strong><a title="How to Remove shortcut arrow from Desktop icons." href="http://www.askprateek.tk/how-to-remove-shortcut-arrow-from-desktop-icons/">How to remove Shortcut Arrow from Desktop Icons in Windows XP.</a></strong></span>

So, Today we will teach you How to Customize or Remove Shortcut Arrow from the icons in Windows Vista and 7

> <p style="text-align: center;">
>   <strong><span style="color: #000000;">Method 1</span></strong>
> </p>

Download <a title="Download Link" href="http://www.pcworld.com/product/946383/vista-shortcut-overlay-remover-.html" target="_blank"><span style="text-decoration: underline;"><strong>Vista Shortcut Overlay Remover</strong></span> (</a>*FxVisor*) By Frameworkx.  FxVisor allows you to either way to modify or remove the shortcut overlay arrow in Windows 7 and Vista. Here is the screenshot of that utility:

<img class="alignnone size-full wp-image-1950" title="Vista Shortcut Overlay Remover" src="http://www.askprateek.tk/wp-content/uploads/2011/10/Vista-Shortcut-Overlay-Remover.jpg" alt="" width="458" height="323" />

> <p style="text-align: center;">
>   <strong><span style="color: #000000;">Method 2</span></strong>
> </p>

**Removing the shortcut arrow: **Download **[blank_icon.zip][1]** and extract the file **blank.ico** to a folder of your choice. In this example, we use ***C:\Icons\blank.ico*** as the path to the icon file you downloaded. And then follow these steps:

  1. Click Start, type **regedit.exe** and press ENTER
  2. Navigate to the following branch:

> **HKEY\_LOCAL\_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Shell Icons**

<ol start="3">
  <li>
    In the right pane, create a new String value (REG_SZ) named <strong>29</strong>
  </li>
  <li>
    Double-click <strong>29</strong> and set its Value data as <strong>C:\Icons\blank.ico</strong>
  </li>
  <li>
    Close Regedit.exe and restart Windows
  </li>
</ol>

> **Note:** If the **Shell Icons** branch does not exist already, you&#8217;ll need to create it.If you don&#8217;t know how to create a new key then visit <span style="text-decoration: underline;"><strong><a title="How to Remove shortcut arrow from Desktop icons." href="http://www.askprateek.tk/how-to-remove-shortcut-arrow-from-desktop-icons/" target="_blank">this Article</a></strong></span>. Read**Method 1.**

<img class="alignright" style="border-color: black; border-style: solid; border-width: 1px;" title="AskPK123" src="http://www.winhelponline.com/content_images/shortcut_lightarrow.gif" alt="" width="78" height="82" border="1" />

**Setting Light arrow overlay:** If you wish to use a light arrow for Shortcuts, you may do so by downloading **[lightarrow.zip][2]** (includes *lightarrow.ico*). Copy *lightarrow.ico* to your Icons folder, and set the Value data for **29** accordingly (see Step **4** above). If you&#8217;ve copied lightarrow.ico to your *C:Icons* folder, the Value data would be***C:\Icons\lightarrow.ico***

&nbsp;

 [1]: http://www.winhelponline.com/fixes/blank_icon.zip
 [2]: http://www.winhelponline.com/fixes/lightarrow.zip