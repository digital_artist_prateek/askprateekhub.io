---
title: '[Aero colours ]Download &#8220;Aero Black&#8221;, &#8220;Aero Pink&#8221; and &#8220;Aero Green&#8221; theme for Windows XP'
author: Ask Prateek
layout: post
permalink: /aero-colours-download-aero-black-aero-pink-and-aero-green-theme-for-windows-xp/
jabber_published:
  - 1343743355
  - 1343743355
  - 1343743355
  - 1343743355
  - 1343743355
  - 1343743355
  - 1343743355
  - 1343743355
email_notification:
  - 1343743355
  - 1343743355
  - 1343743355
  - 1343743355
  - 1343743355
  - 1343743355
  - 1343743355
  - 1343743355
yourls_shorturl:
  - http://ask-pra.tk/25
  - http://ask-pra.tk/25
categories:
  - Visual Styles
  - Windows XP
tags:
  - aero
  - black
  - green
  - pink
  - style
  - theme
  - visual
  - windows
  - Xp
---
<!--more-->

Hello Everyone

As you know that we love to share themes for Windows XP , Vista and Se7en. We have posted about many themes for Windows XP Vista and 7 users. If you haven&#8217;t check them, then you can check them via following links:

  * [Download Windows XP Dark Edition V7 Rebirth Refix Theme ][1]
  * [Download Windows XP DarkLite V2 Theme for XP ][2]
  * <a title="Download Windows 8 MetroUI Theme for Windows XP." href="http://www.askprateek.tk//download-windows-8-metroui-theme-for-windows-xp/" target="_blank">Download Windows 8 MetroUI Theme for XP</a>
  * <a title="Download Three Windows 8 Metro UI Themes for Windows Seven" href="http://www.askprateek.tk//download-windows-8-metro-ui-theme-for-seve/" target="_blank">Download Windows 7 Theme for XP</a>
  * <a title="Download Windows 8 RC Theme for Windows VISTA" href="http://www.askprateek.tk//download-windows-8-rc-theme-for-windows-vista/" target="_blank">Download Best Windows Vista Theme for XP</a>
  * <a title="Download Three Windows 8 Metro UI Themes for Windows Seven" href="http://www.askprateek.tk//download-windows-8-metro-ui-theme-for-seve/" target="_blank">Download Windows 8 MetroUI Theme for Windows Se7en.</a>
  * <a title="Download Best Windows 8 MetroUI Theme for Windows XP." href="http://askpk123.byethost18.com/download-best-windows-8-metroui-theme-for-windows-xp/" target="_blank">Download Best Windows 8 MetroUI Theme for XP (Without Start Button)</a>

Now we share 3 Themes for Windows XP users with 3 different flavours or colours for Border:

> <p style="text-align: center;">
>   <strong>Aero Black</strong>
> </p>

Those who like dark theme then this theme for them. This gives you Dark Taskbar and StartMenu But Borders are of Aero flavour:

<img class="alignnone size-full wp-image-1266" title="AeroBlack" src="http://askpk123.tk/wp-content/uploads/2012/07/AeroBlack.png" alt="" width="710" height="399" />

> <span style="text-decoration: underline;"><strong><a href="http://www.mediafire.com/download.php?exqy3h8891eoc6m" target="_blank">Download Aero Black Theme for Windows XP</a></strong></span>

> <p style="text-align: center;">
>   <strong>Aero Green</strong>
> </p>

Those who love nature related stuff, then this is what you must have, Aero Green Theme for Windows XP:

<img class="alignnone size-full wp-image-1267" title="Aero Green" src="http://askpk123.tk/wp-content/uploads/2012/07/Aero-Green.png" alt="" width="714" height="401" />

> <span style="text-decoration: underline;"><strong><a href="http://www.mediafire.com/download.php?vsgm2d4gck5allt" target="_blank">Download Aero Green Theme for Windows XP</a></strong></span>

> <p style="text-align: center;">
>   <strong>Aero Pink</strong>
> </p>

Yet another lovely flavour of Aero, here is a screenshot of this theme in action:

<img class="alignnone size-full wp-image-1270" title="AeroPink" src="http://askpk123.tk/wp-content/uploads/2012/07/AeroPink.png" alt="" width="710" height="399" />

> <a href="http://www.mediafire.com/download.php?3yj45vabtejz5ti" target="_blank"><span style="text-decoration: underline;"><strong>Download Aero Pink Theme for Windows XP</strong></span></a>

Thats it for now, More theme will come after sometime. <img src="http://www.askprateek.tk/wp-includes/images/smilies/icon_smile.gif" alt=":)" class="wp-smiley" />

 [1]: http://www.askprateek.tk//download-windows-xp-dark-edition-theme/ "Download Windows XP Dark Edition Rebirth Refix V7 Theme"
 [2]: http://www.askprateek.tk//download-windows-xp-darklite-v2-theme-for-xp-professional/ "Download Windows XP DarkLite V2 Theme for XP Professional"