---
title: Transform Windows XP into Windows XP DarkEdition V7 without using Transformation Pack.
author: Ask Prateek
layout: post
permalink: /transform-windows-xp-into-windows-xp-darkedition-v7-without-using-transformation-pack/
jabber_published:
  - 1321546500
  - 1321546500
  - 1321546500
  - 1321546500
  - 1321546500
  - 1321546500
  - 1321546500
  - 1321546500
  - 1321546500
  - 1321546500
  - 1321546500
  - 1321546500
  - 1321546500
  - 1321546500
  - 1321546500
  - 1321546500
email_notification:
  - 1321546504
  - 1321546504
  - 1321546504
  - 1321546504
  - 1321546504
  - 1321546504
  - 1321546504
  - 1321546504
  - 1321546504
  - 1321546504
  - 1321546504
  - 1321546504
  - 1321546504
  - 1321546504
  - 1321546504
  - 1321546504
yourls_shorturl:
  - http://ask-pra.tk/1q
  - http://ask-pra.tk/1q
  - http://ask-pra.tk/1q
  - http://ask-pra.tk/1q
categories:
  - Resource Hacker
  - Visual Styles
  - Windows XP
tags:
  - customization
  - dark
  - edition
  - pack without
  - rebirth
  - refix
  - transform
  - v7
  - Xp
---
<!--more-->

> <span style="color: #000000;"><strong>Note:</strong> We will update this Article whenever we found a new GUI in Windows XP DarkEdition, so keep checking this article regularly to get updates.</span>

The most interesting thing in **Windows XP DarkEdition V7** is its look, new icons, cursors, theme, sounds, login screen, boot screen, etc.

If you are still using Windows XP Professional but want to enjoy the new Windows DarkEdition look, then this tutorial will definitely help you.

After following this tutorial, you&#8217;ll get the same Windows DarkEdition look in your Windows XP. Following is a list of things which are going to change after following this tutorial:

  * **Windows Theme **or** Visual Style**
  * **Icons**
  * **Boot Screen**
  * **Login Screen **or** Welcome Screen**
  * **Sounds**
  * **Cursors**
  * ****System Properties Dialog Box****
  * ****Windows Media Player 11 Mod****
  * **Dark Edition Drive Icons**

> <p style="text-align: center;">
>   <strong>1. Windows Theme </strong>or<strong> Visual Style:</strong>
> </p>

First thing which you should change is XP theme. You can get Windows DarkEdition look in XP using &#8220;DarkEdition Rebirth Refix V7 &#8221; theme:

![][1]

<span style="text-decoration: underline;"><strong><a title="Download Windows XP Dark Edition Rebirth Refix V7 Theme" href="http://www.askprateek.tk//download-windows-xp-dark-edition-theme/" target="_blank">Download Windows XP DarkEdition Rebirth Refix V7 Theme</a><br /> </strong></span>

> <p style="text-align: center;">
>   <strong><strong>2. Icons:</strong></strong>
> </p>

Here is an Icon Pack for Windows XP, which will change XP default icons with Windows DarkEdition icons. So you’ll experience the high-resolution DarkEdition icons in XP:

![][2]

<a title="[DarkVersions] Download Windows XP DarkPK Icons for XP." href="http://www.askprateek.tk//darkversions-download-windows-xp-darkpk-icons-for-xp/" target="_blank"><strong><span style="text-decoration: underline;">Download Windows XP DarkPK Icons for Windows </span></strong></a>

> <p style="text-align: center;">
>   <strong>3. Boot Screen</strong>
> </p>

In Windows XP Darkedition, the boot screen is similar to the one in Windows Vista. You can download it from the following link:

<span style="text-decoration: underline;"><strong><a href="http://www.deviantart.com/download/251237740/windows_vista_and_windows_7_boot_screens_for_xp_by_vishal_gupta-d45kwa4.zip" target="_blank">Download Windows XP DarkEdition Boot screen</a></strong></span>

Use Tuneup Utilities to apply it. Its completely safe.

> <p style="text-align: center;">
>   <strong>4. Logon Screen</strong>
> </p>

Windows XP Dark Edition contain a cool Logon screen. Here is a screenshot of the logon screen.

<img class="alignnone" alt="" src="http://fc06.deviantart.net/fs71/i/2012/227/7/7/windows_xp_darkedition_v7_login_screen_by_prateek_kumar-d566891.png" width="614" height="461" />

You can download this login screen from the following link:

<span style="text-decoration: underline;"><strong><a title="Download Windows XP Dark Edition logon screen for XP" href="http://www.askprateek.tk//download-windows-xp-dark-edition-logon-screen-for-xp/" target="_blank">Download Windows XP DarkEdition Logon screen</a></strong></span>

> <p style="text-align: center;">
>   <strong>4.Sounds</strong>
> </p>

DarkEdition contains new sounds, which are quite nice. You can download the whole pack from here:

<span style="text-decoration: underline;"><strong><a href="http://media.askvg.com/files/VistaSounds.zip" target="_blank">Download DarkEdition Sounds</a></strong></span>

Just open &#8220;**Sounds & Audio Devices**&#8221; (mmsys.cpl) in Control Panel and goto &#8220;**Sounds**&#8221; tab and here you can replace windows default sounds with the new ones.

> <p style="text-align: center;">
>   <strong>6. Cursors</strong>
> </p>

Windows XP DarkEdition Icon Pack will change your default cursors with the DarkEdition one. But if you want to download all the cursors of DarkEdition then here is a link:

**<span style="text-decoration: underline;"><a href="http://media.askvg.com/files/Windows_Vista_and_7_Cursors_for_XP.zip" target="_blank">Download Cursors</a> </span>(They are similar to the one in Windows Vista and 7)**

> <p style="text-align: center;">
>   <strong>7. System Properties Dialog Box</strong>
> </p>

Another major change in Windows XP Dark Edition V7 is the new System Properties Dialog box with Dark touch. Here is a screenshot:

<img class="alignnone" alt="" src="http://fc08.deviantart.net/fs71/i/2012/188/6/c/windows_xp_dark_edition_v_7_system_properties_by_prateek_kumar-d56al6s.png" width="663" height="403" />

<span style="text-decoration: underline;"><strong><a title="[ Dark Edition Version ] Customizing System Properties dailog Box [sysdm.cpl] in Windows XP" href="http://www.askprateek.tk/dark-edition-version-customizing-system-properties-dailog-box-sysdm-cpl-in-windows-xp/" target="_blank">Get Windows XP Dark Edition System Properties Dialog Box</a></strong></span>

> <p style="text-align: center;">
>   <strong>8. Windows Media Player 11 Mod</strong>
> </p>

Windows XP Dark Edition V7 comes with WMP 12 look which looks Awesome:

<img class="alignnone" alt="" src="http://fc06.deviantart.net/fs71/i/2012/188/3/f/windows_xp_dark_edition_media_player_theme_by_prateek_kumar-d56c2pj.png" width="630" height="434" />

You can download the required skin using following link:

> **<a title="Get Windows XP Dark Edition look-a-like Windows Media Player for XP Professional" href="http://www.askprateek.tk//get-windows-xp-dark-edition-look-a-like-windows-media-player-for-xp-professional/" target="_blank">Download Windows Xp Dark Edition Skin for WMP11  of Windows XP</a>**

> <p style="text-align: center;">
>   <strong>8. Windows XP Dark Edition Theme for VistaDrive Icon</strong>
> </p>

Drive icon theme in Dark edition is awesome. We like it very much. Hope the same for you:

<img class="alignnone" alt="" src="http://fc00.deviantart.net/fs70/i/2012/188/5/9/windows_xp_dark_edition_v7_drive_icon_theme_by_prateek_kumar-d56bwse.png" width="635" height="316" />

You can enjoy the look of These icons in your PC by Downloading it From The following link:

> **<span style="text-decoration: underline;"><a title="Download it" href="http://www.deviantart.com/download/312964286/7882814bd96341b3be3cb85cdb1308ab-d56bwse.rar" target="_blank">Download Windows XP Dark Edition Theme for VistaDrive Icon</a></span> **| <span style="text-decoration: underline;"><strong><a href="http://prateek-kumar.deviantart.com/art/Windows-XP-Dark-Edition-V7-Drive-Icon-Theme-312964286" target="_blank">Mirror</a></strong></span>

 [1]: http://askprateek.tk/wp-content/uploads/2011/09/Windows-XP-Dark-Edition-Theme.png
 [2]: http://fc04.deviantart.net/fs70/i/2012/168/0/1/windows_xp_darkpk_icons_for_xp_by_prateek_kumar-d53vv74.png