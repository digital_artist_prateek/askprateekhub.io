---
title: 'TweakUI: Microsoft Powertoy to customize Windows'
author: Ask Prateek
layout: post
permalink: /tweakui-microsoft-powertoy-to-customize-windows/
jabber_published:
  - 1317398771
  - 1317398771
  - 1317398771
  - 1317398771
  - 1317398771
  - 1317398771
  - 1317398771
  - 1317398771
  - 1317398771
  - 1317398771
  - 1317398771
  - 1317398771
  - 1317398771
  - 1317398771
  - 1317398771
  - 1317398771
tagazine-media:
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
  - 'a:7:{s:7:"primary";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:6:"images";a:3:{s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-1.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-2.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"225036";s:9:"file_path";s:0:"";}s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";a:6:{s:8:"file_url";s:60:"http://thewindexpl.files.wordpress.com/2011/09/tweakui-3.jpg";s:5:"width";s:3:"532";s:6:"height";s:3:"424";s:4:"type";s:5:"image";s:4:"area";s:6:"225568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-30 16:06:10";}'
yourls_shorturl:
  - http://ask-pra.tk/1g
  - http://ask-pra.tk/1g
  - http://ask-pra.tk/1g
  - http://ask-pra.tk/1g
categories:
  - Softwares
  - Windows XP
tags:
  - 2.0
  - 2.1
  - 2003
  - microsoft
  - powertoy
  - server
  - tool.to.customize
  - tweakui
  - ultimate
  - windows
  - Xp
---
<!--more-->

**TWEAKUI** is one of the most known **Windows Powertoys/Powertools** (issued but not supported by**Microsoft**) to tweak the User Interface (UI). With this tool you are able to change settings, which aren&#8217;t reachable within Windows. As an example you can make changes to the desktop and start menu, which are not within reach unless you know how to make changes in the Windows registry.

The past time many (free and commercial) tweak applications have seen the light. Most of them have even more options then the **Microsoft TWEAKUI tool**. Essentially they all work the same, but with different interfaces. On this page I discuss the **TWEAKUI** tool, this tool doesn&#8217;t give many troubles when used improperly..

> <p style="text-align: center;">
>   <strong>Using TWEAKUI: moving personal data</strong>
> </p>

After the **TWEAKUI** setup, you will find it in the Start menu, **All Programs**, **Powertoys for Windows XP**. By navigating at the left side, you are able to reach the different subjects you can make changes to, the most important ones will be described here. Below you see the **Special Folders** settings:

<img class="alignnone size-full wp-image-1928" title="TweakUI 1" src="http://www.askprateek.tk/wp-content/uploads/2011/09/TweakUI-1.jpg" alt="" width="532" height="424" />

With this function you can make changes to the location of the different personal folders like **Favorites, My Documents, My Pictures** and **My Music** (before you make use of this option, you will have to create those folders in the Windows Explorer). This option is very useful when you plan to make a system backup: you rather place your important personal data on a different partition, to make sure not to lose important data at a recovery of your Windows. In above example you see that the location of the **Favorites** (Internet Explorer) are located on the M: partition. If you make use of shared folders (between different user accounts), you should move your shared folders as well.

<span style="color: #c60000;"><strong>TIP:</strong></span> Actually you don&#8217;t need the **TWEAKUI** tool for moving your personal data. I prefer to move the **My Documents** folder (sub-folders included) in the **Windows Explorer**. You can realize this by right clicking the **My Documents** folder followed by **Properties**. With the Move button you can move the whole **My Documents** folder to another location (before you perform this operation create a new destination folder). All files will be moved to the new destination and the registry keys will be modified. The **Favorites** folder can be moved by a cut-paste operation (CTRL-X/CTRL-V)! I prefer to paste the **Favorites** folder in the new **My Documents** folder. Of course you have to perform those operations for every user account!

> <p style="text-align: center;">
>   <strong>Using TWEAKUI: hiding drives</strong>
> </p>

The tab **Drives** (see **My Computer**) makes it possible to hide drives in the **Windows Explorer**. The drives will still have a drive letter (which you can use in the command console), but won&#8217;t be visible in the **Windows Explorer** (you can use this option if you have a special partition for the pagefile). To make drives not accessible, you better use the **Disk Management**-tool from Windows to hide drives (**Control Panel, Administrative Tools, Computer Management, Disk Management**).

> <p style="text-align: center;">
>   <strong>Using TWEAKUI: AutoPlay drives</strong>
> </p>

The tab **MyComputer, AutoPlay** gives you the opportunity to manage the automatic playing of CD/DVD drives.

> <p style="text-align: center;">
>   <strong>Using TWEAKUI: AutoLogon</strong>
> </p>

The tab **Logon, AutoLogon** gives you the opportunity to automatically logon a specific user at next reboot.

> <p style="text-align: center;">
>   <strong>Using TWEAKUI: Showing unread mail on Welcome screen</strong>
> </p>

The tab **Logon, Unread Mail** gives you the opportunity to disable the notification of unread mail in the Welcome screen.

> <p style="text-align: center;">
>   <strong>Using TWEAKUI: Explorer adjustments</strong>
> </p>

Another interesting tab, is the **Explorer** tab. As you can see below, there are many settings you can change.

<img class="alignnone size-full wp-image-1929" title="TweakUI 2" src="http://www.askprateek.tk/wp-content/uploads/2011/09/TweakUI-2.jpg" alt="" width="532" height="423" />

I always disable the option **Prefix &#8220;Shortcut to&#8221; on new shortcuts.** This option is not necessary (more irritating): the shortcut arrow is more then enough information. The other options are at your own choice (the shown information is enough to make your own decision).

> <p style="text-align: center;">
>   <strong>Using TWEAKUI: enable/disable (visual) effects</strong>
> </p>

On the tab **General**, you can enable/disable (visual) effects, I advise to disable them all (except the **Enable tooltip animation**) to have maximum Windows performance (you won&#8217;t miss anything):

<img class="alignnone size-full wp-image-1930" title="TweakUI 3" src="http://www.askprateek.tk/wp-content/uploads/2011/09/TweakUI-3.jpg" alt="" width="532" height="424" />

> <p style="text-align: center;">
>   <strong>Using TWEAKUI: Desktop, Taskbar and Start menu</strong>
> </p>

The tab **Taskbar** gives you the chance to disable the irritating balloon tips (the option **Enable Balloon tips)**, the balloon tips sometimes cover essential Start menu buttons. Disabling the option **Warn when low on disk space** can also be a relief. The tab **Desktop** gives you the opportunity to remove undesirable shortcuts from your desktop.

> Tweak UI Version 2.10 requires **Windows XP Service Pack 1 or above / Windows Server 2003**. If you have a Windows XP system without any Service Packs installed, download Version 2.00.
> 
> **<span style="text-decoration: underline;"><a href="http://download.microsoft.com/download/whistler/Install/2/WXP/EN-US/TweakUiPowertoySetup.exe">Download TweakUI 2.00</a></span> | <span style="text-decoration: underline;"><a href="http://download.microsoft.com/download/f/c/a/fca6767b-9ed9-45a6-b352-839afb2a2679/TweakUiPowertoySetup.exe">Download TweakUI 2.10</a></span>**