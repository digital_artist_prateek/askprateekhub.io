---
title: How to stop WordPress asking for FTP Details when updating/installing plugins
author: Ask Prateek
layout: post
permalink: /how-to-stop-wordpress-asking-for-ftp-details-when-updatinginstalling-plugins/
categories:
  - WordPress
tags:
  - asking
  - details
  - ftp
  - stop
  - wordpress
---
<!--more-->

Hello Everyone !

Many WordPress user face a weird problem when the try to upgrade or Install any plugin. WordPress ask&#8217;s for FTP details of you server and you have to provide them each time when you do anything related to FTP:

[<img class="alignnone size-full wp-image-2227" alt="Wordpress FTP Details" src="http://www.askprateek.tk/wp-content/uploads/2013/01/Wordpress-FTP-Details-e1357731889799.png" width="600" height="263" />][1]

&nbsp;

When automatically upgrading, installing or updating a plugin or the WordPress core from the Admin Dashboard, WordPress needs to make changes to the file system (your web hosted site).

Before making the changes, it first needs to check whether it has the correct permissions to do this. If it doesn’t, you will be prompted to enter your FTP user account information

So Toady I am going to provide you a small working solution of this problem. All you have to do is edit your **wp-config.php** file which is found in root of you wordpress installation

Now paste this code in your **wp-config.php **:

> /\*\* Setup FTP Details \*\*/  
> define(&#8220;FTP_HOST&#8221;, &#8220;**localhost**&#8220;);  
> define(&#8220;FTP_USER&#8221;, &#8220;**your-ftp-username**&#8220;);  
> define(&#8220;FTP_PASS&#8221;, &#8220;**your-ftp-password**&#8220;);

Now replace the part in bold with the appropriate details which can be found in your hosting panel

*Hope this helps you if it does then it is recommended to share this article so that other can also learn from it. If you have any doubt then feel free to comment here*

 [1]: http://www.askprateek.tk