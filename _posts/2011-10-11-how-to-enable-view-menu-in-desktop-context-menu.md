---
title: 'How to enable &#8220;View&#8221; Menu in Desktop Context Menu'
author: Ask Prateek
layout: post
permalink: /how-to-enable-view-menu-in-desktop-context-menu/
jabber_published:
  - 1318348145
  - 1318348145
  - 1318348145
  - 1318348145
  - 1318348145
  - 1318348145
  - 1318348145
  - 1318348145
  - 1318348145
  - 1318348145
  - 1318348145
  - 1318348145
  - 1318348145
  - 1318348145
  - 1318348145
  - 1318348145
yourls_shorturl:
  - http://ask-pra.tk/1v
  - http://ask-pra.tk/1v
  - http://ask-pra.tk/1v
  - http://ask-pra.tk/1v
sfw_comment_form_password:
  - yES5EkLWm6um
  - yES5EkLWm6um
  - yES5EkLWm6um
  - yES5EkLWm6um
categories:
  - Resource Hacker
  - Windows XP
tags:
  - context
  - desktop
  - enable
  - menu
  - Resource Hacker
  - view
  - windows
  - Xp
---
<!--more-->

> **NOTE 1:** After applying this tweak the &#8220;View&#8221; button on Toolbar will stop working.
> 
> **NOTE 2:** If you are facing problems while saving the file after editing in resource hacker, then make sure you have disabled WFP <span style="color:#000000;">(Windows File Protection)</span> service using WFP Patcher, it can be found in our &#8220;**<a title="Downloads" href="http://www.askprateek.tk/downloads/" target="_blank">Download</a>**&#8221; section.
> 
> Also if you are getting** error &#8220;Can&#8217;t create file&#8230;**&#8220;, that means you have edited and saved the file in past and there is a backup file which needs to be deleted before saving this file again. Go to &#8220;**System32**&#8221; folder and you&#8217;ll see a file having &#8220;***shell32_original.dll***&#8220;. Delete it and try to save the file in resource hacker.

In this Tutorial we will teach you how to Enable View Menu in Windows XP Desktop Context Menu just like Windows 7. You will need Resource hacker for this task. You can download it from our &#8220;**<span style="text-decoration:underline;"><a title="Downloads" href="http://www.askprateek.tk/downloads/" target="_blank">Download</a></span>**&#8221; Section.

<img class="size-full wp-image-582 alignnone" title="How to enable &quot;VIEW&quot; menu in Desktop Context menu  " src="http://thewindexpl.files.wordpress.com/2011/10/final.png" alt="" width="300" height="281" />

<p style="text-align:left;">
  <strong>1. </strong>Open <strong>%windir%System32Shell32.dll</strong> file in Resource Hacker.
</p>

**2. **Goto: **Menu -> 215 -> 1033**.

**3. **In right-side [<span style="color:#f58220;">pane</span>][1]{#KonaLink1}, replace the line saying:

> POPUP &#8220;&View&#8221;, **28674**, MFT\_STRING, MFS\_ENABLED, 0

**to any of these:**

> <p style="text-align:left;">
>   POPUP &#8220;&View&#8221;, <strong>28675</strong>, MFT_STRING, MFS_ENABLED, 0
> </p>
> 
> <p style="text-align:left;">
>   <span style="text-decoration:underline;"><strong>OR</strong></span>
> </p>
> 
> <p style="text-align:left;">
>   POPUP &#8220;&View&#8221;, <strong></strong>, MFT_STRING, MFS_ENABLED, 0
> </p>
> 
> <p style="text-align:left;">
>   <span style="text-decoration:underline;"><span style="text-decoration:underline;"><strong>OR</strong></span></span>
> </p>
> 
> <p style="text-align:left;">
>   POPUP &#8220;&View&#8221;
> </p>

i.e., you have to only change the ID **28674** to ****. It can also be done by deleting the text after &#8220;&View&#8221; and after compilation resource hacker will automatically add the remaining part.

**4.** Now click on **Compile Script** button and save the file.

Now restart your pc then you will see View button in your Dektop Context menu.

<span style="text-decoration:underline;color:#000000;"><strong>Also Check:</strong></span>

<span style="text-decoration:underline;"><strong><strong><a title="How to Write your name in Desktop Context Menu" href="http://www.askprateek.tk/2011/10/11/how-to-write-your-name-in-desktop-context-menu/">How to Write your name in Desktop Context Menu</a></strong></strong></span>

<span style="text-decoration:underline;"><strong><a title="FileMenu Tools: A free tool to customize Explorer and Desktop Context menu in Windows XP, Vista and 7" href="http://www.askprateek.tk/2011/10/07/filemenu-tools-a-free-tool-to-customizer-context-menu-in-windows-xp-vista-and-7/">FileMenu Tools: A free tool to customize Explorer and Desktop Context menu in Windows XP, Vista and 7</a></p> 

<p>
  </strong></span>
</p>

<p>
  &nbsp;
</p>

 [1]: http://www.askvg.com/how-to-enable-view-menu-on-desktop-in-windows-xp/#