---
title: 'NPointer: Control your Mouse Pointer with your Hand or Head'
author: Ask Prateek
layout: post
permalink: /npointer-control-mouse-pointer-hand-head/
yourls_shorturl:
  - http://ask-pra.tk/4
sfw_comment_form_password:
  - WDXXiWJxqE3Q
categories:
  - Softwares
  - Windows 8
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - control
  - guesture
  - hand
  - head
  - mouse
  - npointer
  - pointer
  - with
---
<!--more-->

Hello Everyone!

While browsing, we find an awesome tool which allows you to control your mouse pointer with your head or hand. This is a very useful software if your Mouse is not working or if you want to fool your friends

![NPointer Settings][1]

It works on Windows XP, Vista, Seven and 8. You can download it from the following link:

> <span style="text-decoration: underline;"><strong><a href="http://www.neurotechnology.com/npointer.html" target="_blank">Download NPointer</a></strong></span>

 [1]: http://cloud.addictivetips.com/wp-content/uploads/2012/08/NPointer-Settings.jpg