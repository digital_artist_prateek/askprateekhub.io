---
title: How to Remove shortcut arrow from Desktop icons.
author: Ask Prateek
layout: post
permalink: /how-to-remove-shortcut-arrow-from-desktop-icons/
jabber_published:
  - 1317469720
  - 1317469720
  - 1317469720
  - 1317469720
  - 1317469720
  - 1317469720
  - 1317469720
  - 1317469720
  - 1317469720
  - 1317469720
  - 1317469720
  - 1317469720
  - 1317469720
  - 1317469720
  - 1317469720
  - 1317469720
yourls_shorturl:
  - http://ask-pra.tk/1u
  - http://ask-pra.tk/1u
  - http://ask-pra.tk/1u
  - http://ask-pra.tk/1u
categories:
  - Softwares
  - Troubleshooting
  - Windows XP
tags:
  - arrow
  - desktop
  - form
  - hide
  - icons
  - registry method
  - shortcut
  - tweakui
---
<!--more-->

Almost all of us want to remove that shortcut arrow from the Desktop icons. It destroy&#8217;s the beauty of Desktop. So, I decided to share some tricks to remove that arrow for Desktop icons.

> <p style="text-align: center;">
>   <strong>Method 1 [Registry Editor]</strong>
> </p>

Before making any changes in Windows Registry you must create a backup of Registry.

First Type** regedit** in Run and navigate to the following key:

> **HKEY\_LOCAL\_MACHINE\SOFTWARE\Microsoft\Windows CurrentVersion\Explorer\Shell Icons**

If the does not exist then** Right click Explorer &#8211;>New&#8211;>Key** and name the key as **Shell Icons. **Now, we have to create a string key. For this Right Click **Shell Icons&#8211;>New&#8211;>String Value **and name it as **&#8220;29&#8221;.**

Now Double click this key and set it&#8217;s value as:

> **C:WINDOWSsystem32shell32.dll,52**

<img class="alignnone size-full wp-image-1944" title="Registry trick" src="http://www.askprateek.tk/wp-content/uploads/2011/10/Registry-trick.jpg" alt="" width="600" height="363" />

**Note :** If you have installed windows XP in diff. drive then Change **C** to that **drive letter.**Now exit Registry Editor and restart your Machine.

> <p style="text-align: center;">
>   <strong>Method 2 Using TweakUI [Simple and Safe]</strong>
> </p>

If you want a safe alternative then you can use TweakUI , a Powertoy form Microsoft. Using TweakUI to change the shortcut arrow icon is more safer. Plus, it offers another special icon Light Arrow (icon within TweakUI.exe). This gives the user an extra option to set the shortcut arrows to Light Arrow style, without completely removing the arrow.

[<span style="text-decoration: underline;"><strong>Download TweakUI, An Ultimate Powertoy form Microsoft.</strong></span>][1]

Open TweakUI, click Explorer, click Shortcut. Choose **Light Arrow** or **None**.

<img class="alignnone size-full wp-image-1943" title="Hidden Shortcut using TweakUI" src="http://www.askprateek.tk/wp-content/uploads/2011/10/Hidden-Shortcut-using-TweakUI.jpg" alt="" width="532" height="424" />

&nbsp;

**<span style="color: #ff0000;">Warning:</span>** If you choose **None**, you cannot differentiate between the shortcut and the actual file/folder. You may end-up copying a shortcut to a file in situations where you need the original file. Hence, setting the arrow to **None** is not advisable. The same warning applies for **Method 1**.

If you are a Windows Vista/Se7en user then you can also Remove Shortcut Overlay by following this link:

<span style="text-decoration: underline;"><strong><a href="http://www.askprateek.tk/how-to-remove-shortcut-arrow-from-desktop-icons-in-vista-and-seven/" rel="bookmark" target="_blank">How to remove Shortcut Arrow from Desktop icons in Vista and Seven</a></strong></span>

 [1]: http://www.askprateek.tk/tweakui-microsoft-powertoy-to-customize-windows/ "TweakUI: Microsoft Powertoy to customize Windows"