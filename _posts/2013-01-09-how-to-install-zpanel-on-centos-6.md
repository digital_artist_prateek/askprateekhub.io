---
title: 'How to install ZPanel on Centos 6  on VPS or Dedicated Server'
author: Ask Prateek
layout: post
permalink: /how-to-install-zpanel-on-centos-6/
categories:
  - VPS
tags:
  - 6
  - centos
  - install
  - zpanel
---
<!--more-->

If you are buying a dedicated server or a VPS, most of us require a control panel which helps to manage the server and sites hosted on it. Even if you are an experienced user, you may still want to use the control panel because for more convenience. Normally our choice is reduced to free web hosting control panels and commercial web hosting control panels such as cPanel, Plesk, DirectAdmin, etc. If you don’t mind about the habits and want to save some money, then you should consider installing a free control panel.

In this article I will tell you about one interesting free control panel which is Zpanel and I will show you how to install it on a Dedicated/VPS server with Centos 6x.

**What is Zpanel ?**

Zpanel is an open panel just like Cpanel which allows you to manage you Domains, Hosting Company etc.

<img class="alignnone size-full wp-image-2222" alt="Zpanel" src="http://www.askprateek.tk/wp-content/uploads/2013/01/Zpanel.jpg" width="550" height="374" />

**How to Install ?**

**1. **First Re install Centos 6 on you server and login to your Server as Root user via SSH client like <a title="Putty Download link" href="http://www.putty.org/" target="_blank">Putty</a>.

**2. **Now give the following commands one by one in Putty. (Copy these command and right click on Putty interface to paste these command)

> mkdir /etc/zpanel
> 
> wget http://blog.fluidhosting.com/wp-content/uploads/2012/09/zpanel-install-kit.zip
> 
> unzip zpanel-install-kit.zip
> 
> cd zpanel-install-kit
> 
> chmod 777 centos-6-2-1.00.sh
> 
> ./centos-6-2-1.00.sh

Now you are done. After sometime the installation will complete and it will reboot the server automatically.

**Your login information**:

URL: your-ip-address  
Username: zadmin  
Password: password