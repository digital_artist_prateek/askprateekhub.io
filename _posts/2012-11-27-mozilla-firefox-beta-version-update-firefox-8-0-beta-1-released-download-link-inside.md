---
title: '[Mozilla Firefox Beta Version Update] Firefox 18.0b1 released. Download link inside.'
author: Ask Prateek
layout: post
permalink: /mozilla-firefox-beta-version-update-firefox-8-0-beta-1-released-download-link-inside/
jabber_published:
  - 1317452356
  - 1317452356
  - 1317452356
  - 1317452356
  - 1317452356
  - 1317452356
  - 1317452356
  - 1317452356
  - 1317452356
  - 1317452356
  - 1317452356
  - 1317452356
  - 1317452356
  - 1317452356
  - 1317452356
  - 1317452356
yourls_shorturl:
  - http://ask-pra.tk/26
  - http://ask-pra.tk/26
  - http://ask-pra.tk/26
  - http://ask-pra.tk/26
categories:
  - Mozilla Firefox
tags:
  - 8
  - beta
  - features
  - firefox
  - mozilla
  - new
  - update
---
<!--more-->

The latest Mozilla Firefox Beta is now available for testing on Windows, Mac, Linux and Android. This beta includes performance enhancements that improve the browsing experience for users and enable developers to create faster Web apps and websites.

Mozilla has updated Firefox to **Beta 1**. Following are the new features in this version

<div>
  <ul>
    <li>
      First revision of the Social API and support for <a title="How to Enable Build-in Facebook Messanger in Mozilla Firefox" href="http://www.askprateek.tk/how-to-enable-build-in-facebook-messanger-in-mozilla-firefox/">Facebook Messenger</a>
    </li>
    <li>
      Click-to-play blocklisting implemented to prevent vulnerable plugin versions from running without the user&#8217;s permission
    </li>
    <li>
      Updated Awesome Bar experience with larger icons
    </li>
    <li>
      Mac OS X 10.5 is no longer supported
    </li>
    <li>
      JavaScript Maps and Sets are now iterable
    </li>
    <li>
      SVG FillPaint and StrokePaint implemented
    </li>
    <li>
      Improvements that make the Web Console, Debugger and Developer Toolbar faster and easier to use
    </li>
    <li>
      New Markup panel in the Page Inspector allows easy editing of the DOM
    </li>
    <li>
      Sandbox attribute for iframes implemented, enabling increased security
    </li>
    <li>
      Over twenty performance improvements, including fixes around the New Tab page (built-in tweak to <strong>customize number of rows and columns in new tab page</strong> ).
    </li>
    <li>
      Fixed: Pointer lock doesn&#8217;t work in web apps
    </li>
    <li>
      Fixed: Page scrolling on sites with fixed headers
    </li>
  </ul>
</div>

<div>
  <p>
    <img class="alignnone size-full wp-image-1936" title="Mozilla Beta Version Update" src="http://www.askprateek.tk/wp-content/uploads/2011/10/Mozilla-Beta-Version-Update.jpg" alt="" width="600" height="394" />
  </p>
</div>

You can download **Mozilla Firefox 18.0 Beta 1 Version ** from the following link:

> <span style="text-decoration: underline;"><strong><a title="Download Mozilla Firefox latest bete Version" href="http://www.mozilla.org/en-US/firefox/all-beta.html" target="_blank">Download Link</a> </strong></span>
> 
> <span style="text-decoration: underline;"><strong><a href="http://www.mozilla.com/en-US/firefox/7.0beta/releasenotes/" target="_blank">Release Notes</a></strong></span>