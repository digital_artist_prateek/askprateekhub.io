---
title: 'Fences: Organise your Desktop In a Cool Way.'
author: Ask Prateek
layout: post
permalink: /fences-organise-your-desktop-in-a-cool-way/
sfw_comment_form_password:
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
  - DIvmq8QTbVJG
yourls_shorturl:
  - http://ask-pra.tk/17
  - http://ask-pra.tk/17
  - http://ask-pra.tk/17
  - http://ask-pra.tk/17
socialize_text:
  - |
    <div id="presetListDiv">This article was posted by <a href="http://www.askpk123.tk">Prateek Kumar</a> in following section:  [post_list name="categories"]</div>
    <div>
    
    If you enjoyed this article, then <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Like us on Facebook</a> and <a title="Follow us on Twitter" href="http://twitter.com/AskPK123" target="_blank">Follow us on Twitter</a> so that you won't miss any of the great Article. Also you can checkout our <a title="Our DeviantART Gallery" href="http://prateek-kumar.deviantart.com/" target="_blank">DeviantART Gallery</a> and browse for some Art Work by Me.
    
    Thanks for Reading and It is requested to leave a comment if you want to Discuss something or simply say Thanks
    
    </div>
  - |
    <div id="presetListDiv">This article was posted by <a href="http://www.askpk123.tk">Prateek Kumar</a> in following section:  [post_list name="categories"]</div>
    <div>
    
    If you enjoyed this article, then <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Like us on Facebook</a> and <a title="Follow us on Twitter" href="http://twitter.com/AskPK123" target="_blank">Follow us on Twitter</a> so that you won't miss any of the great Article. Also you can checkout our <a title="Our DeviantART Gallery" href="http://prateek-kumar.deviantart.com/" target="_blank">DeviantART Gallery</a> and browse for some Art Work by Me.
    
    Thanks for Reading and It is requested to leave a comment if you want to Discuss something or simply say Thanks
    
    </div>
socialize:
  - 
  - 
categories:
  - Softwares
  - Windows Vista
tags:
  - desktop
  - fences
  - organise
  - seven
  - stardock
  - vista
  - Xp
---
<address>
  <!--more-->
</address>

<div>
  <div id="digit-review">
    <p>
      Fences is a great application that allows you to “fence” off the items in your desktop giving you a better organized and cleaner desktop where it is much easier to find the icon you are looking for. If you want to send your desktop icons to their own corners and have them stay there, this tool will be incredible useful.While it is not a tool for managing multiple displays it can be quite useful for managing desktop icons across multiple displays.
    </p>
    
    <p>
      <img class="alignnone" title="Fences" src="http://cfile8.uf.tistory.com/image/15056E4D4D5D2BC92E8C43" alt="" width="614" height="384" />
    </p>
    
    <p>
      The desktop is a convenient dumping ground for your documents, downloads, and files copied of your friends’ pendrives. Many software setups create one or more icons on your desktop (by default) after installing. A computer you’ve long enough will probably have a rather large random collection of application shortcuts, documents, images, videos, urls, folders etc.<br /> I remember a long while ago, making  wallpapers with nearly marked boundaries for different kinds of icons. The only problem was that the icons refused to stay in their places, and sooner or later I would give up on this system of organization.<br /> Fences can organize this mess, by creating —as the application’s name suggests— “fences” that border collections of icons. You can section off different areas on your desktop and assign them different names, and any icons which you add into these sections stay within their “fences”.
    </p>
    
    <p>
      Fences not only ensures that each section of the desktop remains separate, but also makes each individual fenced region scrollable and sortable by its own rules.
    </p>
    
    <p>
      You can create fences with any criteria that you want, since the organization can be done manually by the user themselves, however the sorting of icons by user-defined rules can be automated as well.
    </p>
    
    <blockquote>
      <p>
        <a style="text-decoration: underline; font-weight: bold;" href="http://download.cnet.com/Fences/3000-2072_4-10909535.html">Download Fences</a> | <span style="text-decoration: underline;"><strong><a href="http://www.stardock.com/products/fences/">Homepage</a></strong></span>
      </p>
    </blockquote>
  </div>
</div>