---
title: 'Windows 7 StartORB Changer: A small utility to change Start button image of Windows 7'
author: Ask Prateek
layout: post
permalink: /windows-7-startorb-changer-a-small-utility-to-change-start-button-image-of-windows-7/
sfw_comment_form_password:
  - c8hclIvyhY6a
  - c8hclIvyhY6a
  - c8hclIvyhY6a
  - c8hclIvyhY6a
jabber_published:
  - 1341494986
  - 1341494986
  - 1341494986
  - 1341494986
  - 1341494986
  - 1341494986
  - 1341494986
  - 1341494986
  - 1341494986
  - 1341494986
  - 1341494986
  - 1341494986
  - 1341494986
  - 1341494986
  - 1341494986
  - 1341494986
tagazine-media:
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:6:"images";a:2:{s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/07/w7soc.png";s:5:"width";s:3:"426";s:6:"height";s:3:"325";s:4:"type";s:5:"image";s:4:"area";s:6:"138450";s:9:"file_path";s:0:"";}s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";a:6:{s:8:"file_url";s:61:"http://thewindexpl.files.wordpress.com/2012/07/start-orbs.png";s:5:"width";s:3:"297";s:6:"height";s:3:"244";s:4:"type";s:5:"image";s:4:"area";s:5:"72468";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-05 13:29:43";}'
email_notification:
  - 1341494987
  - 1341494987
  - 1341494987
  - 1341494987
  - 1341494987
  - 1341494987
  - 1341494987
  - 1341494987
  - 1341494987
  - 1341494987
  - 1341494987
  - 1341494987
  - 1341494987
  - 1341494987
  - 1341494987
  - 1341494987
yourls_shorturl:
  - http://ask-pra.tk/j
  - http://ask-pra.tk/j
  - http://ask-pra.tk/j
  - http://ask-pra.tk/j
categories:
  - Softwares
  - Windows Se7en
tags:
  - 7
  - button
  - changer
  - how
  - image
  - orb
  - start
  - to change
  - windows
---
<!--more-->

Hello Everyone!

Today we found a cool application, which allows you to quickly change your Start Button(ORB) in Windows 7. Here is a screenshot showing what you can do via that application

<img class="alignnone size-full wp-image-1343" title="Start Orbs" src="http://askpk123.tk/wp-content/uploads/2012/07/Start-Orbs.png" alt="" width="297" height="244" /><img class="alignnone  wp-image-1344" title="W7SOC" src="http://askpk123.tk/wp-content/uploads/2012/07/W7SOC.png" alt="" width="298" height="227" />

You can Download this tool from the following link:

> <a href="http://www.mediafire.com/?3cft4v3lowmab97" target="_blank"><span style="text-decoration: underline;"><strong>Download Windows 7Start ORB Changer</strong></span></a>

Many Orb Samples are included in the Download. Simply **Run this Application as an Administrator  **and change the orb by clicking on the ORB button.