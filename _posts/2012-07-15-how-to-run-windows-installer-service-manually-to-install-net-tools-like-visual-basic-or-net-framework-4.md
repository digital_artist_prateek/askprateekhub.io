---
title: How to Run Windows Installer Service manually to install .net tools like Visual Basic or Net Framework 4
author: Ask Prateek
layout: post
permalink: /how-to-run-windows-installer-service-manually-to-install-net-tools-like-visual-basic-or-net-framework-4/
jabber_published:
  - 1342365824
  - 1342365824
  - 1342365824
  - 1342365824
  - 1342365824
  - 1342365824
  - 1342365824
  - 1342365824
email_notification:
  - 1342365825
  - 1342365825
  - 1342365825
  - 1342365825
  - 1342365825
  - 1342365825
  - 1342365825
  - 1342365825
tagazine-media:
  - 'a:7:{s:7:"primary";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:6:"images";a:3:{s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";a:6:{s:8:"file_url";s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";s:5:"width";s:3:"413";s:6:"height";s:3:"212";s:4:"type";s:5:"image";s:4:"area";s:5:"87556";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:5:"width";s:3:"712";s:6:"height";s:3:"414";s:4:"type";s:5:"image";s:4:"area";s:6:"294768";s:9:"file_path";s:0:"";}s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";a:6:{s:8:"file_url";s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";s:5:"width";s:3:"421";s:6:"height";s:3:"570";s:4:"type";s:5:"image";s:4:"area";s:6:"239970";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-16 06:00:10";}'
  - 'a:7:{s:7:"primary";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:6:"images";a:3:{s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";a:6:{s:8:"file_url";s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";s:5:"width";s:3:"413";s:6:"height";s:3:"212";s:4:"type";s:5:"image";s:4:"area";s:5:"87556";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:5:"width";s:3:"712";s:6:"height";s:3:"414";s:4:"type";s:5:"image";s:4:"area";s:6:"294768";s:9:"file_path";s:0:"";}s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";a:6:{s:8:"file_url";s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";s:5:"width";s:3:"421";s:6:"height";s:3:"570";s:4:"type";s:5:"image";s:4:"area";s:6:"239970";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-16 06:00:10";}'
  - 'a:7:{s:7:"primary";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:6:"images";a:3:{s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";a:6:{s:8:"file_url";s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";s:5:"width";s:3:"413";s:6:"height";s:3:"212";s:4:"type";s:5:"image";s:4:"area";s:5:"87556";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:5:"width";s:3:"712";s:6:"height";s:3:"414";s:4:"type";s:5:"image";s:4:"area";s:6:"294768";s:9:"file_path";s:0:"";}s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";a:6:{s:8:"file_url";s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";s:5:"width";s:3:"421";s:6:"height";s:3:"570";s:4:"type";s:5:"image";s:4:"area";s:6:"239970";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-16 06:00:10";}'
  - 'a:7:{s:7:"primary";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:6:"images";a:3:{s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";a:6:{s:8:"file_url";s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";s:5:"width";s:3:"413";s:6:"height";s:3:"212";s:4:"type";s:5:"image";s:4:"area";s:5:"87556";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:5:"width";s:3:"712";s:6:"height";s:3:"414";s:4:"type";s:5:"image";s:4:"area";s:6:"294768";s:9:"file_path";s:0:"";}s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";a:6:{s:8:"file_url";s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";s:5:"width";s:3:"421";s:6:"height";s:3:"570";s:4:"type";s:5:"image";s:4:"area";s:6:"239970";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-16 06:00:10";}'
  - 'a:7:{s:7:"primary";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:6:"images";a:3:{s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";a:6:{s:8:"file_url";s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";s:5:"width";s:3:"413";s:6:"height";s:3:"212";s:4:"type";s:5:"image";s:4:"area";s:5:"87556";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:5:"width";s:3:"712";s:6:"height";s:3:"414";s:4:"type";s:5:"image";s:4:"area";s:6:"294768";s:9:"file_path";s:0:"";}s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";a:6:{s:8:"file_url";s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";s:5:"width";s:3:"421";s:6:"height";s:3:"570";s:4:"type";s:5:"image";s:4:"area";s:6:"239970";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-16 06:00:10";}'
  - 'a:7:{s:7:"primary";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:6:"images";a:3:{s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";a:6:{s:8:"file_url";s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";s:5:"width";s:3:"413";s:6:"height";s:3:"212";s:4:"type";s:5:"image";s:4:"area";s:5:"87556";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:5:"width";s:3:"712";s:6:"height";s:3:"414";s:4:"type";s:5:"image";s:4:"area";s:6:"294768";s:9:"file_path";s:0:"";}s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";a:6:{s:8:"file_url";s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";s:5:"width";s:3:"421";s:6:"height";s:3:"570";s:4:"type";s:5:"image";s:4:"area";s:6:"239970";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-16 06:00:10";}'
  - 'a:7:{s:7:"primary";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:6:"images";a:3:{s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";a:6:{s:8:"file_url";s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";s:5:"width";s:3:"413";s:6:"height";s:3:"212";s:4:"type";s:5:"image";s:4:"area";s:5:"87556";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:5:"width";s:3:"712";s:6:"height";s:3:"414";s:4:"type";s:5:"image";s:4:"area";s:6:"294768";s:9:"file_path";s:0:"";}s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";a:6:{s:8:"file_url";s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";s:5:"width";s:3:"421";s:6:"height";s:3:"570";s:4:"type";s:5:"image";s:4:"area";s:6:"239970";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-16 06:00:10";}'
  - 'a:7:{s:7:"primary";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:6:"images";a:3:{s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";a:6:{s:8:"file_url";s:80:"http://thewindexpl.files.wordpress.com/2012/07/sevices-msc-in-run-dialog-box.png";s:5:"width";s:3:"413";s:6:"height";s:3:"212";s:4:"type";s:5:"image";s:4:"area";s:5:"87556";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2012/07/double-click-windows-installer.png";s:5:"width";s:3:"712";s:6:"height";s:3:"414";s:4:"type";s:5:"image";s:4:"area";s:6:"294768";s:9:"file_path";s:0:"";}s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";a:6:{s:8:"file_url";s:72:"http://thewindexpl.files.wordpress.com/2012/07/follow-the-screenshot.png";s:5:"width";s:3:"421";s:6:"height";s:3:"570";s:4:"type";s:5:"image";s:4:"area";s:6:"239970";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"3";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-16 06:00:10";}'
yourls_shorturl:
  - http://ask-pra.tk/m
  - http://ask-pra.tk/m
sfw_comment_form_password:
  - GE2Jh1JNuCkV
  - GE2Jh1JNuCkV
categories:
  - Troubleshooting
  - Windows 8
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - basic
  - framework
  - installer
  - manual
  - net
  - not
  - RUN
  - running
  - service
  - start
  - visual
  - windows
---
<!--more-->

We recently tried to install Visual Basic 2010 but we find a strange type of problem while installing Microsoft Net Framework 4 Multi-Targeting pack and other tools which install after it. It says** Windows Installer service isn&#8217;t Running. **So, today we will teach you how to turn it on safely.

**1.** First open Run dialog box by pressing [Win] + R  key and type **Services.msc .**

<img class="alignnone size-full wp-image-2247" alt="Sevices.msc in Run Dialog Box" src="http://www.askprateek.tk/wp-content/uploads/2012/07/Sevices.msc-in-Run-Dialog-Box.png" width="413" height="212" />

2. Double click on **Windows Installer** in the right side to open its properties.

<img class="alignnone size-full wp-image-2245" alt="Double Click Windows Installer" src="http://www.askprateek.tk/wp-content/uploads/2012/07/Double-Click-Windows-Installer-e1357806028155.png" width="600" height="349" />

**3. **Now change startup type to** Automatic (Delayed start)** and click apply.

> **NOTE: Delayed Start is very important otherwise none of the service will run at windows statup other than Microsoft own**

<img class="alignnone size-full wp-image-2246" alt="Follow the Screenshot" src="http://www.askprateek.tk/wp-content/uploads/2012/07/Follow-the-Screenshot.png" width="421" height="570" />

**4. **Now click start to start the service.

You can also disable unnecessary startup services which will help you in increasing system Performance. Take a look at this article to know about the services which can be safely Turn off to increase Performance:

<span style="text-decoration: underline;"><strong>How to Disable unnecessary Startup services from being loaded at Windows Startup</strong></span></p> 

</a>

It&#8217;s over for now we will bring something more again