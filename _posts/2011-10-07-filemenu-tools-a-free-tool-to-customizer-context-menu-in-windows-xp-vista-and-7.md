---
title: 'FileMenu Tools: A free tool to customize Explorer and Desktop Context menu in Windows XP, Vista and 7'
author: Ask Prateek
layout: post
permalink: /filemenu-tools-a-free-tool-to-customizer-context-menu-in-windows-xp-vista-and-7/
jabber_published:
  - 1317998486
  - 1317998486
  - 1317998486
  - 1317998486
  - 1317998486
  - 1317998486
  - 1317998486
  - 1317998486
  - 1317998486
  - 1317998486
  - 1317998486
  - 1317998486
  - 1317998486
  - 1317998486
  - 1317998486
  - 1317998486
yourls_shorturl:
  - http://ask-pra.tk/24
  - http://ask-pra.tk/24
  - http://ask-pra.tk/24
  - http://ask-pra.tk/24
categories:
  - Softwares
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - context
  - customize
  - desktop
  - explorer
  - file
  - menu
  - tools
---
<!--more-->

Our Explorer context menu, it is that thing without which we can&#8217; live without. So, We are sharing a cool Application to customize Explorer context menu. It let&#8217;s you to

  * Add some built-in utilities that perform operations on files and folders.
  * Add customised commands which run external applications, copy/move to a specific folder or delete specific file types.
  * Configure the &#8220;Send to&#8230;&#8221; submenu.
  * Enable/disable commands which are added by other applications to the context menu.

It also add Customize the **Desktop Context menu. **Here is a Screenshot of this application result.

![][1]

> <span style="color: #000000;"><strong>How to use ? </strong></span>

It is not a difficult task, just select that option which you want to add in Context menu and click Apply Changes and you will see the results instantly.

<img class="size-full wp-image-1998 alignnone" title="File Menu Tools" src="http://www.askprateek.tk/wp-content/uploads/2011/10/File-Menu-Tools.png" alt="" width="600" height="392" />

You can download this utility fron the following link:

> <span style="text-decoration: underline;"><strong><a title="Download FileMenu Tools" href="http://www.lopesoft.com/en/fmtools/download.html" target="_blank">Download link</a></strong></span>
> 
> <span style="text-decoration: underline;"><strong><a title="Visit Website" href="http://www.lopesoft.com/en/index.html" target="_blank">HomePage</a> </strong></span>

This Freeware supports **Windows XP, Vista, Seven** and also support **Windows 98, Me and 2000**

 [1]: http://www.vipindir.com/wp-content/uploads/2012/07/filemenu-tools.png