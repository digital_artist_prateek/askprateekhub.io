---
title: How to Install Android Ice-Cream Sandwich OR other version in PC
author: Ask Prateek
layout: post
permalink: /install-android-ice-cream-sandwich-version-pc/
socialize:
  - 
  - 
yourls_shorturl:
  - http://ask-pra.tk/2q
  - http://ask-pra.tk/2q
yourls_tweeted:
  - 1
  - 1
socialize_text:
  - |
    <div id="presetListDiv">This article was posted by <a href="http://www.askpk123.tk">Prateek Kumar</a> in following section:  [post_list name="categories"]</div>
    <div>
    
    If you enjoyed this article, then <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Like us on Facebook</a> and <a title="Follow us on Twitter" href="http://twitter.com/AskPK123" target="_blank">Follow us on Twitter</a> so that you won't miss any of the great Article. Also you can checkout our <a title="Our DeviantART Gallery" href="http://prateek-kumar.deviantart.com/" target="_blank">DeviantART Gallery</a> and browse for some Art Work by Me.
    
    Thanks for Reading and It is requested to leave a comment if you want to Discuss something or simply say Thanks
    
    </div>
  - |
    <div id="presetListDiv">This article was posted by <a href="http://www.askpk123.tk">Prateek Kumar</a> in following section:  [post_list name="categories"]</div>
    <div>
    
    If you enjoyed this article, then <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Like us on Facebook</a> and <a title="Follow us on Twitter" href="http://twitter.com/AskPK123" target="_blank">Follow us on Twitter</a> so that you won't miss any of the great Article. Also you can checkout our <a title="Our DeviantART Gallery" href="http://prateek-kumar.deviantart.com/" target="_blank">DeviantART Gallery</a> and browse for some Art Work by Me.
    
    Thanks for Reading and It is requested to leave a comment if you want to Discuss something or simply say Thanks
    
    </div>
categories:
  - Troubleshooting
  - Windows 8
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - android
  - cream
  - ice
  - install
  - PC
  - sandwich
  - windows
---
<!--more-->

Hello Everyone, Most of you might be using Android Phones which are famous for there stability, Play Store, etc. And mostly because of the extent of customization. You can tweak Android to a level to which no OS can be tweaked. So, today I am going to teach you **How to Install Android Ice-Cream Sandwich on PC**. Just follow these simple steps:

First Download VmWare Workstation (paid) or Virtual Box (Free) form these links:

> <span style="text-decoration: underline;"><strong><a href="https://www.virtualbox.org/wiki/Downloads" target="_blank">Download VmWare Workstation</a> | <a href="http://www.vmware.com/support/" target="_blank">Download Virtaul Box</a></strong></span>

**Option 1.)** First Download Android x86 Build for PC&#8211;>

> <span style="text-decoration: underline;"><strong><a title="ICS and other builds" href="http://www.android-x86.org/download/" target="_blank">Download link</a></strong></span>

**Option 2.)** Download Preconfigured ICS VM file from the following link:

> <span style="text-decoration: underline;"><strong><a href="http://www.vmlite.com/index.php?option=com_content&view=article&id=68%3Aandroid&catid=17%3Avmlitenewsrotator" target="_blank">Download ICS Pre Configured</a></strong></span>

<div>
  <p>
    Open the Android-v4 folder and double click to launch the blue colored Android v-4 VirtualBox Machine Definition.
  </p>
  
  <p>
    <a href="http://s.online-tech-tips.com/wp-content/uploads/2012/05/Virtual-Box-v4-definition.jpg?41ed4f"><img src="http://s.online-tech-tips.com/wp-content/uploads/2012/05/Virtual-Box-v4-definition_thumb.jpg?41ed4f" alt="Virtual Box v4 definition" width="524" height="171" border="0" data-lazy-type="image" data-lazy-src="http://s.online-tech-tips.com/wp-content/uploads/2012/05/Virtual-Box-v4-definition_thumb.jpg?41ed4f" /></a>
  </p>
  
  <p>
    <strong>Step 7:</strong> When the boot menu is presented in VirtualBox, press ‘<em>start</em>‘ on the top toolbar and then if required select the ‘<em>Android Startup from /dev/sda</em>‘ option.
  </p>
  
  <p>
    <strong>Step 8:</strong> All steps are complete. Android 4.0 ICS should now be booting up allowing you to enjoy that Android goodness.
  </p>
</div>

<div>
</div>

<div>
</div>