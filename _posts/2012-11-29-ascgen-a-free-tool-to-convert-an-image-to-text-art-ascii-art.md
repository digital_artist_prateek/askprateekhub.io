---
title: 'AscGen: A free tool to convert an Image to Text Art (ASCII Art)'
author: Ask Prateek
layout: post
permalink: /ascgen-a-free-tool-to-convert-an-image-to-text-art-ascii-art/
categories:
  - Softwares
tags:
  - ascgen
  - convert
  - image to text
  - windows
---
<!--more-->

Hello Everyone,

As you know that we regularly post cool freewares like <span style="text-decoration: underline;"><a title="NPointer: Control your Mouse Pointer with your Hand or Head" href="http://www.askprateek.tk/npointer-control-mouse-pointer-hand-head/" target="_blank">NPointer( Control Cursor with Hand or Head)</a></span> and <span style="text-decoration: underline;"><a title="Luxand Blink! : A small tool which allows to login via Face Recognition" href="http://www.askprateek.tk/luxand-blink-a-small-tool-which-allows-to-login-via-face-recognition/" target="_blank">Face Recognition Software like Luxand Blink.</a></span>

Today I am sharing a freeware which will convert any image into text. It is called AscGen. It is very easy to use but awesome tool. You can use any image to make a ASCII art then using Levels, Brightness, Dither etc you can finalize the image which will result in a text photograph like this:

<img class="alignnone size-full wp-image-2039" title="PiCS" src="http://www.askprateek.tk/wp-content/uploads/2012/11/PiCS.png" alt="" width="500" height="547" />

This tool also allows you to edit the Ramp. Ramp is the collection of characters used for generating text.

After manipulating the image you can either store it in text format or as an image. But it is recommended to try different combinations before exporting the final product.

You can Download it from the following link:

> **Download AscGen 2:** Choose either: <span style="text-decoration: underline;"><strong><a href="http://sourceforge.net/projects/ascgen2/files/Executable/Ascgen2-2.0.0/Ascgen2-2.0.0.zip/download">Zip File</a> </strong></span>(143.2Kb) or <span style="text-decoration: underline;"><strong><a href="http://sourceforge.net/projects/ascgen2/files/Executable/Ascgen2-2.0.0/Ascgen2-2.0.0.7z/download">7z File</a></strong> </span>(123Kb).
> 
> <span style="text-decoration: underline;"><strong><a title="Homepage" href="http://ascgendotnet.jmsoftware.co.uk/" target="_blank">Homepage</a></strong></span>

<div>
</div>

&nbsp;