---
title: 'How to customize &#8220;Send To&#8221; menu in Windows XP, Vista, 7 and 8'
author: Ask Prateek
layout: post
permalink: /how-to-customize-send-to-menu-in-windows-explorer-context-menu/
jabber_published:
  - 1319131500
  - 1319131500
  - 1319131500
  - 1319131500
  - 1319131500
  - 1319131500
  - 1319131500
  - 1319131500
yourls_shorturl:
  - http://ask-pra.tk/1o
  - http://ask-pra.tk/1o
sfw_comment_form_password:
  - StuwqDq70kZQ
  - StuwqDq70kZQ
categories:
  - Troubleshooting
  - Windows 8
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - add
  - customize
  - drive
  - hard
  - menu
  - partition
  - send
  - to
  - windows
  - Xp
---
<!--more-->

Everyone like &#8220;Send To&#8221; menu in windows explorer context menu, but we can use this menu only when we have to copy something in a external drive, CD drive or My Documents.

But today I will teach you How to customize Send To menu in Windows XP, Vista, 7 and 8 and add some more options in that menu. Here is a screenshot of what we are going to do:

<img class="alignnone size-full wp-image-2068" title="Final" src="http://www.askprateek.tk/wp-content/uploads/2011/10/Final.png" alt="" width="561" height="455" />

**1.** First we have to change the properties of  hidden files. For this click on start&#8211;>Control Panel&#8211;>Folder options. Now select the view tab and select &#8220;**Show hidden files and folders**&#8221; under &#8220;**Hidden files and folder**&#8221; option. Click OK.

For Windows 8 users, follow the 2nd Dialog box and **check the Hidden items box** shown in the Screenshot:

<img class="alignnone size-full wp-image-2070" title="Hidden Files" src="http://www.askprateek.tk/wp-content/uploads/2011/10/Hidden-Files.jpg" alt="" width="710" height="298" />

**2.** Now browse for this Folder if you are using Windows XP

> C:\Documents and Settings\**YourUsername\**SendTo

If you are using Windows Vista, 7 and 8 the open My Computer and paste the following line the address bar:

> %APPDATA%\Microsoft\Windows\SendTo

**Note:** Change &#8220;C&#8221; to the drive in which you have installed your Windows and Replace &#8220;**YourUsername**&#8221; with your account name which you use to login to your computer.

**3.** Now copy a shortcut of the desired folder or Drive which you want to appear in &#8220;Send To&#8221; Menu. For instance if you want to show your **D:\drive** in the Menu then** Copy D:\drive** and then Right Click anywhere inside the Send To Menu and click on **Paste Shortcut.**

<img class="alignnone size-full wp-image-2071" title="Send To Folder" src="http://www.askprateek.tk/wp-content/uploads/2011/10/Send-To-Folder.png" alt="" width="700" height="432" />

**Bonus Tip:** If you will add a shortcut of Notepad in &#8220;Send To&#8221; menu then it will open the selected file in Notepad. Also you can change the Icons of Shortcut icons via properties so that the Send To Menu appear elegant.

Don&#8217;t forget to check our great articles on Windows 8:

  * [[Windows 8 Review] All about Microsoft new Cperating System Windows 8][1]
  * [[Windows 8 Features] All about Windows 8 New Additions][2]

&nbsp;

 [1]: http://www.askprateek.tk/windows-8-review-all-about-mircosoft-new-operating-system-windows-8/ "[Windows 8 Review] All about Mircosoft new Operating System Windows 8."
 [2]: http://www.askprateek.tk/windows-8-features-all-about-new-additions-in-windows-8/ "[Windows 8 Features] All about new additions in Windows 8"