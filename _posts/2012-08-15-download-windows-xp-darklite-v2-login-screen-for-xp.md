---
title: Download Windows XP DarkLite V2 Login Screen for XP
author: Ask Prateek
layout: post
permalink: /download-windows-xp-darklite-v2-login-screen-for-xp/
yourls_shorturl:
  - http://ask-pra.tk/2p
  - http://ask-pra.tk/2p
jabber_published:
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
email_notification:
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
  - 1345020393
sfw_comment_form_password:
  - qeF9XCoczUV1
  - qeF9XCoczUV1
  - qeF9XCoczUV1
  - qeF9XCoczUV1
categories:
  - Windows XP
tags:
  - dark
  - download
  - lite
  - login
  - screen
  - v2
  - windows
  - Xp
---
<!--more-->

As you know that we love to share customization stuff like themes, logon screen, boot screen etc. We have posted about Windows XP Dark Edition resources. Now we are sharing **Windows XP DarkLite V2 Login Screen** for you. Here is a screenshot of this login screen:

<img class="  alignnone" title="Windows XP DarkLite V2 Logon Screen" alt="" src="http://fc03.deviantart.net/fs71/i/2012/229/8/d/windows_xp_darklite_v2_login_screen_for_xp_by_prateek_kumar-d5be13s.png" width="738" height="415" />

You can download it from the following link:

> **<a href="http://www.deviantart.com/download/321461272/windows_xp_darklite_v2_login_screen_for_xp_by_prateek_kumar-d5be13s.rar" target="_blank">Download Windows XP DarkLite V2 Login</a><a href="http://www.deviantart.com/download/321461272/windows_xp_darklite_v2_login_screen_for_xp_by_prateek_kumar-d5be13s.rar" target="_blank"> Screen</a>**

Instruction for using it are included in the download <img src="http://www.askprateek.tk/wp-includes/images/smilies/icon_smile.gif" alt=":)" class="wp-smiley" />

**Also Check :**

  * [Download Windows XP Dark Edition V7 Theme][1]
  * [Download Windows XP Dark Edition V7 Login Screen][2]
  * [Download Windows XP DarkLite V2 Theme][3]
  * [Download Windows XP Dark Edition V6 PowerPack Login Screen][4]

&nbsp;

 [1]: http://www.askprateek.tk/download-windows-xp-dark-edition-theme/ "Download Windows XP Dark Edition Rebirth Refix V7 Theme"
 [2]: http://www.askprateek.tk/download-windows-xp-dark-edition-logon-screen-for-xp/ "Download Windows XP Dark Edition logon screen for XP"
 [3]: http://www.askprateek.tk/2011/11/04/download-windows-xp-darklite-v2-theme-for-xp-professional/ "Download Windows XP DarkLite V2 Theme for XP Professional"
 [4]: http://www.askprateek.tk/download-windows-xp-dark-edition-v6-powerpack-login-screen-for-xp/ "Download Windows XP Dark Edition V6 PowerPack Login Screen for XP"