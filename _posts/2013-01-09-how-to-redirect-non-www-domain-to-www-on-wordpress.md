---
title: How to Redirect Non WWW domain to WWW on WordPress
author: Ask Prateek
layout: post
permalink: /how-to-redirect-non-www-domain-to-www-on-wordpress/
categories:
  - WordPress
---
<!--more-->

First of all the question arise that **Why should we Redirect Non WWW domain to WWW ?** The answer is simple, Google thinks that http://www.askprateek.tk and http://askprateek.tk are two different sites although they are one. To avoid duplicate content in search engines you can force users to use either the www or the non-www version of your website domain. This avoids search engines such as Google indexing two versions of your domain, something which is quite common because people link to both www and on-www versions of a domain (known as the www/non-www canonical issue).

So today I am doing to tell you **how to Redirect Non WWW domain to WWW domain on WordPress. **All you have to do is login to your WordPress Website.

Then visit settings>>General Settings and add www to you domain name next to WordPress Address and Site address:

<img class="alignnone size-full wp-image-2231" alt="Wordpress www Redirect" src="http://www.askprateek.tk/wp-content/uploads/2013/01/Wordpress-www-Redirect-e1357733153949.png" width="600" height="235" />

Now you will have to login again to your WordPress website and you are done. Now your domain will be redirected to www version of your domain name

If you are not using and Scripts like WordPress and want to force www on your Domain name then you have to edit to **.htaccess** file present in the public HTML folder of your website.

If **.htaccess** don&#8217;t exist then create a new file and paste the following codes according to your requirement in  it:

<span style="text-decoration: underline; color: #008000;"><strong>Force users to use http://www.yoursite.com</strong></span>

To force users to use the www version of your domain all you have to do is add the following code to your .htaccess file (just replace yoursite.com with your domain name).

> \# Redirect non-www urls to www
> 
> RewriteEngine on
> 
> RewriteCond %{HTTP_HOST} !^www\.**yoursite**\.com
> 
> RewriteRule (.*) http://**www.yoursite.com**/$1 [R=301,L]

<span style="text-decoration: underline; color: #008000;"><strong>Force users to use http://yoursite.com</strong></span>

To force users to use the non www version of your domain all you have to do is add the following code to your .htaccess file (just replace yoursite.com with your domain name).

> \# Redirect www urls to non-www
> 
> RewriteEngine on
> 
> RewriteCond %{HTTP_HOST} ^www\.**yoursite**\.com [NC]
> 
> RewriteRule (.*) http://**yoursite.com**/$1 [R=301,L]

**P.S: **We will recommend you to use www in your domain name because it looks great. But it&#8217;s up to you to decide. Happy Blogging <img src="http://www.askprateek.tk/wp-includes/images/smilies/icon_smile.gif" alt=":)" class="wp-smiley" />