---
title: How to install Counter Strike 1.6 on VPS or Dedicated Server
author: Ask Prateek
layout: post
permalink: /how-to-install-counter-strike-1-6-on-vps-or-dedicated-server/
categories:
  - VPS
---
<!--more-->

Hello Users!

We recently posted an article showing <span style="text-decoration: underline;"><strong><a title="How to install ZPanel on Centos 6  on VPS or Dedicated Server" href="http://www.askprateek.tk/how-to-install-zpanel-on-centos-6/">How to install ZPanel on VPS or Dedicated Server</a></strong></span>. Now we are here with another great article which will help Gamers in installing Counter Strike 1.6 Server on any VPS or Dedicated Server. So, Let&#8217;s start our Tutorial

**1. **First login to your VPS or Dedicated Server via any SSH Client as Root user.

**2.** Now give the following  one by one in the SSH Client (Copy the code then Right Click in SSH Client to paste the code)

> mkdir / home/84
> 
> cd / home/84
> 
> wget http://www.eylulsunucu.com/eylulforum/cs.tar.gz
> 
> tar zxvf cs.tar.gz
> 
> cd / home/84/cs
> 
> . / hlds_run-game cstrike + ip **212.175.84.84** + sv_ \*** 1-nomaster + maxplayers 32 + map de_dust

Change the IP written in bold with the IP of your Server. (IMPORTANT)

<span style="text-decoration: underline;"><strong>What does these code do ?</strong></span>

Code 1: Create a Map

Code 2: Enter That Folder

Code 3: Install CS 1.6 Server

Code 4: Extract File

Code 5: Open Folder

Code 6: Activate

Also you can check this screenshot to see how many players will be good for your server according to your server hardware configuration:

![][1]

<pre></pre>

 [1]: http://www.cstrike-planet.com/images/tutorial/i1-2.jpg