---
title: 'Firefox 6 Released: Download link inside'
author: Ask Prateek
layout: post
permalink: /firefox-6-released-download-link-inside/
sfw_comment_form_password:
  - lDzJ5arsSlly
  - lDzJ5arsSlly
  - lDzJ5arsSlly
  - lDzJ5arsSlly
  - lDzJ5arsSlly
  - lDzJ5arsSlly
  - lDzJ5arsSlly
  - lDzJ5arsSlly
  - lDzJ5arsSlly
  - lDzJ5arsSlly
  - lDzJ5arsSlly
  - lDzJ5arsSlly
  - lDzJ5arsSlly
  - lDzJ5arsSlly
  - lDzJ5arsSlly
  - lDzJ5arsSlly
yourls_shorturl:
  - http://ask-pra.tk/q
  - http://ask-pra.tk/q
  - http://ask-pra.tk/q
  - http://ask-pra.tk/q
categories:
  - Mozilla Firefox
tags:
  - 6
  - download
  - firefox
  - ink
  - public
  - released
  - to
---
<!--more-->

Mozilla has made its second rapid release; Firefox 6 is now available for download. As expected from this new release process, there aren’t any major groundbreaking changes; however the browser does bring a number of interesting new features.

There are many minor improvements throughout the Firefox experience in Firefox 6, and even a few features that are entirely new.

Firefox 6 should now load much faster if you are using Panorama. Firefox will no longer load tabs that are part of inactive Panorama groups. This means that your browser starts faster and becomes responsive faster as fewer websites are loaded to begin with. If you switch to a panorama group after loading Firefox, it will then begin loading tabs from that group.

Users who haven’t used Firefox Sync will notice that Firefox will now promote the Firefox Sync feature while saving passwords or saving bookmarks.

![][1]

Another subtle new feature is that Firefox will now highlight the domain name in the addressbar, making it easier to ascertain which website you are visiting.

![][2]

There are a number of updates and new features for developers. A “Get more tools” menu entry in the Firefox menu’s “Web Developer” menu links directly to the development selection of the Firefox add-on site. Web development tools should show up here.

The web console introduced with Firefox 4 has been significantly improved in this release. For one it can be moved around now, or docked to the top / bottom of the window. Autocomplete has also been enhanced now, and it shows autocomplete suggestions in a list. Messages logged in the console now link to the source of the message, making debugging easier.

<img src="http://www.thinkdigit.com/FCKeditor/uploads/file/Firefox_Support_Home_Page__Firefox_Help_-_Mozilla_Firefox-2011-08-16_13_12_14.png" alt="" width="590" height="127" />

For developers there is a completely new feature called Scratchpad. The new scratchpad feature provides a handy new way to test JavaScript code. It is a simple text area in which one can enter JavaScript code, and run it against the current page, or even the Firefox core code. Scripts can be saved or opened, so you can create your own collection of user scripts that you apply as need be.

![][3]

Support for new web standards have also been added for developers. The HTML5 progress element is now supports, and so are new DOM features such as server-sent events, matchMedia, and the new web sockets API.

One of the big new features to come with Firefox 6 has to be the new Permissions Manager accessible from “about: permissions”. For those concerned about their privacy, or wanting to control their data on each website individually, can now do so with ease. The new Permissions manager offers a simple interface to see the list of websites that have visited, and individually set permissions for password storage, location sharing, cookies, popup windows and offline storage. The interface also offers options to completely forget about a website, thus removing all you browsing data on that site. While this new UI doesn’t make it possible to do anything that wasn’t possible before, it consolidates the different UI options accessible from different locations in Firefox into one convenient place.

<img src="http://www.thinkdigit.com/FCKeditor/uploads/file/Permissions_Manager_-_Mozilla_Firefox-2011-08-16_13_07_17.png" alt="" width="736" height="479" />

If you are already running Firefox 5, you should see Firefox 6 available as an update option soon. Otherwise you can download the browser from <a href="http://www.mozilla.com/en-US/firefox/new/" target="_blank">Mozilla&#8217;s website</a>.

You can also check out a preview of Firefox 7 or 8 by downloading the [Aurora][4] or [Nightly Firefox][5]build. Remember that these can be very unstable at times and it is recommended you use then with a [separate profile][6].

Also check:

  * <a title="[Mozilla Firefox Latest version Update] Firefox 13.0.1 released. Download link Inside." href="http://askprateek.tk/mozilla-firefox-latest-version-update-firefox-7-0-released-download-link-inside/" rel="bookmark">[Mozilla Firefox Latest version Update]</a>
  * <a title="[Mozilla Firefox Beta Version Update] Firefox 14.0.10 Beta released. Download link inside." href="http://askprateek.tk/mozilla-firefox-beta-version-update/" rel="bookmark">[Mozilla Firefox Beta Version Update]</a>
  * <a title="[Mozilla Firefox Future Build Update] Mozilla Firefox 10.0 Alpha 1 Build Released, Download NOW" href="http://askprateek.tk/mozilla-firefox-future-build-update-mozilla-firefox-10-0-alpha-1-build-released-download-now/" rel="bookmark">[Mozilla Firefox Future Build Update]</a>

&nbsp;

 [1]: http://www.thinkdigit.com/FCKeditor/uploads/file/Firefox_Support_Home_Page__Firefox_Help_-_Mozilla_Firefox-2011-08-16_12_42_54.png
 [2]: http://www.thinkdigit.com/FCKeditor/uploads/file/Firefox_Support_Home_Page__Firefox_Help_-_Mozilla_Firefox-2011-08-16_13_00_11.png
 [3]: http://www.thinkdigit.com/FCKeditor/uploads/file/Scratchpad-2011-08-16_13_01_48.png
 [4]: http://www.mozilla.com/en-US/firefox/channel/
 [5]: http://nightly.mozilla.org/
 [6]: http://www.thinkdigit.com/Features/Mozilla-Firefox-Tips-and-Tricks_5749/5.html