---
title: How to install Windows 8 via VMware Workstation ?
author: Ask Prateek
layout: post
permalink: /how-to-install-windows-8-via-vmware-workstation/
jabber_published:
  - 1341999347
  - 1341999347
  - 1341999347
  - 1341999347
  - 1341999347
  - 1341999347
  - 1341999347
  - 1341999347
tagazine-media:
  - 'a:7:{s:7:"primary";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:6:"images";a:5:{s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";s:5:"width";s:3:"423";s:6:"height";s:3:"272";s:4:"type";s:5:"image";s:4:"area";s:6:"115056";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";s:5:"width";s:3:"669";s:6:"height";s:3:"332";s:4:"type";s:5:"image";s:4:"area";s:6:"222108";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"5";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-11 09:35:47";}'
  - 'a:7:{s:7:"primary";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:6:"images";a:5:{s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";s:5:"width";s:3:"423";s:6:"height";s:3:"272";s:4:"type";s:5:"image";s:4:"area";s:6:"115056";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";s:5:"width";s:3:"669";s:6:"height";s:3:"332";s:4:"type";s:5:"image";s:4:"area";s:6:"222108";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"5";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-11 09:35:47";}'
  - 'a:7:{s:7:"primary";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:6:"images";a:5:{s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";s:5:"width";s:3:"423";s:6:"height";s:3:"272";s:4:"type";s:5:"image";s:4:"area";s:6:"115056";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";s:5:"width";s:3:"669";s:6:"height";s:3:"332";s:4:"type";s:5:"image";s:4:"area";s:6:"222108";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"5";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-11 09:35:47";}'
  - 'a:7:{s:7:"primary";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:6:"images";a:5:{s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";s:5:"width";s:3:"423";s:6:"height";s:3:"272";s:4:"type";s:5:"image";s:4:"area";s:6:"115056";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";s:5:"width";s:3:"669";s:6:"height";s:3:"332";s:4:"type";s:5:"image";s:4:"area";s:6:"222108";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"5";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-11 09:35:47";}'
  - 'a:7:{s:7:"primary";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:6:"images";a:5:{s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";s:5:"width";s:3:"423";s:6:"height";s:3:"272";s:4:"type";s:5:"image";s:4:"area";s:6:"115056";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";s:5:"width";s:3:"669";s:6:"height";s:3:"332";s:4:"type";s:5:"image";s:4:"area";s:6:"222108";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"5";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-11 09:35:47";}'
  - 'a:7:{s:7:"primary";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:6:"images";a:5:{s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";s:5:"width";s:3:"423";s:6:"height";s:3:"272";s:4:"type";s:5:"image";s:4:"area";s:6:"115056";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";s:5:"width";s:3:"669";s:6:"height";s:3:"332";s:4:"type";s:5:"image";s:4:"area";s:6:"222108";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"5";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-11 09:35:47";}'
  - 'a:7:{s:7:"primary";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:6:"images";a:5:{s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";s:5:"width";s:3:"423";s:6:"height";s:3:"272";s:4:"type";s:5:"image";s:4:"area";s:6:"115056";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";s:5:"width";s:3:"669";s:6:"height";s:3:"332";s:4:"type";s:5:"image";s:4:"area";s:6:"222108";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"5";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-11 09:35:47";}'
  - 'a:7:{s:7:"primary";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:6:"images";a:5:{s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2012/07/vmware-error.png";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-1.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-2.png";s:5:"width";s:3:"442";s:6:"height";s:3:"401";s:4:"type";s:5:"image";s:4:"area";s:6:"177242";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-3.png";s:5:"width";s:3:"423";s:6:"height";s:3:"272";s:4:"type";s:5:"image";s:4:"area";s:6:"115056";s:9:"file_path";s:0:"";}s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";a:6:{s:8:"file_url";s:59:"http://thewindexpl.files.wordpress.com/2012/07/vmware-4.png";s:5:"width";s:3:"669";s:6:"height";s:3:"332";s:4:"type";s:5:"image";s:4:"area";s:6:"222108";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"5";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-11 09:35:47";}'
email_notification:
  - 1341999347
  - 1341999347
  - 1341999347
  - 1341999347
  - 1341999347
  - 1341999347
  - 1341999347
  - 1341999347
sfw_comment_form_password:
  - jU0TEP2QbbF4
  - jU0TEP2QbbF4
yourls_shorturl:
  - http://ask-pra.tk/e
  - http://ask-pra.tk/e
categories:
  - Softwares
  - Troubleshooting
  - Windows 8
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - candidate
  - consumer
  - developer
  - error
  - install
  - machine
  - preview
  - rc
  - release
  - virtual
  - vmware
  - windows
  - workstation
---
<!--more-->Hello Everyone!

Recently we found an Article on **Web which says &#8220;Windows 8 can&#8217;t be install using Vmware Workstation&#8221;**. They also posted about big commands which will allow you to Run Windows 8 in Full Screen. But we don&#8217;t need to type any command while installing Windows 8.

We also find a problem while installing Windows 8 in Virtual machine directly. It show this kind of error:

<img class="alignnone size-full wp-image-1306" title="VMWARE-Error" src="http://askpk123.tk/wp-content/uploads/2012/07/VMWARE-Error.png" alt="" width="710" height="399" />

So today we will teach you how to install **Windows 8 via VMware Workstation.**

**1.** First Create a new Virtual Machine. At the time for selecting installation media select **&#8220;I will install the Operating system Later&#8221; **and click Next.

<img class="alignnone size-full wp-image-1305" title="VMWARE-1" src="http://askpk123.tk/wp-content/uploads/2012/07/VMWARE-1.png" alt="" width="442" height="401" />

**2.** Then it will ask for Operating system. Select** Windows 7 **and click Next

<img class="alignnone size-full wp-image-1307" title="VMWARE-2" src="http://askpk123.tk/wp-content/uploads/2012/07/VMWARE-2.png" alt="" width="442" height="401" />

**3.** Then choose the Virtual machine name and hard disk size via next 2 dialog box.

Then you will have something this type of Dialog box shown in the picture. Then click **Customize Hardware **

<img class="alignnone size-full wp-image-1308" title="VMWARE-3" src="http://askpk123.tk/wp-content/uploads/2012/07/VMWARE-3.png" alt="" width="423" height="272" />

4. Now select **CD/DVD **Device from right side as shown in the picture. Now select **&#8220;Use Iso image file&#8221; **from the right side and **browse for Windows 8 iso file** in you system. Alternatively you can select **Physical Drive** and set it to Auto detect if you have a bootable DVD of Windows 8.

<img class="alignnone size-full wp-image-1310" title="VMWARE-4" src="http://askpk123.tk/wp-content/uploads/2012/07/VMWARE-4.png" alt="" width="669" height="332" />

Now you are done. Close that dialog box and then click finish. Now just Power on the Virtual Machine.

**Bonus Tip**: Now you will be able to make partitions in Virtual machine and you can also run HirenBootCD etc type Bootable CD/DVD/.iso via this method.

Hope this article hepls you and comments are Welcome