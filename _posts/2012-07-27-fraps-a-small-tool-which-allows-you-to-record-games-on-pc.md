---
title: 'Fraps: A small tool which allows you to record games on PC'
author: Ask Prateek
layout: post
permalink: /fraps-a-small-tool-which-allows-you-to-record-games-on-pc/
yourls_shorturl:
  - http://ask-pra.tk/2x
  - http://ask-pra.tk/2x
sfw_comment_form_password:
  - SXjf0M0facEC
  - SXjf0M0facEC
  - SXjf0M0facEC
  - SXjf0M0facEC
jabber_published:
  - 1343387043
  - 1343387043
  - 1343387043
  - 1343387043
  - 1343387043
  - 1343387043
  - 1343387043
  - 1343387043
  - 1343387043
  - 1343387043
  - 1343387043
  - 1343387043
  - 1343387043
  - 1343387043
  - 1343387043
  - 1343387043
email_notification:
  - 1343387045
  - 1343387045
  - 1343387045
  - 1343387045
  - 1343387045
  - 1343387045
  - 1343387045
  - 1343387045
  - 1343387045
  - 1343387045
  - 1343387045
  - 1343387045
  - 1343387045
  - 1343387045
  - 1343387045
  - 1343387045
categories:
  - Softwares
  - Windows 8
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - capture
  - fifa
  - fps
  - games
  - high
  - record
  - screen
  - screenshots
---
<!--more-->

Hello Everyone!

Fraps is a universal Windows application that can be used with games using DirectX or OpenGL graphic technology.  In its current form Fraps performs many tasks and can best be described as:

**Benchmarking Software** &#8211; Show how many Frames Per Second (FPS) you are getting in a corner of your screen.  Perform custom benchmarks and measure the frame rate between any two points.  Save the statistics out to disk and use them for your own reviews and applications.

**Screen Capture Software** &#8211; Take a screenshot with the press of a key!  There&#8217;s no need to paste into a paint program every time you want a new shot.  Your screen captures are automatically named and timestamped.

**Realtime Video Capture Software** &#8211; Have you ever wanted to record video while playing your favourite game?  Come join the Machinima revolution!  Throw away the VCR, forget about using a DV cam, game recording has never been this easy!  Fraps can capture audio and video up to 7680&#215;4800 with custom frame rates from 1 to 120 frames per second!

<img class="alignnone size-full wp-image-1263" title="Fraps" src="http://askpk123.tk/wp-content/uploads/2012/07/Fraps.jpg" alt="" width="616" height="378" />

You can download it from the following link:

> **<span style="text-decoration: underline;"><a title="Direct Link" href="http://www.fraps.com/setup.exe">Download Fraps</a></span> | <span style="text-decoration: underline;"><a title="Mirror" href="http://www.fraps.com/download.php" target="_blank">Mirror</a></span>**