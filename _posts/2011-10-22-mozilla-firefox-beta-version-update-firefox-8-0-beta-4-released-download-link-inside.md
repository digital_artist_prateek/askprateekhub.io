---
title: '[Mozilla Firefox Beta Version Update] Firefox 8.0 Beta 4 released. Download link inside.'
author: Ask Prateek
layout: post
permalink: /mozilla-firefox-beta-version-update-firefox-8-0-beta-4-released-download-link-inside/
jabber_published:
  - 1319280211
  - 1319280211
  - 1319280211
  - 1319280211
  - 1319280211
  - 1319280211
  - 1319280211
  - 1319280211
sfw_comment_form_password:
  - zcToAaUWKPbp
  - zcToAaUWKPbp
yourls_shorturl:
  - http://ask-pra.tk/o
  - http://ask-pra.tk/o
categories:
  - Mozilla Firefox
  - Softwares
tags:
  - 4
  - 7
  - 8
  - beta
  - features
  - firefox
  - mozilla
  - new
  - update
---
<!--more-->

The latest Mozilla Firefox Beta is now available for testing on Windows, Mac, Linux and Android. This beta includes performance enhancements that improve the browsing experience for users and enable developers to create faster Web apps and websites.

Mozilla has updated Firefox to **Beta 4**. Following are the new features in this version.

  * Add-ons installed by third party programs are now disabled by default
  * Added a one-time add-on selection dialog to manage previously installed add-ons
  * Added Twitter to the search bar
  * Added a preference to load tabs on demand, improving start-up time when windows are restored
  * Improved tab animations when moving, reordering, or detaching tabs
  * Improved performance and memory handling when using <audio> and <video> elements
  * Added CORS support for cross-domain textures in WebGL
  * Added support for HTML5 context menus
  * Added support for insertAdjacentHTML
  * Improved CSS hyphen support for many languages
  * Improved WebSocket support
  * Fixed several stability issues

<div>
  <p>
    <img class="alignnone size-full wp-image-398" title="Mozilla Firefox Beta Version Update" src="http://thewindexpl.files.wordpress.com/2011/09/mozilla-beta-version-update.jpg" alt="" width="600" height="394" />
  </p>
  
  <p>
    You can download <strong>Mozilla Firefox 8.0 Beta 4  Version </strong> from the following link:
  </p>
  
  <blockquote>
    <p>
      <span style="text-decoration:underline;"><strong><a title="Download Mozilla Firefox latest bete Version" href="http://www.mozilla.org/en-US/firefox/all-beta.html" target="_blank">Download Link</a> </strong></span>
    </p>
    
    <p>
      <span style="text-decoration:underline;"><strong><a href="http://www.mozilla.com/en-US/firefox/7.0beta/releasenotes/" target="_blank">Release Notes</a> </strong></span>
    </p>
  </blockquote>
  
  <p>
    If you don&#8217;t want to try this Beta version but want to try the Latest Stable version then here is something for you:
  </p>
  
  <p>
    <span style="text-decoration:underline;"><strong><a title="[Mozilla Firefox Latest version Update] Firefox 7.0.1 released. Download link Inside." href="http://www.askprateek.tk/2011/09/29/mozilla-firefox-latest-version-update-firefox-7-0-released-download-link-inside/">Mozilla Firefox Latest version Update</a></strong></span>
  </p>
</div>

If you are a power user and want to try future build the follow this link:

<span style="text-decoration:underline;"><strong><a title="[Mozilla Firefox Future Build Update] Mozilla Firefox 10.0 Alpha 1 Build Released, Download NOW" href="http://www.askprateek.tk/2011/09/29/mozilla-firefox-future-build-update-mozilla-firefox-10-0-alpha-1-build-released-download-now/">Mozilla Firefox Nightly Build Update</a></strong></span>