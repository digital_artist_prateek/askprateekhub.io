---
title: How to Get Windows 8 Metro based Task Manager in Windows 7 using inbuild task Manager
author: Ask Prateek
layout: post
permalink: /how-to-get-windows-8-metro-based-task-manager-in-windows-7-using-inbuild-task-manager/
jabber_published:
  - 1344622873
  - 1344622873
  - 1344622873
  - 1344622873
  - 1344622873
  - 1344622873
email_notification:
  - 1344622874
  - 1344622874
  - 1344622874
  - 1344622874
  - 1344622874
  - 1344622874
sfw_comment_form_password:
  - XeLx5aXdYIvd
  - XeLx5aXdYIvd
yourls_shorturl:
  - http://ask-pra.tk/k
  - http://ask-pra.tk/k
categories:
  - Troubleshooting
  - Windows Se7en
tags:
  - 7
  - 8
  - based
  - eight
  - manager
  - metro
  - seven
  - task
  - UI
  - windows
---
<!--more-->

> *<span style="color: #000000;"><strong>Note:</strong></span> <span style="color: #000000;">Please note that The Windows Explorer is the first website to post about this unique trick.</span>*

Hello Everyone!

As you know that Windows 8 contain a **new Task manager**  along with new start screen, theme, UI, Boot, Logon Screen etc as we posted in our **<a title="[Windows 8 Review] All about Mircosoft new Operating System Windows 8." href="http://www.askprateek.tk/2011/11/18/windows-8-review-all-about-mircosoft-new-operating-system-windows-8/" target="_blank">review on Windows 8</a>.**

Now we are posting a small trick which will change the default Task Manager with the one in Windows 8. You can see the screenshot of Task Manager UI that you will get after this small trick:

<img class="alignnone size-full wp-image-2181" alt="Windows 7 Metro Task Manager" src="http://www.askprateek.tk/wp-content/uploads/2012/08/windows-7metro-task-manager.png" width="412" height="459" />

<span style="color: #000000;"><strong>What you have to Do?</strong></span>

Just Double click on the Grey area Shown in the screenshot and you are done:

<img class="alignnone size-full wp-image-2180" alt="Win 7 Task Manager" src="http://www.askprateek.tk/wp-content/uploads/2012/08/Win-7-Task-Manager.png" width="412" height="459" />

You can access Task Manager by Pressing **[CTRL]+ [Shift] + [Esc]** **key** or right click on taskbar and click Start task manager or press  **[CTRL]+ [ALT] + [DELETE] key.**

<span style="text-decoration: underline;"><em>Hope you like this article, If yes or no, feel free to share your view about this trick and Windows 8.</em></span>