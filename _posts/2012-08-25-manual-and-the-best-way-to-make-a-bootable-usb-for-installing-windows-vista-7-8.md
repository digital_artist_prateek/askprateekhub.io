---
title: 'Manual and the Best way to make a Bootable USB for installing Windows Vista, 7 &#038; 8.'
author: Ask Prateek
layout: post
permalink: /manual-and-the-best-way-to-make-a-bootable-usb-for-installing-windows-vista-7-8/
jabber_published:
  - 1331994716
  - 1331994716
  - 1331994716
  - 1331994716
publicize_results:
  - 'a:2:{s:7:"twitter";a:1:{i:181125948;a:2:{s:7:"user_id";s:8:"AskPK123";s:7:"post_id";s:18:"181025083341541376";}}s:2:"fb";a:1:{i:100001242262159;a:2:{s:7:"user_id";s:15:"100001242262159";s:7:"post_id";s:15:"346126052105467";}}}'
  - 'a:2:{s:7:"twitter";a:1:{i:181125948;a:2:{s:7:"user_id";s:8:"AskPK123";s:7:"post_id";s:18:"181025083341541376";}}s:2:"fb";a:1:{i:100001242262159;a:2:{s:7:"user_id";s:15:"100001242262159";s:7:"post_id";s:15:"346126052105467";}}}'
  - 'a:2:{s:7:"twitter";a:1:{i:181125948;a:2:{s:7:"user_id";s:8:"AskPK123";s:7:"post_id";s:18:"181025083341541376";}}s:2:"fb";a:1:{i:100001242262159;a:2:{s:7:"user_id";s:15:"100001242262159";s:7:"post_id";s:15:"346126052105467";}}}'
  - 'a:2:{s:7:"twitter";a:1:{i:181125948;a:2:{s:7:"user_id";s:8:"AskPK123";s:7:"post_id";s:18:"181025083341541376";}}s:2:"fb";a:1:{i:100001242262159;a:2:{s:7:"user_id";s:15:"100001242262159";s:7:"post_id";s:15:"346126052105467";}}}'
reddit:
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1332919946";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1332919946";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1332919946";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1332919946";}'
email_notification:
  - 1331994721
  - 1331994721
  - 1331994721
  - 1331994721
tagazine-media:
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/03/proof.png";s:6:"images";a:2:{s:73:"http://thewindexpl.files.wordpress.com/2012/03/mount-to-virtual-drive.png";a:6:{s:8:"file_url";s:73:"http://thewindexpl.files.wordpress.com/2012/03/mount-to-virtual-drive.png";s:5:"width";s:3:"628";s:6:"height";s:3:"391";s:4:"type";s:5:"image";s:4:"area";s:6:"245548";s:9:"file_path";s:0:"";}s:56:"http://thewindexpl.files.wordpress.com/2012/03/proof.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/03/proof.png";s:5:"width";s:3:"742";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"313866";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-03-17 14:31:55";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/03/proof.png";s:6:"images";a:2:{s:73:"http://thewindexpl.files.wordpress.com/2012/03/mount-to-virtual-drive.png";a:6:{s:8:"file_url";s:73:"http://thewindexpl.files.wordpress.com/2012/03/mount-to-virtual-drive.png";s:5:"width";s:3:"628";s:6:"height";s:3:"391";s:4:"type";s:5:"image";s:4:"area";s:6:"245548";s:9:"file_path";s:0:"";}s:56:"http://thewindexpl.files.wordpress.com/2012/03/proof.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/03/proof.png";s:5:"width";s:3:"742";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"313866";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-03-17 14:31:55";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/03/proof.png";s:6:"images";a:2:{s:73:"http://thewindexpl.files.wordpress.com/2012/03/mount-to-virtual-drive.png";a:6:{s:8:"file_url";s:73:"http://thewindexpl.files.wordpress.com/2012/03/mount-to-virtual-drive.png";s:5:"width";s:3:"628";s:6:"height";s:3:"391";s:4:"type";s:5:"image";s:4:"area";s:6:"245548";s:9:"file_path";s:0:"";}s:56:"http://thewindexpl.files.wordpress.com/2012/03/proof.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/03/proof.png";s:5:"width";s:3:"742";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"313866";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-03-17 14:31:55";}'
  - 'a:7:{s:7:"primary";s:56:"http://thewindexpl.files.wordpress.com/2012/03/proof.png";s:6:"images";a:2:{s:73:"http://thewindexpl.files.wordpress.com/2012/03/mount-to-virtual-drive.png";a:6:{s:8:"file_url";s:73:"http://thewindexpl.files.wordpress.com/2012/03/mount-to-virtual-drive.png";s:5:"width";s:3:"628";s:6:"height";s:3:"391";s:4:"type";s:5:"image";s:4:"area";s:6:"245548";s:9:"file_path";s:0:"";}s:56:"http://thewindexpl.files.wordpress.com/2012/03/proof.png";a:6:{s:8:"file_url";s:56:"http://thewindexpl.files.wordpress.com/2012/03/proof.png";s:5:"width";s:3:"742";s:6:"height";s:3:"423";s:4:"type";s:5:"image";s:4:"area";s:6:"313866";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"2";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-03-17 14:31:55";}'
yourls_shorturl:
  - http://ask-pra.tk/27
  - http://ask-pra.tk/27
  - http://ask-pra.tk/27
  - http://ask-pra.tk/27
categories:
  - Troubleshooting
  - Windows 8
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - 8
  - a
  - best
  - bootable
  - consumer
  - developer
  - easy
  - fast
  - how
  - make
  - manual
  - manually
  - method
  - to
  - usb
  - vista
  - windows seven
---
**<!--more-->**

> <p style="text-align: center;">
>   <span style="text-decoration: underline;"><strong>Please note that The Windows Explorer is the first Website post this unique Trick.</strong></span>
> </p>

Most of us use different softwares to make a Bootable USB. But these softwares takes a lot of time. So we decided to share the simplest method make a Bootable USB of  <a title="Windows Vista" href="http://www.askprateek.tk//windows-vista/" target="_blank">Windows Vista</a>, <a title="Windows Se7en" href="http://www.askprateek.tk//windows-se7en/" target="_blank">Seven</a> and <a title="Windows 8" href="http://www.askprateek.tk//windows-8/" target="_blank">Window 8</a>. So without wasting any time let&#8217;s start our Tutorial.

**1.** First** Run CMD** and Type DiskPart in CMd Window. Then in **DiskPart Window, Type list Disk:**

<img class="alignnone size-full wp-image-2283" alt="CDM Diskpart" src="http://www.askprateek.tk/wp-content/uploads/2012/08/CDM-Diskpart.png" width="677" height="278" />

**2.** Type the following commands in DiskaPart Windows:

> **Select Disk # **(Where # is the number of your USB disk. We typed “Select Disk 1”)
> 
> **Clean **(removes any existing partitions from the USB disk, including any hidden sectors)
> 
> **Create Partition Primary** (Creates a new primary partition with default parameters)
> 
> **Select Partition 1** (Focus on the newly created partition)
> 
> **Active **(Sets the in-focus partition to active, informing the disk firmware that this is a valid system partition)
> 
> **Format FS=NTFS** (Formats the partition with the NTFS file system. This may take several minutes to complete, depending on the size of your USB key.)
> 
> **Assign **(Gives the USB drive a Windows volume and next available drive letter, which you should write down. In our case, drive “L” was assigned.)

After these commands DiskPart Windows will look like this:

<img class="alignnone size-full wp-image-2284" alt="Disk Part Commands" src="http://www.askprateek.tk/wp-content/uploads/2012/08/Disk-Part-Commeands.png" width="677" height="570" />

Mount your Windows .iso file to a Virtual Drive. You can use any of the software listed below:

  * <a title="Download UltraISO" href="http://www.google.co.in/url?sa=t&rct=j&q=ultraiso&source=web&cd=1&sqi=2&ved=0CDcQ0gIoADAA&url=http%3A%2F%2Fwww.ezbsystems.com%2Fultraiso%2Fdownload.htm&ei=1ZlkT-COBMvhrAeMuKSfDQ&usg=AFQjCNHeMwyXeadCfqVeTJiKzPW3o-T0FA" target="_blank">UltraISO </a>
  * <a title="Download PowerISO" href="http://www.google.co.in/url?sa=t&rct=j&q=poweriso&source=web&cd=2&sqi=2&ved=0CDoQjBAwAQ&url=http%3A%2F%2Fwww.poweriso.com%2Fdownload.htm&ei=-plkT-TpJ4vqrQeU_dTRCw&usg=AFQjCNHL04atbjEyGcsPO8Yf2q3NFSiZqQ" target="_blank">PowerISO</a>
  * <a title="Download MagicISO" href="http://www.google.co.in/url?sa=t&rct=j&q=magiciso&source=web&cd=4&sqi=2&ved=0CEUQjBAwAw&url=http%3A%2F%2Fwww.magiciso.com%2Ftutorials%2Fmiso-magicdisc-history.htm&ei=xJpkT77lBcHRrQfb-cX2DQ&usg=AFQjCNHZF0w8xeoRTDX4rcFax7LjqeqCkw" target="_blank">MagicISO</a>

After installing any of the above software Mount your Windows.iso file to a Virtual Drive. To do so, Right Click your .ISO file the select the correct menu option according to your Downloaded software, then **Mount to Drive** as shown in the Screenshot:

<img class="alignnone size-full wp-image-2285" alt="Mount to Virtual Drive" src="http://www.askprateek.tk/wp-content/uploads/2012/08/Mount-to-Virtual-Drive.png" width="717" height="191" />

Now after Mounting to Virtal Drive, Copy the content of the ISO file into a Flash Drive and You are Done

Alternatively you can Directly extract your ISO file into a Removal Disk  by selecting **Extract to&#8230;** option in the above Screenshot.

**Note:** You also use WinRAR for extracting .Iso file. WinRAR is available in our &#8220;**<a title="Downloads" href="http://www.askprateek.tk//downloads/" target="_blank">Downloads</a>**&#8221; Section

&nbsp;