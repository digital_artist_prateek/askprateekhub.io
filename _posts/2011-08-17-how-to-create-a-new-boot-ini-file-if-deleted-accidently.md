---
title: How to create a new Boot.ini file if deleted accidently
author: Ask Prateek
layout: post
permalink: /how-to-create-a-new-boot-ini-file-if-deleted-accidently/
yourls_shorturl:
  - http://ask-pra.tk/2w
  - http://ask-pra.tk/2w
sfw_comment_form_password:
  - XdmByPXMlnk7
  - XdmByPXMlnk7
  - XdmByPXMlnk7
  - XdmByPXMlnk7
  - XdmByPXMlnk7
  - XdmByPXMlnk7
  - XdmByPXMlnk7
  - XdmByPXMlnk7
  - XdmByPXMlnk7
  - XdmByPXMlnk7
  - XdmByPXMlnk7
  - XdmByPXMlnk7
  - XdmByPXMlnk7
  - XdmByPXMlnk7
  - XdmByPXMlnk7
  - XdmByPXMlnk7
socialize_text:
  - |
    <div id="presetListDiv">This article was posted by <a href="http://www.askpk123.tk">Prateek Kumar</a> in following section:  [post_list name="categories"]</div>
    <div>
    
    If you enjoyed this article, then <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Like us on Facebook</a> and <a title="Follow us on Twitter" href="http://twitter.com/AskPK123" target="_blank">Follow us on Twitter</a> so that you won't miss any of the great Article. Also you can checkout our <a title="Our DeviantART Gallery" href="http://prateek-kumar.deviantart.com/" target="_blank">DeviantART Gallery</a> and browse for some Art Work by Me.
    
    Thanks for Reading and It is requested to leave a comment if you want to Discuss something or simply say Thanks
    
    </div>
  - |
    <div id="presetListDiv">This article was posted by <a href="http://www.askpk123.tk">Prateek Kumar</a> in following section:  [post_list name="categories"]</div>
    <div>
    
    If you enjoyed this article, then <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Like us on Facebook</a> and <a title="Follow us on Twitter" href="http://twitter.com/AskPK123" target="_blank">Follow us on Twitter</a> so that you won't miss any of the great Article. Also you can checkout our <a title="Our DeviantART Gallery" href="http://prateek-kumar.deviantart.com/" target="_blank">DeviantART Gallery</a> and browse for some Art Work by Me.
    
    Thanks for Reading and It is requested to leave a comment if you want to Discuss something or simply say Thanks
    
    </div>
socialize:
  - 
  - 
categories:
  - Troubleshooting
  - Windows XP
tags:
  - accidently
  - boot.ini
  - create
  - deleted
  - how
  - if
  - to
  - windows
  - Xp
---
<span style="font-family: Georgia, 'Times New Roman', 'Bitstream Charter', Times, serif;"><!--more-->Hello Everyone, </span>Today I am going to tell you How to create a new BOOT.INI if deleted accidently . All you have to do is Just follow these simple steps

<span style="font-family: Georgia, 'Times New Roman', 'Bitstream Charter', Times, serif;">1.Right Click <strong>My Computer >Properties.</strong></span>

<img class="alignnone size-full wp-image-1874" title="Step 1" src="http://askprateek.tk/wp-content/uploads/2011/08/1.png" alt="" width="477" height="491" />

> **Note**: If you like the above System Properties Dialog Box then you also get it via <span style="text-decoration: underline;"><strong><a title="[ Dark Edition Version ] Customizing System Properties dailog Box [sysdm.cpl] in Windows XP" href="http://askprateek.tk/dark-edition-version-customizing-system-properties-dailog-box-sysdm-cpl-in-windows-xp/">This Article</a></strong></span>

<span style="font-family: Georgia, 'Times New Roman', 'Bitstream Charter', Times, serif;">2. Go to <strong>ADVANCED > SETTINGS [STARTUP AND RECOVERY]</strong></span>

<img class="alignnone size-full wp-image-1873" title="Step 2" src="http://askprateek.tk/wp-content/uploads/2011/08/2.png" alt="" width="465" height="501" />

<span style="font-family: Georgia, 'Times New Roman', 'Bitstream Charter', Times, serif;">3. Click Edit And Paste the Following in the Notepad.</span>

> <div>
>   [boot loader]timeout=0
> </div>
> 
> <div>
>   default=multi(0)disk(0)rdisk(0)partition(1)WINDOWS
> </div>
> 
> <div>
>   [operating systems]
> </div>
> 
> <div>
>   multi(0)disk(0)rdisk(0)partition(1)WINDOWS=”Microsoft Windows XP Professional” /fastdetect /TUTag=NFTMS0 /Kernel=TUKernel.exe
> </div>

<img class="alignnone size-full wp-image-1875" title="3" src="http://askprateek.tk/wp-content/uploads/2011/08/3.png" alt="" width="369" height="175" />

<span style="font-family: Georgia, 'Times New Roman', 'Bitstream Charter', Times, serif;">Understanding the BOOT.ini:</span>

  * <span style="font-family: Georgia, 'Times New Roman', 'Bitstream Charter', Times, serif;"><strong>rdisk( )–></strong> This represent the no. of Hard Disk in your PC. If you have installed Windows XP in First HardDisk then Write Zero ’0‘ in the brackets</span>
  * <span style="font-family: Georgia, 'Times New Roman', 'Bitstream Charter', Times, serif;"><strong>partition( )–></strong> This represent the partition number. If you have installed Windows XP in C Drive then type Zero ’0′ in brackets, but if you have installed XP in D Drive the type one ’1′ in brackets and so on.</span>

&nbsp;