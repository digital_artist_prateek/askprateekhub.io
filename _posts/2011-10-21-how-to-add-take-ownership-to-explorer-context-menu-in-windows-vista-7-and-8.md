---
title: 'How to add &#8220;Take Ownership&#8221; to Explorer context Menu in Windows Vista, 7 and 8'
author: Ask Prateek
layout: post
permalink: /how-to-add-take-ownership-to-explorer-context-menu-in-windows-vista-7-and-8/
jabber_published:
  - 1319185851
  - 1319185851
  - 1319185851
  - 1319185851
  - 1319185851
  - 1319185851
  - 1319185851
  - 1319185851
  - 1319185851
  - 1319185851
  - 1319185851
  - 1319185851
  - 1319185851
  - 1319185851
  - 1319185851
  - 1319185851
yourls_shorturl:
  - http://ask-pra.tk/h
  - http://ask-pra.tk/h
  - http://ask-pra.tk/h
  - http://ask-pra.tk/h
sfw_comment_form_password:
  - bMfCnB4GbRqQ
  - bMfCnB4GbRqQ
  - bMfCnB4GbRqQ
  - bMfCnB4GbRqQ
categories:
  - Troubleshooting
  - Windows 8
  - Windows Se7en
  - Windows Vista
tags:
  - 7
  - 8
  - add
  - context
  - explorer
  - files
  - folder
  - menu
  - ownership
  - system
  - take
  - vista
  - windows
---
<!--more-->

<div>
  <p>
    Taking ownership of system files or folders in Windows 7 or Vista is not a simple task.You have to take ownership on system files to edit them or to replace them. Whether you use the GUI or the command line, it takes far too many steps.
  </p>
  
  <p>
    Thankfully somebody created a registry hack that will give you a menu item for “<strong>Take Ownership</strong>” that will handle all the steps for you.  (If you are the person that originally made this script, let me know and I’ll give you credit)
  </p>
  
  <p>
    Here’s what the new right-click menu will look like after installing this registry hack.
  </p>
  
  <p>
    <img class="alignnone size-full wp-image-2088" title="TakeOwnership" src="http://www.askprateek.tk/wp-content/uploads/2011/10/TakeOwnership.png" alt="" width="477" height="127" />
  </p>
  
  <p>
    You just need to download the zip file and run Takeownership.REG file. It will ask for confirmation, click Yes and you are done. To Remove take ownership option simply run the uninstallation script in the download.
  </p>
  
  <blockquote>
    <p>
      <span style="text-decoration: underline;"><strong><a href="http://www.howtogeek.com/downloads/TakeOwnership.zip">Download Registry Script</a></strong></span>
    </p>
  </blockquote>
</div>