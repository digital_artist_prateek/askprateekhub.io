---
title: 'How to make a copy of Windows XP Geniune [ Activate Windows XP ].'
author: Ask Prateek
layout: post
permalink: /how-to-make-a-copy-of-windows-xp-geniune/
yourls_shorturl:
  - http://ask-pra.tk/2c
  - http://ask-pra.tk/2c
  - http://ask-pra.tk/2c
  - http://ask-pra.tk/2c
categories:
  - Softwares
  - Troubleshooting
  - Windows XP
tags:
  - copy
  - download
  - finder
  - genuine
  - key
  - make
  - of
  - windows
  - Xp
---
<address>
  <!--more-->
</address>

> **Note:** If you are Windows 7 user and want to Activate Windows 7, then follow** [this link][1]**

<span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;">Today, I am going to tell you how to make your copy of Windows XP Genuine so that you can run many useful softwares of Microsoft like Windows Media Player 11. For activating your Windows First you have to Deactivate you Current Windows Using Registry Editor.</span>

<span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><span style="color: #000000;"><strong>1.</strong></span>Type regedit in RUN and go to this key</span>

> <span style="color: #000000;"><strong><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;">HKEY_LOCAL_MACHINE/SOFTWARE/Microsoft/Windows NT/CurrentVersion/WPAEvents</span></strong></span>

<span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;">Now, in the Right side double-click OOBETimer.Then Select the values I have selected in the screenshot and </span>Press backspace then Click OK and Exit Registry Editor.

<img class="alignnone size-full wp-image-1865" title="DeactivateWindows" src="http://www.askprateek.tk/wp-content/uploads/2011/08/DeactivateWindows1.jpg" alt="" width="375" height="323" />

<span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><span style="color: #000000;"><strong>2.</strong></span> Now you need to Enter a Genuine key to Activate Windows. We will use an In-build Windows tool to  Activate Windows. So, type this in RUN:</span>

<span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><br /> </span>

> <span style="color: #000000;"><strong><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;">%systemroot%system32oobemsoobe.exe /a</span></strong></span><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><br /> </span>

<img class="alignright" style="border-color: initial; border-style: initial;" title="Second Option" src="http://lh4.ggpht.com/_wTYo1VncEdM/SrR-dveR_rI/AAAAAAAAAtg/fzyS7wS3sq8/1_thumb.png" alt="" width="189" height="124" />

<span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;">3. Now in The Let’s Activate Windows dialog box,Check the Second option[yes,I want to activate windows..]</span>

&nbsp;

&nbsp;

<span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;">Then,Click Change Product Key button and you will have this Dialog Box.</span>

<span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><img src="http://u.damasgate.com/files/8sdweit0a8opj9eikop0.jpg" alt="" /><br /> </span><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><br /> </span><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><span style="color: #000000;"><strong>4.</strong></span> In the Front of  NEW KEY Enter this Key:</span><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><br /> </span>

> <span style="color: #000000;"><strong><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;">THMPV-77D6F-94376-8HGKG-VRDRQ</span></strong></span>

<span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><strong>5.</strong>Click Update and Restart Windows.Now you can Enjoy your Genuine Windows</span>

> <div>
>   <span style="text-decoration: underline; color: #000000;"><strong>Note:</strong></span> If you don’t know about Registry Editor then you can download a ready-made Registry Script and merge in your current Registry.
> </div>
> 
> <div>
>   <span style="text-decoration: underline;"><strong><a title="Registry Script to Deactivate Windows by Prateek Kumar" href="http://www.deviantart.com/download/199047301/deactivate_windows_by_prateekkumar-d3ai9vp.rar">Download Registry Script to Deactivate Windows</a></strong></span>
> </div>

<span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><br /> </span>

> <span style="color: #000000;"><strong><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;">Method 2</span></strong></span>

<span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><br /> </span><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;">Method 2 is Quite simple use KEY FINDER to change the Key. Click on <strong>Options&#8211;>Change Product Key</strong>. Then Enter the new Key</span><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><a href="http://www.askprateek.tk/?attachment_id=20" rel="attachment wp-att-20"><br /> </a><img src="http://mjb.imstatic.net/im/screen.png" alt="KeyFinder screenshot" /><br /> </span><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><br /> </span><a href="http://www.magicaljellybean.com/keyfinder/" target="_blank"><span style="text-decoration: underline;"><strong><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;">Download Key Finder</span></strong></span></a><a href="http://www.magicaljellybean.com/keyfinder/" target="_blank"><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><br /> </span></a>

> <span style="color: #000000;"><strong><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;">Method 3</span></strong></span>

<span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;"><br /> </span><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif;">In Method 3 you need to Reinstall Windows and Enter a Genuine key.  :p</span><span style="text-decoration: underline;"><strong><span style="font-family: 'Helvetica Neue', Helvetica, Arial, 'Nimbus Sans L', sans-serif; text-decoration: underline;">Please Comment ,your one comment can inspire me</span></strong></span>

 [1]: http://askprateek.tk/windows-7-loader-activate-any-version-of-windows-7-with-genuine-key/ "Windows 7 Loader: Activate any version of Windows 7 with Genuine Key"