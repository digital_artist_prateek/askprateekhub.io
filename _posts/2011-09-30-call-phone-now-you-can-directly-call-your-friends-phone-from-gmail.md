---
title: '&#8220;Call Phone&#8221;: Now you can directly call your friends phone from Gmail.'
author: Ask Prateek
layout: post
permalink: /call-phone-now-you-can-directly-call-your-friends-phone-from-gmail/
jabber_published:
  - 1317391287
  - 1317391287
  - 1317391287
  - 1317391287
  - 1317391287
  - 1317391287
  - 1317391287
  - 1317391287
  - 1317391287
  - 1317391287
  - 1317391287
  - 1317391287
yourls_shorturl:
  - http://ask-pra.tk/z
  - http://ask-pra.tk/z
  - http://ask-pra.tk/z
  - http://ask-pra.tk/z
sfw_comment_form_password:
  - 1mxI1CZLOEZ0
  - 1mxI1CZLOEZ0
  - 1mxI1CZLOEZ0
  - 1mxI1CZLOEZ0
categories:
  - Internet
tags:
  - +
  - call
  - country
  - every
  - google
  - phone
  - voice
---
<!--more-->

Gmail voice and video chat makes it easy to stay in touch with friends and family using your computer’s microphone and speakers. But until now, this required both people to be at their computers, signed into Gmail at the same time. Given that most of us don’t spend all day in front of our computers, we thought, “wouldn’t it be nice if you could call people directly on their phones?”

When Google + goes Public ,Google has also released a new feature for there users **&#8220;Call Phone&#8221;.** Yes, Now you can call your friend directly from your gmail account.

From Google official Blog:

> Calls to the U.S. and Canada will be free for at least the rest of the year and calls to other countries will be billed at our very low rates. We worked hard to make these rates really cheap (see <span style="text-decoration: underline;"><strong><a href="http://www.google.com/chat/voice/compare.html">comparison table</a></strong></span>) with calls to the U.K., France, Germany, China, Japan—and many more countries—for as little as $0.02 per minute.

Dialing a phone number works just like a normal phone. Just click “Call phone” at the top of your chat list and dial a number or enter a contact’s name.

<img class="alignnone size-full wp-image-1925" title="CallPhone" src="http://www.askprateek.tk/wp-content/uploads/2011/09/CallPhone.jpg" alt="" width="251" height="403" />

If you have a Google Voice phone number, calls made from Gmail will display this number as the outbound caller ID. And if you decide to, you can receive calls made to this number right inside Gmail.

<span style="text-decoration: underline;"><strong><a href="http://www.google.com/support/chat/bin/answer.py?answer=187936" target="_blank">See instructions</a></strong></span>

This Feature is available for everyone. If you don&#8217;t see this try to logout and sign in again.

*If you like this then feel free to post your opinion or if you have something to tell,news, update etc. then you can send us from <span style="text-decoration: underline;"><strong><a title="Contact us" href="http://www.askprateek.tk/contact-us/" target="_blank">here</a></strong></span>*