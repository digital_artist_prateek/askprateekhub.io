---
title: How to get Mac OS or Linux Type Font Rendering in Windows.
author: Ask Prateek
layout: post
permalink: /how-to-get-mac-os-or-linux-type-font-rendering-in-windows-2/
sfw_comment_form_password:
  - USIcnReJWcse
  - USIcnReJWcse
jabber_published:
  - 1343379241
  - 1343379241
email_notification:
  - 1343379243
  - 1343379243
yourls_shorturl:
  - http://ask-pra.tk/g
  - http://ask-pra.tk/g
categories:
  - Softwares
tags:
  - font
  - linux
  - mac
  - redering
  - smoothing
  - type
---
<!--more-->

Hello Everyone!

As you know that font rendering in Mac is just awesome, Drop Down Shadow and corner smoothing. Although Microsoft introduced Clear Type font smoothing in Windows but sometimes a few new fonts such as Segoe UI become hard to read and look too much sharp on screen.

Wouldn&#8217;t it be great if we could get Mac and Linux style font smoothing in Windows? Today in this topic, we are going to share 2 free software which can bring Mac and Linux like font smoothing in Windows. These freeware can be considered as a replacement of the default font rendering service in Windows. These software render Windows fonts smoothly which makes them easier to read and look nice on screen.

So without wasting time, lets share these free software which will help you in making Windows fonts look-like Mac OS and Linux fonts as shown in following image:

<img class="alignnone size-full wp-image-1757" title="Mac type font rendering" src="http://askprateek.tk/wp-content/uploads/2012/07/Mac-type-font-rendering.jpg" alt="" width="600" height="341" />

These 2 freeware are:

  * **gdipp**
  * **mactype**

Installation of both software is easy. Simply follow the instructions given by the setup wizard and complete the installation process.

gdipp is easier to use compared to mactype and you don&#8217;t need to customize any setting in gdipp. It automatically starts its service and brings Mac and Linux like font smoothing in Windows immediately just after the installation.

On the other hand, mactype is highly customizable. It comes with many built-in and pre-configured font smoothing profiles. You can select any of the given profiles or you can also create your own profile. You&#8217;ll need to give each profile a try as different font profiles will work differently depending upon the computer screen and its resolution.

You can download both of them using following links:

> **<a href="http://code.google.com/p/gdipp/" target="_blank">Download gdipp</a>**
> 
> **<a href="http://code.google.com/p/mactype/" target="_blank">Download mactype</a>**

Both of these software work perfectly in **32-bit** and **64-bit** Windows editions.