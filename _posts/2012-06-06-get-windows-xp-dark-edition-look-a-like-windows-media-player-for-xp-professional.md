---
title: Get Windows XP Dark Edition look-a-like Windows Media Player for XP Professional
author: Ask Prateek
layout: post
permalink: /get-windows-xp-dark-edition-look-a-like-windows-media-player-for-xp-professional/
yourls_shorturl:
  - http://ask-pra.tk/2u
sfw_comment_form_password:
  - Ycg3QIhTQ4tX
  - Ycg3QIhTQ4tX
  - Ycg3QIhTQ4tX
  - Ycg3QIhTQ4tX
  - Ycg3QIhTQ4tX
  - Ycg3QIhTQ4tX
  - Ycg3QIhTQ4tX
  - Ycg3QIhTQ4tX
categories:
  - Windows XP
tags:
  - askpk123
  - dark
  - edition
  - media
  - playes
  - skin
  - windows
  - Xp
---
<!--more-->

As you know **Windows XP Dark Edition** contain **Windows Media Player 11** which looks like as following:

![][1]

Now if you want to enjoy the same media player in **Windows XP **, then this article will help you.

We are going to share 1 skin with you  for **WMP11** in XP . This skin can make the WMP 11 under XP  look like WMP11 of Windows XP Dark Edition.

You can download the required skin using following link:

> <span style="text-decoration: underline;"><strong><a title="Download Windows XP Dark Edition Media Player Skin for Xp WMP 11" href="http://www.deviantart.com/download/312971959/windows_xp_dark_edition_media_player_theme_by_prateek_kumar-d56c2pj.zip">Download Windows Xp Dark Edition Skin for WMP11  of Windows XP</a></strong></span> | <span style="text-decoration: underline;"><strong><a title="Alternative link" href="http://prateek-kumar.deviantart.com/art/Windows-XP-Dark-Edition-Media-Player-Theme-312971959" target="_blank">Mirror</a></strong></span>

Now you need to replace the DLL file with the original one . You can Download Replacer from our ‘**<a title="Downloads" href="http://www.askprateek.tk//downloads/" target="_blank">Downloads</a>**‘ Section.

> If you don&#8217;t know how to use Replacer then this link is helpful for you:
> 
> <a title="How to use 3rd Party Themes in Windows/ Patch UxTheme.dll in XP/Vista and Seven." href="http://www.askprateek.tk//how-to-use-3rd-party-themes-in-windows-patch-uxtheme-dll-in-xpvista-and-seven/" target="_blank"><span style="text-decoration: underline;"><strong>How to Replace default .dll files of Windows </strong></span></a>
> 
> In the above link we have discussed how to replace UxTheme.dll to use 3rd party themes, so you this tutorial will also help you to Replace** wmploc.dll.**

 [1]: http://fc06.deviantart.net/fs71/i/2012/188/3/f/windows_xp_dark_edition_media_player_theme_by_prateek_kumar-d56c2pj.png