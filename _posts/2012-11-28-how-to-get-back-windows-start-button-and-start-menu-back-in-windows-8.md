---
title: How to get back Windows Start Button and Start Menu back in Windows 8
author: Ask Prateek
layout: post
permalink: /how-to-get-back-windows-start-button-and-start-menu-back-in-windows-8/
categories:
  - Windows 8
tags:
  - 8
  - back
  - button
  - classic
  - pro
  - shell
  - start
  - vistart
  - windows
---
<!--more-->

<img class="alignright size-full wp-image-2023" title="Bottom Left Corner" src="http://www.askprateek.tk/wp-content/uploads/2012/11/Bottom-Left-Corner1.png" alt="" width="154" height="92" />Almost all of us are very well aware of the fact that Microsoft has removed the **Start button** and **Start Menu **from its latest OS **[Windows 8][1]**. Start Menu has been replaced by the new [Start Screen][2] which shows live tiles of built-in programs, 3rd party programs installed by you and a few useful tools such as weather, RSS feed, etc. Start button has been replaced by a small Start Screen thumbnail which appears when you move your mouse cursor to bottom-left corner of screen.

But many of the users want that old Start Menu Back in Windows 8. So Today We are sharing few Freewares which will help you to bring back the old Start Menu.

So without wasting time let&#8217;s take a look at the list:

> <p style="text-align: center;">
>   <strong>1. Classic Shell</strong>
> </p>

Classic Shell is an amazing tool that will allow you to get back the old start menu. It also allows you to get the good old classic Explorer features such as classic navigation pane, fully functional status bar, shared folder overlay icon and much more. It works in both Windows 7 and Windows 8.

<img class="alignnone size-full wp-image-2026" title="Classic Shell" src="http://www.askprateek.tk/wp-content/uploads/2012/11/Classic-Shell.png" alt="" width="449" height="517" />

You can read more about it and download it using following link:

<span style="text-decoration: underline;"><strong><a href="http://www.askvg.com/get-start-button-orb-and-classic-start-menu-back-in-windows-8-using-classic-shell/">Download Classic Shell</a></strong></span>

> <p style="text-align: center;">
>   <strong>2. ViStart</strong>
> </p>

**ViStart** was one of those first freeware which were created to provide Windows Vista look-like Start Menu in Windows XP. Then it was updated to mimic Windows 7 Start Menu and now it can also bring the same Start Menu and Start button to Windows 8.

![Windows 8 start menu preview][3]

You can download it from the following link:

<span style="text-decoration: underline;"><strong><a title="Download link" href="http://lee-soft.com/vistart/" target="_blank">Download ViStart</a></strong></span>

**<span style="text-decoration: underline;"><em>Hope you like these tools. These will definitely help you to bring back Start Button</em></span>**

 [1]: http://www.askprateek.tk/windows-8-features-all-about-new-additions-in-windows-8/ "[Windows 8 Features] All about new additions in Windows 8"
 [2]: http://www.askprateek.tk/windows-8-review-all-about-mircosoft-new-operating-system-windows-8/ "[Windows 8 Review] All about Mircosoft new Operating System Windows 8."
 [3]: http://lee-soft.com/vistart/Start-Menu.jpg