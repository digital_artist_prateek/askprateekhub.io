---
title: '[Ultimate Collection] 77 Tips to make Windows 7 Super fast.'
author: Ask Prateek
layout: post
permalink: /ultimate-collection-77-tips-to-make-windows-7-super-fast/
jabber_published:
  - 1317983447
  - 1317983447
  - 1317983447
  - 1317983447
  - 1317983447
  - 1317983447
  - 1317983447
  - 1317983447
  - 1317983447
  - 1317983447
  - 1317983447
  - 1317983447
  - 1317983447
  - 1317983447
  - 1317983447
  - 1317983447
yourls_shorturl:
  - http://ask-pra.tk/1i
  - http://ask-pra.tk/1i
  - http://ask-pra.tk/1i
  - http://ask-pra.tk/1i
sfw_comment_form_password:
  - 1jaF9A8hPDC4
  - 1jaF9A8hPDC4
  - 1jaF9A8hPDC4
  - 1jaF9A8hPDC4
categories:
  - Windows Se7en
tags:
  - 7
  - 77
  - fast
  - keyboard
  - shortcuts
  - super
  - tips
  - tricks
  - windows
---
<!--more-->

Windows 7 is already a fast Windows created by Microsoft. In the past We have posted many Articles on<span style="text-decoration:underline;"><strong><a title="Will tell you more about Windows 7" href="http://www.askprateek.tk/category/windows-se7en/" target="_blank"> Windows 7</a></strong></span>. NOw we posting about 77 tips for making Windows 7 super fast and extract juice out of it. We have found it on Microsoft Website of Technet.

<span style="text-decoration:underline;"><strong><a href="http://technet.microsoft.com/en-us/magazine/2009.10.77windows.aspx" target="_blank">77 Windows 7 Tips</a> </strong></span>

We are posting some Best Windows 7 Keyboard shortcuts, which we found on that Website. As The Windows can performs a wide variety of functions. Here are a handful of the most useful ones:

> <div id="id0480002">
>   <strong>1. Win+h -</strong> Move current window to full screen
> </div>
> 
> <div id="id0480003">
>   <strong>2. Win+i -</strong> Restore current full screen window to normal size or minimize current window if not full screen
> </div>
> 
> <div id="id0480004">
>   <strong>3. Win+Shift+arrow -</strong> Move current window to alternate screen
> </div>
> 
> <div id="id0480005">
>   <strong>4. Win+D</strong> &#8211; Minimize all windows and show the desktop
> </div>
> 
> <div id="id0480006">
>   <strong>5. Win+E  -</strong> Launch Explorer with Computer as the focus
> </div>
> 
> <div id="id0480007">
>   <strong>6. Win+F -</strong> Launch a search window
> </div>
> 
> <div id="id0480008">
>   <strong>7. Win+G -</strong> Cycle through gadgets
> </div>
> 
> <div id="id0480009">
>   <strong>8. Win+L -</strong> Lock the desktop
> </div>
> 
> <div id="id0480010">
>   <strong>9. Win+R -</strong> Open the Run window
> </div>
> 
> <div id="id0480012">
>   <strong>10. Win+T -</strong> Cycle through task bar opening Aero Peek for each running item
> </div>
> 
> <div id="id0480013">
>   <strong>11. Win+U -</strong> Open the Ease of Use center
> </div>
> 
> <div id="id0480014">
>   <strong>12. Win+Space -</strong> Aero Peek the desktop
> </div>
> 
> <div id="id0480015">
>   <strong>13. Ctrl+Win+Tab -</strong> Open persistent task selection window, roll mouse over each icon to preview item and minimize others
> </div>
> 
> <div>
>   <strong>14. Win+M -</strong> Minimize the current window
> </div>

&nbsp;