---
title: '[Windows 8 Features] All about new additions in Windows 8'
author: Ask Prateek
layout: post
permalink: /windows-8-features-all-about-new-additions-in-windows-8/
categories:
  - Windows 8
tags:
  - 8
  - features
  - list
  - new
  - pro
  - snap
  - windows
  - winx
  - xbox
---
<!--more-->

As you know that Microsoft has released it&#8217;s operating system Windows 8, which is now available for customers. We have also written a **<span style="text-decoration: underline;"><a title="[Windows 8 Review] All about Mircosoft new Operating System Windows 8." href="http://www.askprateek.tk/windows-8-review-all-about-mircosoft-new-operating-system-windows-8/">Review on Microsoft Windows 8</a></span>** in past. Now we are here to tell you about all the feature of Windows 8.

So, without wasting time let&#8217;s start this article about **Windows 8 Ultimate Feature **which are never seen before in any operating system

> <p style="text-align: center;">
>   <strong>1. Quick Access Menu [Win+X Menu]</strong>
> </p>

As we know that Micrsoft has removed the Start Button from Windows 8 and added <span style="text-decoration: underline;"><strong><a title="Pulmon 2.0: Get Windows 8 Live Tiles “Start Screen” in Windows XP, Vista and 7" href="http://www.askprateek.tk/pulmon-2-0-get-windows-8-live-tiles-start-screen-in-windows-7/">Start Screen</a></strong></span>. But they have added a Context Menu which provides easier access to many useful system tools such as:

  * Programs and Features
  * Power Options
  * System Settings
  * Device Manager
  * Command Prompt
  * Task Manager
  * Control Panel
  * Search
  * Run

<img class="alignnone size-full wp-image-1981" title="Win +X Menu" src="http://www.askprateek.tk/wp-content/uploads/2012/11/Win-+X-Menu.png" alt="" width="272" height="386" />

To access this Menu Just Right-Click on Mini Start Screen which appears when we move the mose to the bottom left corner. Also you can use **Win+X** keyboard Shortcut to Access this menu.

> <p style="text-align: center;">
>   <strong>2. Windows 8 Snap Feature (Multi-Tasking)</strong>
> </p>

Windows 8 is inspired from Windows phone, So Microsoft has added an awesome feature called &#8220;**Snap**&#8221; which allows you to perform multiple tasks at the same time. Here is a preview of this feature live in action:

<img class="alignnone size-full wp-image-1974" title="Snap Preview" src="http://www.askprateek.tk/wp-content/uploads/2012/11/Snap-Preview.jpg" alt="" width="700" height="393" />

> **How to use ?**

**1. **To use this feature just move your mouse at the top then you will see a hand instead of Cursor. First drag the hand down and then left or right until you see a bar with three dots.

**2. **You will see coloured space on the other side. First click on it to open start screen. Then run any app which you want for multi task and repeat the step one for multi tasking

If you are facing problem in using this feature, then here is a Video Tutorial:



> <p style="text-align: center;">
>   <strong>3. Social Network Integration</strong>
> </p>

This is one of the most amazing feature of Windows 8. Microsoft has integrated Social Network in People&#8217;s Widget. When we are connected Facebook via People&#8217;s Widget then Via Messaging Widget we can chat with our friends. If our friend tries to chat with us then it shows a notification at top corner showing the friend name and the message:

<img class="size-full wp-image-1988 alignnone" title="Multi Task Chat" src="http://www.askprateek.tk/wp-content/uploads/2012/11/Multi-Task-Chat.jpg" alt="" width="700" height="393" />

We really love this feature. We can work on one side and can chat on the other side. Similar feature is also introduced by Mozilla in Firefox. They have added Social API in Latest version of Firefox. It shows facebook notification, Facebook Sidebar showing online friends etc. You can also enable this feature by following this article:

<span style="text-decoration: underline;"><strong><a href="http://www.askprateek.tk/how-to-enable-build-in-facebook-messanger-in-mozilla-firefox/">How to Enable Build-in Facebook Messanger in Mozilla Firefox</a></strong></span>

> <p style="text-align: center;">
>   <strong>4. Windows 8 App Store</strong>
> </p>

Finally Microsoft has added App store in Windows 8 which allows you to Download great apps in HD which were available for Windows Phone only. Windows 8 app store has almost all require apps which a normal busy person needs.

<img class="alignnone size-full wp-image-1860" title="Screenshot (6)" src="http://www.askprateek.tk/wp-content/uploads/2011/11/Screenshot-6.png" alt="" width="700" height="393" />

Since I am a big fan of Football, but even then I can&#8217;t view all the matches. I won&#8217;t get all the details of what&#8217;s happening now. So, I am using an app from app store, which show latest news even on the tile

> <p style="text-align: center;">
>   <strong>5. Universal Search</strong>
> </p>

Windows 8 Modern Search allow you to search everything on the PC. If you are browsing app store then it will search App store, If you are at start screen then It will search for installed apps, settings, files etc.

<img class="alignnone size-full wp-image-1978" title="Search" src="http://www.askprateek.tk/wp-content/uploads/2012/11/Search.png" alt="" width="344" height="414" />

If you search for a Website then it show a link of the website and open it in Internet Explorer.

**How to Access Search ?**

Well this is not difficult. Search is present in Charms bars which you use when we need to shutdown Windows

> <p style="text-align: center;">
>   <strong>6. Xbox gaming Titles</strong>
> </p>

Yes, Now Xbox games are available for PC. You can buy and play XBox Games in your machine. All you need is your XBox Live ID to login and then you can enjoy all those latest games without buying an XBox.

<img class="alignnone size-full wp-image-1979" title="XBox Gaming" src="http://www.askprateek.tk/wp-content/uploads/2012/11/XBox-Gaming.jpg" alt="" width="700" height="393" />

> <p style="text-align: center;">
>   <strong>7. Auto Print Screen [PrntScr]</strong>
> </p>

That&#8217;s an awesome addition to Windows 8. In previous Windows versions, whenever you needed to take a screenshot of your screen, you had to press &#8220;**PrntScr**&#8221; key or if you wanted to take screenshot of a specific program window, you needed to click on it to make it active and press &#8220;**Alt+PrntScr**&#8221; keys. After doing this, the screenshot was captured by clipboard and you had to paste and save it using an image editing software like built-in MS Paint, Adobe Photoshop, etc.

<img class="alignnone size-full wp-image-1980" title="Auto Screen Capture" src="http://www.askprateek.tk/wp-content/uploads/2012/11/Auto-Screen-Capture.png" alt="" width="700" height="398" />

Windows 8 makes the whole process a lot easier. You can just press &#8220;**Win+PrntScr**&#8221; keys together and Windows 8 will automatically save the screenshot in your &#8220;**Pictures**&#8221; library folder. The screenshot is saved with the name &#8220;**Screenshot.png**&#8220;. If you take more screenshots, they are saved with an added number such as &#8220;**Screenshot (2).png**&#8220;, &#8220;**Screenshot (3).png**&#8221; and so on.

> <p style="text-align: center;">
>   <strong>8. New Hot Corners in Screen</strong>
> </p>

Windows 8 comes with hot corner feature which allows you to access some built-in options when you move your mouse cursor in a corner of your monitor screen.

<span style="text-decoration: underline;"><strong>Bottom Left Corner</strong></span>

<img class="alignright size-full wp-image-1982" title="Bottom Left Corner" src="http://www.askprateek.tk/wp-content/uploads/2012/11/Bottom-Left-Corner.png" alt="" width="154" height="92" />

When you move the cursor to bottom left corner the it will show you a mini Start Screen. When you click on it then you will be take to the new Modern Start Screen

&nbsp;

<span style="text-decoration: underline;"><strong>Top Left Corner</strong></span>

If you will move the cursor to the Top left of the screen then Windows 8 will allow you to switch between running Metro Apps and Desktop and if you will just move the cursor down from the Top Left then It will show a list of all running apps

<img class="size-full wp-image-1983 alignnone" title="Top Left Corner" src="http://www.askprateek.tk/wp-content/uploads/2012/11/Top-Left-Corner.jpg" alt="" width="700" height="393" />

<span style="text-decoration: underline;"><strong>Right Corners</strong></span>

If you move the cursor to any of the right corner then it will show you charms bar of which you are all ready aware, Where you can access Settings, Search, Share, Devices and Start Screen charm.

> <p style="text-align: center;">
>   <strong>9. Virtual Drive Support</strong>
> </p>

Windows 8 allows you to mount .ISO files to a Virtual Drive Without using any software so that you can extract files directly. This helps  when we are making a Bootable USB of Windows as mentioned <span style="text-decoration: underline;"><strong><a title="Manual and the Best way to make a Bootable USB for installing Windows Vista, 7 & 8." href="http://www.askprateek.tk/manual-and-the-best-way-to-make-a-bootable-usb-for-installing-windows-vista-7-8/">here</a></strong></span>

<img class="size-full wp-image-1987 alignnone" title="Virtual Drive" src="http://www.askprateek.tk/wp-content/uploads/2012/11/Mount.png" alt="" width="459" height="182" />

*That&#8217;s all for now guys! We&#8217;ll keep updating the list whenever we find another secret stuff in Windows 8 so keep checking this place in future.*

*If you have also come across a new and special feature of Windows 8, feel free to share it in your comment.*