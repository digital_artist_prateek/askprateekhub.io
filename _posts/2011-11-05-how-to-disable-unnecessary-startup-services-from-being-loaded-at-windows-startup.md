---
title: How to Disable unnecessary Startup services from being loaded at Windows Startup
author: Ask Prateek
layout: post
permalink: /how-to-disable-unnecessary-startup-services-from-being-loaded-at-windows-startup/
jabber_published:
  - 1320474192
  - 1320474192
  - 1320474192
  - 1320474192
  - 1320474192
  - 1320474192
  - 1320474192
  - 1320474192
  - 1320474192
  - 1320474192
  - 1320474192
  - 1320474192
  - 1320474192
  - 1320474192
  - 1320474192
  - 1320474192
email_notification:
  - 1320474193
  - 1320474193
  - 1320474193
  - 1320474193
  - 1320474193
  - 1320474193
  - 1320474193
  - 1320474193
  - 1320474193
  - 1320474193
  - 1320474193
  - 1320474193
  - 1320474193
  - 1320474193
  - 1320474193
  - 1320474193
reddit:
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
  - 'a:2:{s:5:"count";s:1:"0";s:4:"time";s:10:"1339461596";}'
sfw_comment_form_password:
  - 3MewzjZRqSFh
  - 3MewzjZRqSFh
  - 3MewzjZRqSFh
  - 3MewzjZRqSFh
yourls_shorturl:
  - http://ask-pra.tk/i
  - http://ask-pra.tk/i
  - http://ask-pra.tk/i
  - http://ask-pra.tk/i
categories:
  - Troubleshooting
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - disable
  - off
  - optimize
  - performance
  - processes
  - seed
  - sefly
  - services
  - startup
  - turn
  - unnecessary
---
<!--more-->

Many a times when you install any software it also install some unnecessary tools and add theme in Windows Startup. This decrease Windows Startup speed and you have to wait a lot till all the services start. So, I am going to share this Tutorial to Disable those unnecassary services from being loaded at Windows Startup. So, let&#8217;s start our Tutorial.

> <p style="text-align: center;">
>   <strong>Startup Processes</strong>
> </p>

**1.** First click on **start&#8211;>Run** OR press **[Win] + R** key.

**2.** Then type <span style="color: #000000;"><strong>msconfig</strong></span> in Run Dialog box. Then it will show up System Configuration Utility Dialog box.

**3.** Click on the Startup tab and uncheck those services which you don&#8217;t want to be loaded at next System startup.

<img class="alignnone size-full wp-image-2250" alt="msconfig" src="http://www.askprateek.tk/wp-content/uploads/2011/11/msconfig.png" width="585" height="391" />

**4.**Click OK and click Exit Without Restart.

> <p style="text-align: center;">
>   <strong>BackGround Processes</strong>
> </p>

When we install **Windows **, there are many services running in background. Among them many services are not important and can be set **manual** to make your windows running faster.

**1.** Right-click on **My Computer** icon on desktop and select **Manage**, it&#8217;ll open a window, in which go to:**Services & Application -> Services**. To disable or Set a service to MANUAL, double-click on the service and select the desired option in **Startup** list box.

<img class="alignnone size-full wp-image-2245" alt="Disable any unnecessary Service" src="http://www.askprateek.tk/wp-content/uploads/2012/07/Double-Click-Windows-Installer-e1357806028155.png" width="600" height="349" />

**2.** You can also open the same by going to **Administrative Tools** and open **Computer Management**.

> **NOTE:** Always set the service to MANUAL, never disable it. So that whenever windows needs to start a service it can easily start and use it. If you set any service to DISABLED, then windows will not be able to start it and will give ERROR.

*So here is the list of services in  that can be safely set to MANUAL (Many of them may be disabled automatically on some systems):*

<span style="text-decoration: underline; color: #000000;"><strong>Windows XP:</strong></span>

  * **Alerter**
  * **Computer Browser**
  * **Distributed Link Tracking Client** (If you have NTFS partition, then don&#8217;t disable it)
  * **Fast User Switching Capability** (removes Switch Users option)
  * **Indexing service**
  * **Internet Connection Firewall/Internet Connection Sharing**
  * **Messenger**
  * **Remote Registry** (Always disable it for Security purposes)
  * **Secondary Logon**
  * **Server**
  * **System Restore** (If you don&#8217;t use System Restore)
  * **TCP/IP NetBIOS Helper Service**
  * **Uninterruptible Power Supply**
  * **Upload Manager**
  * **Wireless Zero Configuration**

<span style="text-decoration: underline; color: #000000;"><strong>Windows Vista/Seven:</strong></span>

  * **Application Experience**
  * **Computer Browser** (If your computer is not connected to any network)
  * **Desktop Window Manager Session Manager** (If you don&#8217;t use Aero theme)
  * **Diagnostic Policy Service**
  * **Distributed Link Tracking Client**
  * **IP Helper **(Windows 7)**  
    **
  * **Indexing service** (Windows Vista-If you don&#8217;t use Windows Search feature frequently)
  * **Offline Files**
  * **Portable Device Enumerator Service**
  * **Print Spooler** (If you don&#8217;t have Printer)
  * **Protected Storage **(Windows 7)
  * **Remote Registry** (Always disable it for Security purposes)
  * **Secondary Logon**
  * **Security Center**
  * **System Restore **(Vista)
  * **Server** (If your computer is not connected to any network)
  * **Tablet PC Input Service**
  * **TCP/IP NetBIOS Helper  
    **
  * **Themes** ( If you use Windows Classic theme)
  * **Windows Error Reporting Service**
  * **Windows Media Center Service Launcher**
  * **Windows Search** (If you don&#8217;t use Windows Search feature frequently)
  * **Windows Time** (If you don&#8217;t want to update system tray clock time using Internet)

**PS: **You can see the details of each service so that you can determine whether it should be disabled or not