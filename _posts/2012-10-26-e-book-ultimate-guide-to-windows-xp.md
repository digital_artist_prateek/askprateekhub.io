---
title: '[E-Book] Ultimate guide to Windows XP'
author: Ask Prateek
layout: post
permalink: /e-book-ultimate-guide-to-windows-xp/
yourls_shorturl:
  - http://ask-pra.tk/3e
  - http://ask-pra.tk/3e
categories:
  - E-Books
tags:
  - basic
  - book
  - e
  - e-book
  - guide
  - install
  - windows
  - Xp
---
<!--more-->

Hello Everyone, Today we are sharing an **Ultimate Guide to Windows XP**. This E-Book will cover many topics related to Windows XP Basics like Installing Windows, Creating Accounts, Installing other Softwares as shown below:

**Chapter 1 Operating Systems—The Basics**

[<img class="alignright size-full wp-image-2165" alt="Windows XP" src="http://www.askprateek.tk/wp-content/uploads/2012/10/Windows-XP.jpg" width="414" height="638" />][1]1.1 What Is Thy Purpose?  
1.2 The Heart, The Soul, The Kernel  
1.3 The Juggler—Multitasking  
1.4 The Manager—Memory Management  
1.5 Putting It All Away—Managing Storage

**Chapter 2 The Evolution Of Windows **

2.1 The History of the Operating System  
2.2 From DOS to Windows XP  
2.3 The many Faces of Windows XP

**Chapter 3 Getting Started with Windows XP **

3.1 Windows XP: Step-By-Step Installation  
3.2 Installing Device Drivers  
3.3 Creating User Accounts  
3.4 Creating an Internet connection  
3.5 Post-Install Stuff To Add

**Chapter 4 Using Windows XP **

4.1 Getting Around In Windows XP  
4.2 Managing Files, Folders, And Documents  
4.3 Searching For Files, Folders,  
And Documents  
4.4 Printing  
4.5 Using Optical Media  
4.6 The Control Panel  
4.7 Addressing Compatibility Issues

**Chapter 5 Getting Online With Windows XP **

5.1 Browsing  
5.2 E-Mail  
5.3 Instant Messaging

**Chapter 6 Customising And Enhancing Win XP**

6.1 The eXPerience  
6.2 Tweaking Visual Settings  
6.3 Third-Party Software

You can download it from the following link:

> **<a title="Windows XP " href="http://www.mediafire.com/?171wqs7or869zst" target="_blank"><span style="text-decoration: underline;">Download Ultimate Guide to Windows XP</span></a> ( PDF File )**

&nbsp;

 [1]: http://www.AskPrateek.tk