---
title: Windows 8 Will Provide New GUI (User Interface) Codenamed “Wind”
author: Ask Prateek
layout: post
permalink: /windows-8-will-provide-new-gui-user-interface-codenamed-wind/
jabber_published:
  - 1317378709
  - 1317378709
  - 1317378709
  - 1317378709
  - 1317378709
  - 1317378709
  - 1317378709
  - 1317378709
  - 1317378709
  - 1317378709
  - 1317378709
  - 1317378709
  - 1317378709
  - 1317378709
  - 1317378709
  - 1317378709
yourls_shorturl:
  - http://ask-pra.tk/s
  - http://ask-pra.tk/s
  - http://ask-pra.tk/s
  - http://ask-pra.tk/s
categories:
  - Windows 8
tags:
  - 8
  - GUI
  - new
  - rummours
  - wind
  - windows
---
<!--more-->

> **Microsoft has released official Download links for Windows 8 Developer Preview**
> 
> <span style="text-decoration:underline;"><strong><a href="http://www.askprateek.tk/2011/09/23/download-windows-8-developer-preview-build-2/" rel="bookmark">[Direct and Torrent links]Download Windows 8 Developer Preview Build.</a></strong></span>

People who are eagerly waiting for some news about Microsoft’s upcoming OS “**Windows 8**“, should get excited about today news.

Our friend site “**<a href="http://www.winrumors.com/rumor-new-windows-8-user-interface-codenamed-wind/" target="_blank">winrumors</a>**” along with “**<a href="http://www.windows8italia.com/2010/12/news-esclusive-su-windows-8-trapelate.html" target="_blank">windows8italia</a>**” is reporting that **Windows 8 will sport a new GUI** codenamed “**Wind**“.

> Microsoft’s next-generation Windows operating system will be **32-bit and 64-bit with two separate interfaces**. The main interface will be codenamed “**Wind**” and will initially only be supported by high-end notebook and desktop PCs with dedicated video cards. **The interface will require around 170MB of video memory**.
> 
> **“Wind” will only activate on 64-bit copies of Windows 8 and will be fully 3D.**
> 
> “Wind” will be “**fully dynamic**” and **able to adapt to user habits**. Icons and shortcuts will adapt to different usage scenarios to speed up daily tasks. Windows 8 is also rumored to include a **new fast hibernation system**. The system will hibernate in around three to six seconds and save all open documents and running tasks.

A recent release of **NVIDIA’s Quadro 265 driver** specifically references a new kernel with the number **6.2** (Windows 7 is 6.1) and includes references to Windows 8:

> **NVIDIA Windows Vista / 7 / 8 (64 bit) Display INF file**  
> **DiskID1 =** “NVIDIA Windows Vista / 7 / **8 (64 bit)** Driver Library Installation Disk 1″  
> NVIDIA_**WIN8** = “NVIDIA”

So following are some interesting things about the claimed Windows 8 GUI codenamed “**Wind**“:

  * It’ll have 2 separate interfaces for 32-bit and 64-bit editions which is quite interesting and new thing.
  * New new GUI “Wind” will only activate in 64-bit edition. May be we find some way to activate in 32-bit editions as well. Who knows? ![;)][1]
  * “Wind” will be fully dynamic. Not sure but it might adopt Windows Phone 7 interface.
  * “Wind” will be able to adopt user habits. That sounds really impressive. ![:)][2]

<div id="ilikeposts">
</div>

<div>
</div>

 [1]: http://s1.wp.com/wp-includes/images/smilies/icon_wink.gif?m=1286669865g
 [2]: http://s2.wp.com/wp-includes/images/smilies/icon_smile.gif?m=1286669865g