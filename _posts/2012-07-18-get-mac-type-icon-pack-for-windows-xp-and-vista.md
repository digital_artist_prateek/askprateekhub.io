---
title: Get MAC Type icon pack for Windows XP and Vista
author: Ask Prateek
layout: post
permalink: /get-mac-type-icon-pack-for-windows-xp-and-vista/
yourls_shorturl:
  - http://ask-pra.tk/2n
  - http://ask-pra.tk/2n
jabber_published:
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
email_notification:
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
  - 1342601979
sfw_comment_form_password:
  - TGdpaONR1xqM
  - TGdpaONR1xqM
  - TGdpaONR1xqM
  - TGdpaONR1xqM
categories:
  - Softwares
  - Windows Vista
  - Windows XP
tags:
  - icon
  - mac
  - redesigned
  - reimagined
  - rendering
  - type
  - v1
  - v2
  - windows
---
<!--more-->

Hello Everyone!

As you know that we love to share themes, icons, wallpapers for Windows. Today we find an awesome Windows Icon pack for XP and Vista users. We really like them.

All the credit goes to **[SaviourMachine][1]**@DA for making these cool icon to give Mac Type Rendering. Here is a screenshot of those icons:

> <p style="text-align: center;">
>   <strong>Windows Icon V1</strong>
> </p>

![][2]

> <p style="text-align: center;">
>   <strong>Windows Icon V2</strong>
> </p>

![][3]

We really like them and support the Icon Developer. You can download it from the following link:

> <span style="text-decoration: underline;"><strong><a href="http://www.deviantart.com/download/49139616/Windows_Icons_V1_by_SaviourMachine.zip">Download Windows Icon V1</a> | <a href="http://www.deviantart.com/download/86569702/Windows_Icons_V2_by_SaviourMachine.zip">Download Windows Icon V2</a></strong></span>

**How to use?**

Visit the following link to know how to install them:

<a href="http://monolistic.deviantart.com/journal/Applying-icons-in-Windows-XP-214148549" target="_blank"><span style="text-decoration: underline;"><strong>Installation guide</strong></span></a>

You can also use Icon Tweaker or Icon Packager for using them.

Also check:

<span style="text-decoration: underline;"><strong><a title="[DarkVersions] Download Windows XP DarkPK Icons for XP." href="http://www.askprateek.tk//darkversions-download-windows-xp-darkpk-icons-for-xp/" target="_blank">Download Windows XP DarkPK [Dark Edition] icon pack for Windows</a></strong></span>

 [1]: http://saviourmachine.deviantart.com/
 [2]: http://fc09.deviantart.net/fs13/i/2007/050/2/1/Windows_Icons_V1_by_SaviourMachine.jpg
 [3]: http://fc00.deviantart.net/fs29/i/2008/145/5/d/Windows_Icons_V2_by_SaviourMachine.jpg