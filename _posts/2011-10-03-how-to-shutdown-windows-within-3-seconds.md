---
title: How to ShutDown Windows within 3 Seconds?
author: Ask Prateek
layout: post
permalink: /how-to-shutdown-windows-within-3-seconds/
jabber_published:
  - 1317664452
  - 1317664452
  - 1317664452
  - 1317664452
  - 1317664452
  - 1317664452
  - 1317664452
  - 1317664452
  - 1317664452
  - 1317664452
  - 1317664452
  - 1317664452
yourls_shorturl:
  - http://ask-pra.tk/1s
  - http://ask-pra.tk/1s
  - http://ask-pra.tk/1s
  - http://ask-pra.tk/1s
categories:
  - Troubleshooting
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - 1
  - 2
  - 3
  - 4
  - 5
  - askpk123
  - down
  - explorer
  - fast
  - PC
  - second
  - shut
  - taskmanager
  - thewindows
  - trick
---
<!--more-->Now a Days most of the PC&#8217;s take almost a minute for shutting down. 1 minute is not much, But who wants to wait in this Fast life. many a time 1 minute is like an Hour. Suppose you have to go outside your Home and you have to ShutDown your PC as fast as possible But because of slow Windows, your PC ShutDown like a snail and at that time 1 minute looks like 1 hour.

Almost all Windows users know about &#8220;**Task Manager**&#8221; which is a built-in tool and also one of the most useful tool in Windows.

We regularly use it to check which services are running in background, which programs are taking how much system resources, etc. We also use it to kill not responding programs.

So, I Decide to share a simple Task Manager Trick to Shutdown PC within 3 Seconds. My PC take about 2 seconds and your can take more than 3 depending  on machine to machine. Just follow these simple steps:

**1.**First start Task Manager any of these three combinations:

  * Press** &#8220;Ctrl + Alt + Delete&#8221;** key.
  * Press &#8220;**Ctrl + Shift + Esc** &#8220;key combination.
  * Right Click Taskbar and select TaskManager.

**2. **Now you have to do nothing, Just** Press and Hold CTRL key** and **click on ShutDown>>Turnoff**. Now your PC will shut down within 3 seconds.

![][1]

*Now enjoy and feel free to Express your Opinion on this Article and Remember We will always here to help you.*

 [1]: http://www.dq.winsila.com/wp-content/uploads/2008/06/quick-shut-down-thumb.jpg