---
title: 'Pulmon 2.0: Get Windows 8 Live Tiles &#8220;Start Screen&#8221; in Windows XP, Vista and 7'
author: Ask Prateek
layout: post
permalink: /pulmon-2-0-get-windows-8-live-tiles-start-screen-in-windows-7/
jabber_published:
  - 1320765187
  - 1320765187
  - 1320765187
  - 1320765187
  - 1320765187
  - 1320765187
  - 1320765187
  - 1320765187
  - 1320765187
  - 1320765187
  - 1320765187
  - 1320765187
email_notification:
  - 1320765188
  - 1320765188
  - 1320765188
  - 1320765188
  - 1320765188
  - 1320765188
  - 1320765188
  - 1320765188
  - 1320765188
  - 1320765188
  - 1320765188
  - 1320765188
tagazine-media:
  - 'a:7:{s:7:"primary";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:6:"images";a:1:{s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";a:6:{s:8:"file_url";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:5:"width";s:3:"761";s:6:"height";s:3:"474";s:4:"type";s:5:"image";s:4:"area";s:6:"360714";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-08 15:21:46";}'
  - 'a:7:{s:7:"primary";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:6:"images";a:1:{s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";a:6:{s:8:"file_url";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:5:"width";s:3:"761";s:6:"height";s:3:"474";s:4:"type";s:5:"image";s:4:"area";s:6:"360714";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-08 15:21:46";}'
  - 'a:7:{s:7:"primary";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:6:"images";a:1:{s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";a:6:{s:8:"file_url";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:5:"width";s:3:"761";s:6:"height";s:3:"474";s:4:"type";s:5:"image";s:4:"area";s:6:"360714";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-08 15:21:46";}'
  - 'a:7:{s:7:"primary";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:6:"images";a:1:{s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";a:6:{s:8:"file_url";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:5:"width";s:3:"761";s:6:"height";s:3:"474";s:4:"type";s:5:"image";s:4:"area";s:6:"360714";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-08 15:21:46";}'
  - 'a:7:{s:7:"primary";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:6:"images";a:1:{s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";a:6:{s:8:"file_url";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:5:"width";s:3:"761";s:6:"height";s:3:"474";s:4:"type";s:5:"image";s:4:"area";s:6:"360714";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-08 15:21:46";}'
  - 'a:7:{s:7:"primary";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:6:"images";a:1:{s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";a:6:{s:8:"file_url";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:5:"width";s:3:"761";s:6:"height";s:3:"474";s:4:"type";s:5:"image";s:4:"area";s:6:"360714";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-08 15:21:46";}'
  - 'a:7:{s:7:"primary";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:6:"images";a:1:{s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";a:6:{s:8:"file_url";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:5:"width";s:3:"761";s:6:"height";s:3:"474";s:4:"type";s:5:"image";s:4:"area";s:6:"360714";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-08 15:21:46";}'
  - 'a:7:{s:7:"primary";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:6:"images";a:1:{s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";a:6:{s:8:"file_url";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:5:"width";s:3:"761";s:6:"height";s:3:"474";s:4:"type";s:5:"image";s:4:"area";s:6:"360714";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-08 15:21:46";}'
  - 'a:7:{s:7:"primary";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:6:"images";a:1:{s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";a:6:{s:8:"file_url";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:5:"width";s:3:"761";s:6:"height";s:3:"474";s:4:"type";s:5:"image";s:4:"area";s:6:"360714";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-08 15:21:46";}'
  - 'a:7:{s:7:"primary";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:6:"images";a:1:{s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";a:6:{s:8:"file_url";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:5:"width";s:3:"761";s:6:"height";s:3:"474";s:4:"type";s:5:"image";s:4:"area";s:6:"360714";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-08 15:21:46";}'
  - 'a:7:{s:7:"primary";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:6:"images";a:1:{s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";a:6:{s:8:"file_url";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:5:"width";s:3:"761";s:6:"height";s:3:"474";s:4:"type";s:5:"image";s:4:"area";s:6:"360714";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-08 15:21:46";}'
  - 'a:7:{s:7:"primary";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:6:"images";a:1:{s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";a:6:{s:8:"file_url";s:112:"http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800";s:5:"width";s:3:"761";s:6:"height";s:3:"474";s:4:"type";s:5:"image";s:4:"area";s:6:"360714";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-08 15:21:46";}'
yourls_shorturl:
  - http://ask-pra.tk/u
  - http://ask-pra.tk/u
  - http://ask-pra.tk/u
  - http://ask-pra.tk/u
sfw_comment_form_password:
  - T3LnlhnpfpeN
  - T3LnlhnpfpeN
  - T3LnlhnpfpeN
  - T3LnlhnpfpeN
categories:
  - Softwares
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - 2.0
  - 8
  - download
  - live
  - pulmon
  - screen
  - seven
  - start
  - tiles
  - windows
---
<!--more-->

<span style="color:#000000;">Pulmon is great application which brings Windows 8 in Windows XP, Vista and 7 PC. Now you can use Windows 8 Start Screen in Windows Xp, Vista and 7. It gives you various options to add as tiles such as <strong>Mozilla Aurora, Storage</strong> etc. Mozilla Aurora widget adds a beautiful tile on desktop. Clicking on it will open Aurora. </span>

> <span style="color:#000000;">Pulmon is created by our friend</span> &#8220;**<a title="Visit Website" href="http://downloadinformer.blogspot.com" target="_blank">Paras Sidhu</a>**&#8220;.<span style="color:#000000;"> He is only 13 years old and has done a fabulous job by creating this application.</span>

<span style="color:#000000;">Here is a screeshot of this Application in action:</span>

<img class="alignnone" src="http://lh6.ggpht.com/-e9aMAsMLa-g/Trf1bsWQeuI/AAAAAAAAAo4/exRscQQj_as/Pulmon_thumb%25255B7%25255D.jpg?imgmax=800" alt="Pulmon" width="761" height="474" />

> **<span style="text-decoration:underline;"><span style="color:#000000;text-decoration:underline;">Here is information about some of its Widgets:</span></span>**

<span style="color:#000000;"><span style="text-decoration:underline;"><strong>User-</strong></span> Clicking “User” tile will open a User Widget. If you want to open User Accounts, double-click on the green place of tile or double-click on User Avatar. There are many tweaks available for user tile. Right click on the tile and click options. You can choose <strong>“Take User Avatar from Windows System”</strong> or choose custom avatar.</span>

<span style="color:#000000;"><span style="text-decoration:underline;"><strong>Control Panel-</strong></span> Clicking Control Panel tile will load control panel widget. You can open Control Panel by clicking on its tile. Its currently not more than a tile. It will get some features in next versions.</span>

<span style="color:#000000;"><span style="text-decoration:underline;"><strong>Clock-</strong></span> Pulmon will provide you a very cool widget “Clock”. It will show you time, month and day of the week. Some new tweaks will be provided to you in the next versions</span>

<span style="color:#000000;"><strong><span style="text-decoration:underline;">Shutdown Manager-</span></strong> It’s a very easy to use widget which will allows you to control over you pc with one click. Shutdown,Restart, Log Off and Hibernate’s Options are given. Please note that some of the features are not working.</span>

<span style="color:#000000;"><strong>Webby-</strong> Webby is another good widget which provides you quick access to tech sites like The Windows Club, IntoWindows,WinMatrix, AskVG etc..</span>

**<span style="color:#000000;">You can download Pulmon from the following link:</span>**

> <span style="text-decoration:underline;"><strong><a href="http://fc03.deviantart.net/fs71/f/2011/311/c/a/pulmon_2_0_by_parassidhu-d416eh1.rar">Download Pulmon</a> </strong></span>

<span style="text-decoration:underline;color:#000000;"><strong>How to Use ?</strong></span>

<span style="color:#000000;"><strong>1.</strong> Download and Run the application</span>

<span style="color:#000000;"><strong>2.</strong> You will see two groups “Add Tiles” and “Remove Tiles”</span>

<span style="color:#000000;"><strong>3.</strong> Click a tile in Add Tiles group e.g Control Panel Tile</span>

<span style="color:#000000;"><strong>4.</strong> It will load Control Panel tile and the icon of Control Panel will be disappeared from “Add Tiles” group and start appearing in “Remove Tiles” group</span>

<span style="color:#000000;"><strong>5.</strong> Click the Control Panel tile in Remove Tiles group will remove it</span>