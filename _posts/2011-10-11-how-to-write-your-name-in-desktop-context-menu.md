---
title: How to Write your name in Desktop Context Menu
author: Ask Prateek
layout: post
permalink: /how-to-write-your-name-in-desktop-context-menu/
jabber_published:
  - 1318350264
  - 1318350264
  - 1318350264
  - 1318350264
  - 1318350264
  - 1318350264
  - 1318350264
  - 1318350264
  - 1318350264
  - 1318350264
  - 1318350264
  - 1318350264
  - 1318350264
  - 1318350264
  - 1318350264
  - 1318350264
sfw_comment_form_password:
  - eEODjFjEC5TO
  - eEODjFjEC5TO
  - eEODjFjEC5TO
  - eEODjFjEC5TO
yourls_shorturl:
  - http://ask-pra.tk/1t
  - http://ask-pra.tk/1t
  - http://ask-pra.tk/1t
  - http://ask-pra.tk/1t
categories:
  - Resource Hacker
  - Windows XP
tags:
  - add
  - context
  - desktop
  - hacker
  - menu
  - name
  - resource
  - Resource Hacker
  - shell32.dll
  - your
---
<!--more-->

> **NOTE 1:** If you are facing problems while saving the file after editing in resource hacker, then make sure you have disabled WFP (Windows File Protection) service using WFP Patcher, it can be found in our “**<a title="Downloads" href="http://www.askprateek.tk/downloads/" target="_blank">Download</a>**” section.
> 
> Also if you are getting** error ”Can’t create file…**“, that means you have edited and saved the file in past and there is a backup file which needs to be deleted before saving this file again. Go to “**System32**” folder and you’ll see a file having “***shell32_original.dll***“. Delete it and try to save the file in resource hacker.

Today I will teach you how to Write your name in Desktop Context Menu. You will need Resource Hacker for this task, which can be Downloaded from our &#8220;**<span style="text-decoration:underline;"><a title="Downloads" href="http://www.askprateek.tk/downloads/" target="_blank">Download</a></span>**&#8221; Section. Just follow these steps:

<img class="alignnone size-full wp-image-577" title="Enabled &quot;VIEW&quot; Menu" src="http://thewindexpl.files.wordpress.com/2011/10/view-menu2.png" alt="" width="394" height="377" />

**1. **Open **%windir%System32Shell32.dll** file in Resource Hacker.

**2. **Goto: **Menu -> 215 -> 1033**.

**3. **In right-side pane, add the following line anywhere you want to show your name

> MENUITEM &#8220;**.:[Prateek Kumar]:.**&#8220;, 65535, MFT\_STRING, MFS\_ENABLED

Suppose if you want to show your name above View Menu then add the above code before this line

> POPUP &#8220;&View&#8221;, 28674, MFT\_STRING, MFS\_ENABLED, 0

Then you will have something like this:

> <div>
>   215 MENUEX
> </div>
> 
> <div>
>   LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US
> </div>
> 
> <div>
>   {
> </div>
> 
> <div>
>   POPUP &#8220;&#8221;, 0, MFT_STRING, MFS_ENABLED, 0
> </div>
> 
> <div>
>   {
> </div>
> 
> <div>
>   MENUITEM &#8220;.:[Prateek Kumar]:.&#8221;, 65535, MFT_STRING, MFS_ENABLED
> </div>
> 
> <div>
>   MENUITEM &#8220;www.AskPK123.com&#8221;, 65535, MFT_STRING, MFS_ENABLED
> </div>
> 
> <div>
>   MENUITEM &#8220;&#8221;, 65535, MFT_SEPARATOR, MFS_ENABLED
> </div>
> 
> <div>
>   POPUP &#8220;&View&#8221;, 28675, MFT_STRING, MFS_ENABLED, 0
> </div>

<div>
  Notice <strong><span style="color:#000000;">MFS_ENABLED</span></strong> in the end of 6 and 7 line which we have added in original code, You can change it to <span style="color:#000000;"><strong>MFS_GRAYED</strong></span> then the colour of your name will change to Gray, same like <strong>Paste</strong> and <strong>Paste Shortcut</strong> text in the menu.
</div>

> <div>
>    <strong>MFS_ENABLED &#8211;> </strong>For Black Colour
> </div>
> 
> <div>
>   <strong></strong><strong>MFS_GRAYED     &#8211;> </strong>For Gray Colour
> </div>

<div>
  4. Compile the script and save the file[ Don&#8217;t select save as, because then you have to Replace your shell32.dll].
</div>

<div>
  Now restart your PC then you will see your name in Desktop Contest Menu.
</div>

<span style="text-decoration:underline;"><strong>Also Check:</strong></span>

<span style="text-decoration:underline;"><strong><a title="How to enable “View” Menu in Desktop Context Menu" href="http://www.askprateek.tk/2011/10/11/how-to-enable-view-menu-in-desktop-context-menu/">How to enable &#8220;VIEW&#8221; menu in Destop Context menu</a> </strong></span>

<span style="text-decoration:underline;"><strong><a href="http://www.askprateek.tk/2011/10/07/filemenu-tools-a-free-tool-to-customizer-context-menu-in-windows-xp-vista-and-7/" rel="bookmark">FileMenu Tools: A free tool to customize Explorer and Desktop Context menu in Windows XP, Vista and 7</a></strong></span>