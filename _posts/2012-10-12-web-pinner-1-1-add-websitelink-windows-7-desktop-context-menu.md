---
title: 'Web Pinner 1.1: Add any website/link in Windows 7 Desktop Context menu'
author: Ask Prateek
layout: post
permalink: /web-pinner-1-1-add-websitelink-windows-7-desktop-context-menu/
yourls_shorturl:
  - http://ask-pra.tk/31
  - http://ask-pra.tk/31
categories:
  - Softwares
  - Windows Se7en
tags:
  - 7
  - context
  - desktop
  - menu
  - pinner
  - webpinner
  - website
  - windows
---
<!--more-->

Hello Everyone

Have you ever thought of Adding a Website url in Right click menu. Yes, Our Friend **<a href="http://downloadinformer.blogspot.in" target="_blank">Paras Sidhu</a>** has created an awesome app which can Pin a Website into Windows 7 Desktop Context menu:

<img class="alignnone  wp-image-1531" title="Web Pinner" src="http://askprateek.tk/wp-content/uploads/2012/10/Web-Pinner.jpg" alt="" width="464" height="305" /> <img class="alignnone size-full wp-image-1534" title="Web Pinner 2" src="http://askprateek.tk/wp-content/uploads/2012/10/Web-Pinner-21.jpg" alt="" width="220" height="305" />

Just fill up the setting and click apply and this tool will add that Website link in Windows 7 Desktop Context Menu.

You can Download it from the following link :

> <span style="text-decoration: underline;"><strong><a title="Download link" href="http://www.thewindowsclub.com/downloads/webpin.zip">Download WebPinner 1.1</a></strong></span>

**Also check some other apps by Paras Sidhu:**

<a title="Pulmon 2.0: Get Windows 8 Live Tiles “Start Screen” in Windows XP, Vista and 7" href="http://www.askprateek.tk/pulmon-2-0-get-windows-8-live-tiles-start-screen-in-windows-7/" rel="bookmark">Pulmon 2.0: Get Windows 8 Live Tiles “Start Screen” in Windows XP, Vista and 7</a>

<a title="Download Zuro: A Windows 8 MetroUI Based Web Browser for Windows" href="http://www.askprateek.tk/download-zuro-a-windows-8-metroui-based-web-browser-for-windows/" rel="bookmark">Download Zuro: A Windows 8 MetroUI Based Web Browser for Windows</a>