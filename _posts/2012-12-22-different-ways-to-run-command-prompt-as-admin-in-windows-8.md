---
title: Different Ways to Run Command Prompt as Admin in Windows 8
author: Ask Prateek
layout: post
permalink: /different-ways-to-run-command-prompt-as-admin-in-windows-8/
categories:
  - Troubleshooting
  - Windows 8
tags:
  - 8
  - admin
  - command
  - prompt
  - windows
---
<!--more-->

Many times we need to open **Command Prompt** window as **Administrator** in Windows which is also called as &#8220;**Elevated Command Prompt**&#8221; window. By default when you launch a Command Prompt window, it opens in a restricted mode i.e. some advanced commands can&#8217;t be executed in this mode. It happens to prevent accidental execution of some advanced commands which can interact with Windows boot loader, etc.

So, Today I will tell you different ways of opening Command Prompt as Admin in Windows 8

> <p style="text-align: center;">
>   <strong>1. [Win] + X Menu</strong>
> </p>

As we know that Micrsoft has removed the Start Button from Windows 8 and added **[Start Screen][1]**. But they have added a Context Menu which provides easier access to many useful system tools and one of them is Command Prompt

![][2]

> <p style="text-align: center;">
>   <strong>2. Search</strong>
> </p>

First open Charms Bar by pressing **[Win] + C** key and click Search. Now type **Command Prompt** in Search bar, then it will show CMD App. Right click on it then select Run as Administrator.

<img class="alignnone size-full wp-image-2149" alt="Command Prompt as Admin" src="http://www.askprateek.tk/wp-content/uploads/Command-Prompt-as-Admin1-e1356185077150.jpg" width="500" height="607" />

> <p style="text-align: center;">
>   <strong>3. Windows Explorer</strong>
> </p>

This is also a great addition in Windows 8. All you have to do is open any Explorer Windows>> Click on File>> Open Command Prompt

[<img class="alignnone size-full wp-image-2146" alt="CMD as Admin Explorer" src="http://www.askprateek.tk/wp-content/uploads/CMD-as-Admin-Explorer.jpg" width="502" height="328" />][3]

*Now That&#8217;s it for now, Hope you like it. If you know any other way to Run Command Prompt as Admin the feel free to share with us*

 [1]: http://www.askprateek.tk/pulmon-2-0-get-windows-8-live-tiles-start-screen-in-windows-7/ "Pulmon 2.0: Get Windows 8 Live Tiles “Start Screen” in Windows XP, Vista and 7"
 [2]: http://www.askprateek.tk/wp-content/uploads/2012/11/Win-+X-Menu.png
 [3]: http://www.AskPrateek.tk