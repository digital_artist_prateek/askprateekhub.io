---
title: How to Enable Build-in Facebook Messanger in Mozilla Firefox
author: Ask Prateek
layout: post
permalink: /how-to-enable-build-in-facebook-messanger-in-mozilla-firefox/
categories:
  - Mozilla Firefox
---
<!--more-->

Hello Everyone,

As you know that Mozilla is working Hard on its Web Browser FireFox. Now they have introduced Social API and now support Facebook Messenger. So, today I will tell you **How to Enable Build-in Facebook ****Messenger in Mozilla Firfox. **All you need is the Latest version of Mozilla Firefox, you can get it from here:

**<span style="text-decoration: underline;"><a title="[Mozilla Firefox Latest version Update] Firefox 13.0.1 released. Download link Inside." href="http://www.askprateek.tk/mozilla-firefox-latest-version-update-firefox-7-0-released-download-link-inside/" target="_blank">Download Latest Version of Mozilla Firefox</a></span>**

This feature it not enabled by default. It gets automatically enabled if you access these social networking websites frequently.

But if you want to use it Right now then follow these simple steps:

**1.** First Visit official Facebook page to enable this feature:

> <span style="text-decoration: underline;"><strong><a title="Facebook for Firefox" href="https://www.facebook.com/about/messenger-for-firefox" target="_blank">Visit Facebook to Turn on Facebook Messenger</a></strong></span>

Now just click on the Turn on Button to enable Facebook API:

<img class="alignnone size-full wp-image-1966" title="Firefobx API" src="http://www.askprateek.tk/wp-content/uploads/2012/11/Firefobx-APi.jpg" alt="" width="700" height="360" />

**2.** As soon as you turn the Facebook messenger on, it&#8217;ll add Facebook buttons in Firefox toolbar and Facebook chat sidebar in right-side of Firefox window as shown in following screenshot:

<img class="alignnone size-full wp-image-1969" title="FirBox Button" src="http://www.askprateek.tk/wp-content/uploads/2012/11/FirBox-Button.jpg" alt="" width="623" height="522" />

The Facebook buttons in Firefox toolbar will help you in checking your notifications, messages, etc without going to actual Facebook website.

Facebook chat sidebar will allow you chat with your friends just like you do in original Facebook website.

You can disable the Facebook chat sidebar by clicking on Facebook button present in Firefox toolbar and unchecking &#8220;**Show sidebar**&#8221; option. Also you can Remove it from Firefox via same Button

&nbsp;

&nbsp;