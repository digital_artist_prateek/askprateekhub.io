---
title: '[Windows 8 Review] All about Mircosoft new Operating System Windows 8.'
author: Ask Prateek
layout: post
permalink: /windows-8-review-all-about-mircosoft-new-operating-system-windows-8/
yourls_shorturl:
  - http://ask-pra.tk/2s
jabber_published:
  - 1321616492
  - 1321616492
  - 1321616492
  - 1321616492
  - 1321616492
  - 1321616492
  - 1321616492
  - 1321616492
email_notification:
  - 1321616501
  - 1321616501
  - 1321616501
  - 1321616501
  - 1321616501
  - 1321616501
  - 1321616501
  - 1321616501
tagazine-media:
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:6:"images";a:8:{s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";s:5:"width";s:3:"450";s:6:"height";s:3:"338";s:4:"type";s:5:"image";s:4:"area";s:6:"152100";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";s:5:"width";s:4:"1280";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:6:"983040";s:9:"file_path";s:0:"";}s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";a:6:{s:8:"file_url";s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"598";s:4:"type";s:5:"image";s:4:"area";s:6:"476008";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"639";s:4:"type";s:5:"image";s:4:"area";s:6:"508644";s:9:"file_path";s:0:"";}s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";a:6:{s:8:"file_url";s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";s:5:"width";s:3:"458";s:6:"height";s:3:"296";s:4:"type";s:5:"image";s:4:"area";s:6:"135568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"8";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-18 11:41:31";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:6:"images";a:8:{s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";s:5:"width";s:3:"450";s:6:"height";s:3:"338";s:4:"type";s:5:"image";s:4:"area";s:6:"152100";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";s:5:"width";s:4:"1280";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:6:"983040";s:9:"file_path";s:0:"";}s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";a:6:{s:8:"file_url";s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"598";s:4:"type";s:5:"image";s:4:"area";s:6:"476008";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"639";s:4:"type";s:5:"image";s:4:"area";s:6:"508644";s:9:"file_path";s:0:"";}s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";a:6:{s:8:"file_url";s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";s:5:"width";s:3:"458";s:6:"height";s:3:"296";s:4:"type";s:5:"image";s:4:"area";s:6:"135568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"8";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-18 11:41:31";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:6:"images";a:8:{s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";s:5:"width";s:3:"450";s:6:"height";s:3:"338";s:4:"type";s:5:"image";s:4:"area";s:6:"152100";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";s:5:"width";s:4:"1280";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:6:"983040";s:9:"file_path";s:0:"";}s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";a:6:{s:8:"file_url";s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"598";s:4:"type";s:5:"image";s:4:"area";s:6:"476008";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"639";s:4:"type";s:5:"image";s:4:"area";s:6:"508644";s:9:"file_path";s:0:"";}s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";a:6:{s:8:"file_url";s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";s:5:"width";s:3:"458";s:6:"height";s:3:"296";s:4:"type";s:5:"image";s:4:"area";s:6:"135568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"8";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-18 11:41:31";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:6:"images";a:8:{s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";s:5:"width";s:3:"450";s:6:"height";s:3:"338";s:4:"type";s:5:"image";s:4:"area";s:6:"152100";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";s:5:"width";s:4:"1280";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:6:"983040";s:9:"file_path";s:0:"";}s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";a:6:{s:8:"file_url";s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"598";s:4:"type";s:5:"image";s:4:"area";s:6:"476008";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"639";s:4:"type";s:5:"image";s:4:"area";s:6:"508644";s:9:"file_path";s:0:"";}s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";a:6:{s:8:"file_url";s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";s:5:"width";s:3:"458";s:6:"height";s:3:"296";s:4:"type";s:5:"image";s:4:"area";s:6:"135568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"8";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-18 11:41:31";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:6:"images";a:8:{s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";s:5:"width";s:3:"450";s:6:"height";s:3:"338";s:4:"type";s:5:"image";s:4:"area";s:6:"152100";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";s:5:"width";s:4:"1280";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:6:"983040";s:9:"file_path";s:0:"";}s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";a:6:{s:8:"file_url";s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"598";s:4:"type";s:5:"image";s:4:"area";s:6:"476008";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"639";s:4:"type";s:5:"image";s:4:"area";s:6:"508644";s:9:"file_path";s:0:"";}s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";a:6:{s:8:"file_url";s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";s:5:"width";s:3:"458";s:6:"height";s:3:"296";s:4:"type";s:5:"image";s:4:"area";s:6:"135568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"8";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-18 11:41:31";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:6:"images";a:8:{s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";s:5:"width";s:3:"450";s:6:"height";s:3:"338";s:4:"type";s:5:"image";s:4:"area";s:6:"152100";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";s:5:"width";s:4:"1280";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:6:"983040";s:9:"file_path";s:0:"";}s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";a:6:{s:8:"file_url";s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"598";s:4:"type";s:5:"image";s:4:"area";s:6:"476008";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"639";s:4:"type";s:5:"image";s:4:"area";s:6:"508644";s:9:"file_path";s:0:"";}s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";a:6:{s:8:"file_url";s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";s:5:"width";s:3:"458";s:6:"height";s:3:"296";s:4:"type";s:5:"image";s:4:"area";s:6:"135568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"8";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-18 11:41:31";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:6:"images";a:8:{s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";s:5:"width";s:3:"450";s:6:"height";s:3:"338";s:4:"type";s:5:"image";s:4:"area";s:6:"152100";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";s:5:"width";s:4:"1280";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:6:"983040";s:9:"file_path";s:0:"";}s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";a:6:{s:8:"file_url";s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"598";s:4:"type";s:5:"image";s:4:"area";s:6:"476008";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"639";s:4:"type";s:5:"image";s:4:"area";s:6:"508644";s:9:"file_path";s:0:"";}s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";a:6:{s:8:"file_url";s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";s:5:"width";s:3:"458";s:6:"height";s:3:"296";s:4:"type";s:5:"image";s:4:"area";s:6:"135568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"8";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-18 11:41:31";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:6:"images";a:8:{s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";a:6:{s:8:"file_url";s:67:"http://thewindexpl.files.wordpress.com/2011/11/win8-boot-loader.jpg";s:5:"width";s:3:"450";s:6:"height";s:3:"338";s:4:"type";s:5:"image";s:4:"area";s:6:"152100";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-logon-screen.jpg";s:5:"width";s:3:"600";s:6:"height";s:3:"449";s:4:"type";s:5:"image";s:4:"area";s:6:"269400";s:9:"file_path";s:0:"";}s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/11/win8-start-screen.jpg";s:5:"width";s:4:"1366";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:7:"1049088";s:9:"file_path";s:0:"";}s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";a:6:{s:8:"file_url";s:63:"http://thewindexpl.files.wordpress.com/2011/11/win8-desktop.jpg";s:5:"width";s:4:"1280";s:6:"height";s:3:"768";s:4:"type";s:5:"image";s:4:"area";s:6:"983040";s:9:"file_path";s:0:"";}s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";a:6:{s:8:"file_url";s:64:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"598";s:4:"type";s:5:"image";s:4:"area";s:6:"476008";s:9:"file_path";s:0:"";}s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";a:6:{s:8:"file_url";s:81:"http://thewindexpl.files.wordpress.com/2011/11/win8-explorer-without-ribbonui.jpg";s:5:"width";s:3:"796";s:6:"height";s:3:"639";s:4:"type";s:5:"image";s:4:"area";s:6:"508644";s:9:"file_path";s:0:"";}s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";a:6:{s:8:"file_url";s:74:"http://thewindexpl.files.wordpress.com/2011/11/win8-progress-dialogbox.jpg";s:5:"width";s:3:"458";s:6:"height";s:3:"296";s:4:"type";s:5:"image";s:4:"area";s:6:"135568";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"8";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-11-18 11:41:31";}'
sfw_comment_form_password:
  - pzsCNg7mrwnc
  - pzsCNg7mrwnc
categories:
  - Windows 8
tags:
  - 8
  - about
  - boot scree
  - box
  - dialog
  - logon screen
  - progress
  - review
  - ribbonui
  - start screen
  - windows
---
<address>
  <!--more-->
</address>

****As you know that Windows 8 was launched in October 2012 which is the Best OS my Microsoft for me. So, I am writing a **Windows 8 Review** on The Windows Explorer

We all know Microsoft has released a Consumer Preview build of **Windows 8** to public which can be downloaded using following link:

**<span style="text-decoration: underline;"><a title="[Direct Download Links] Windows 8 Consumer Preview (Public Beta)" href="http://www.askprateek.tk/direct-download-links-windows-8-consumer-preview-public-beta/" target="_blank">Download Windows 8 Consumer Preview Build NOW</a></span>**

Windows 8 comes with lots of new features and enhancements like Metro UI, Ribbons in Windows Explorer, Start Screen, etc.

Today in this topic, we are going to share all new features and changes with screenshots we found in **Windows 8 Pro with Media Center** while testing it . It&#8217;ll help people who can&#8217;t test Windows 8 Developer Preview build atm.

So without wasting time, lets start the screenshot tour:

> <p style="text-align: center;">
>   <strong>1. Windows 8 Boot Screen:</strong>
> </p>

Windows 8 contains a Minimal boot screen which looks cool. It a cool animated circle along with Metro Based Windows 8 Logo:

<img title="Windows 8 Boot Screen" src="http://fc08.deviantart.net/fs71/i/2012/234/1/8/windows_8_rtm_boot_screen_for_windows_7_by_vishal_gupta-d5c2ilx.gif" alt="Windows 8 Review" width="150" height="150" />

> <p style="text-align: center;">
>   <strong>2.Windows 8 Boot Loader</strong>
> </p>

Windows 8 Boot loader is now in Win 32 mode, you can also use Mouse for selecting OS. In previous versions of  Windows you have to use keyboard to select an OS but in Windows 8 you can use your Mouse.

Windows 8 BootLoader shows installed OS as tiles with a Metro Touch:

[<img class="alignnone size-full wp-image-1847" title="Boot Loader" src="http://askprateek.tk/wp-content/uploads/2011/11/Boot-Loader1.jpg" alt="" width="534" height="340" />][1]

> <p style="text-align: center;">
>   <strong>3. Windows 8 Login Screen or Welcome Screen:</strong>
> </p>

****Windows 8 Pro has a cool and minimal logon screen with Metro Touch. At the bottom it shows the Avatar of the user is a Human. We liked it a lot:

<img class="alignnone size-full wp-image-1852" title="Login Screen" src="http://askprateek.tk/wp-content/uploads/2011/11/Login-Scree.jpg" alt="" width="515" height="350" />

You can also change the background Colour of Login Screen

> <p style="text-align: center;">
>   <strong>4. Windows 8 Start screen </strong>
> </p>

After logging in in your account you will see Windows 8 Start Screen, basically it shows live tiles of currently installed programs along with other programs like Contrl Panel, Internet Explorer, Twitter and games,etc. To switch to the Desktop **click Desktop Tile or Press [Win] Ke**y.

<img class="alignnone size-full wp-image-1858" title="Start Screen" src="http://askprateek.tk/wp-content/uploads/2011/11/Screenshot-2.png" alt="" width="700" height="393" />

You can use your mouse to scroll between tiles. A scrollbar is also shown at the bottom to scroll between tiles. You can click on your username or avatar to change the avatar, to lock, log off or to add new user.

If you want to enjoy this startscreen in XP, Vista or 7 then check this Article:

<span style="text-decoration: underline;"><strong><a title="Pulmon 2.0: Get Windows 8 Live Tiles “Start Screen” in Windows XP, Vista and 7" href="http://www.askprateek.tk/pulmon-2-0-get-windows-8-live-tiles-start-screen-in-windows-7/" rel="bookmark">Pulmon 2.0: Get Windows 8 Live Tiles “Start Screen” in Windows XP, Vista and 7</a></strong></span>

> <p style="text-align: center;">
>   <strong>5. Windows 8 Desktop</strong>
> </p>

When you click on the Desktop tile on the Startscreen then it shows you the Desktop of Windows 8 Much similar to Windows 7. Also Microsoft has removed the StartMenu, Now when you hover the start button it show a different type of Menu and Today&#8217;s date, Day, etc.

<img class="alignnone size-full wp-image-1849" title="Desktop" src="http://askprateek.tk/wp-content/uploads/2011/11/Desktop.png" alt="" width="700" height="393" />

> <p style="text-align: center;">
>   <strong>6. Windows 8 Explorer</strong>
> </p>

Windows 8 Explorer is quite similar to Windows 7 Explorer. Microsoft has added the good old &#8220;**Up**&#8221; button and Office 2010 style Ribbon UI to Windows 8 Explorer which allows quick access to various system tasks. Windows Explorer in Windows 8 also shows program name and icon in titlebar. Program name is shown in the middle of titlebar.

<img class="alignnone size-full wp-image-1773" title="Explorer" src="http://askprateek.tk/wp-content/uploads/2011/11/Explorer.jpg" alt="Windows 8 Review" width="650" height="442" />

If you don&#8217;t like ribbon UI, you can minimize the ribbon by right-click on ribbon header and select minimize ribbon option. You can also put Quick Access toolbar below the ribbon to use it like a standard toolbar.

But If you like this RibbonUI, then you can also enjoy it in Windows Seven by visiting the following Article:

<span style="text-decoration: underline;"><strong><a title="BExplorer (Better Explorer): Windows 8 Look-Like Explorer with Ribbon UI for Windows 7" href="http://www.askprateek.tk/bexplorer-better-explorer-windows-8-look-like-explorer-with-ribbon-ui-for-windows-7/" target="_blank">BExplorer (Better Explorer): Windows 8 Look-Like Explorer with Ribbon UI for Windows 7</a></strong></span>

Windows 8 Explorer also comes with new improved file management UI. The copy / move dialog box has been redesigned:

&nbsp;

Now it shows pause button which means that you can also pause your current copying or moving progress just like TeraCopy.

> <p style="text-align: center;">
>   <strong>7. Windows 8 AeroLite Theme</strong>
> </p>

Microsoft has changed the Windows 8 Theme to new AeroLite Theme which looks Awesome with RibbonUI as you can see in the Screenshot:

<img class="alignnone size-full wp-image-1766" title="AeroLite Theme" src="http://askprateek.tk/wp-content/uploads/2011/11/AeroLite-Theme.jpg" alt="Windows 8 Review" width="700" height="393" />

If you like the new Theme then you can Enjoy the same look by downloading this theme for your Windows:

  * [Download Windows 8 Theme for Windows XP][2]
  * <a title="Download Windows 8 RC Theme for Windows VISTA" href="http://www.askprateek.tk/download-windows-8-rc-theme-for-windows-vista/" target="_blank">Download Windows 8 Theme for Windows Vista</a>
  * [Download Windows 8 Theme for Windows 7][3]

> <p style="text-align: center;">
>   <strong>8.Windows 8 AeroAuto Colourization</strong>
> </p>

Microsot has added Aero Auto colourization feature for Aero Themes. It automatically changes the colour and <del>Transparency</del> of Windows Border according to your current Wallpaper.

<img class="alignnone size-full wp-image-1767" title="Aero Auto Colourisation" src="http://askprateek.tk/wp-content/uploads/2011/11/Aero-Auto-Colourisation.jpg" alt="Windows 8 Review" width="689" height="338" />

> <p style="text-align: center;">
>   <strong>9. Windows 8 MetroUI Control Panel</strong>
> </p>

Windows 8 comes with 2 Control Panel, One with Metro Touch showing some Basic setting and the Traditionl one in Windows 7.

<img class="alignnone size-full wp-image-1768" title="Control Panel" src="http://askprateek.tk/wp-content/uploads/2011/11/Screenshot-4.png" alt="Windows 8 Review" width="600" height="337" />

If you want to access old **classic Control Panel**, scroll down to the bottom of new Control Panel and there you&#8217;ll have a link &#8220;**More Settings**&#8221; to old Control Panel which is similar to Windows Vista and 7 Control Panel.

> <p style="text-align: center;">
>   <strong>10. Windows 8 File Histroy Feature</strong>
> </p>

Windows 8 also comes with a new feature &#8220;**File History**&#8221; which automatically saves copies of your selected files so you can restore them if they&#8217;re lost or damaged. You can access this feature using Control Panel.

<img class="alignnone size-full wp-image-1771" title="File History" src="http://askprateek.tk/wp-content/uploads/2011/11/File-History.jpg" alt="Windows 8 Review" width="700" height="281" />

> <p style="text-align: center;">
>   <strong>11. Windows 8 Task Manager</strong>
> </p>

Windows  8 comes with newly redesigned Task Manager. First it shows only running applications but when you click More Detail button then it shows the new Task Manager.

<img class="alignnone size-full wp-image-1859" title="Task Manager" src="http://askprateek.tk/wp-content/uploads/2011/11/Task-Manager.jpg" alt="" width="661" height="580" />

It also allows you to Restart Explorer by Right clicking on the process of Windows Explorer. You can get Windows 8 TaskManager in Windows 7Via Following link:

  * <a title="How to Get Windows 8 Metro based Task Manager in Windows 7 using inbuild task Manager" href="http://www.askprateek.tk//how-to-get-windows-8-metro-based-task-manager-in-windows-7-using-inbuild-task-manager/" target="_blank">Get Windows 8 Task Manager using InBuild Feature in Windows 7</a>
  * <a href="http://www.askvg.com/get-windows-8-look-like-metro-task-manager-in-windows-xp-vista-and-7/" target="_blank">Get Windows 8 TaskBar using Application</a> (For XP, Vista and Seven)

> <p style="text-align: center;">
>   <strong>12. Windows 8 Lock Screen</strong>
> </p>

Windows 8 comes with a different Lock Screen other then Logon Screen. It shows current Date and time along with Battery and Network Status:

<img class="alignnone size-full wp-image-1769" title="Lock Screen" src="http://askprateek.tk/wp-content/uploads/2011/11/Screenshot-5.png" alt="Windows 8 Review" width="596" height="337" />

> <p style="text-align: center;">
>   <strong>13.Windows 8 New &#8220;Recovery&#8221; Feature</strong>
> </p>

An excellent new feature in Windows 8 is the &#8220;**Recovery**&#8221; feature which allows you to Refresh or Reset your PC.<img class="alignnone size-full wp-image-1770" title="Recovery" src="http://askprateek.tk/wp-content/uploads/2011/11/Recovery.jpg" alt="Windows 8 Review" width="700" height="278" />

**Refresh PC** option reloads Windows 8 without loosing your files, photos, videos, music, etc. Its like reinstalling Windows but better than it as you don&#8217;t have to worry about your documents and files.

**Reset PC** option puts your computer back to the way it was originally. Keep in mind, it&#8217;ll remove all your files. It might come handy when you want to clean your hard disk due to virus infection, etc. You can access this feature using Control Panel.

> <p style="text-align: center;">
>   <strong>14. App Store</strong>
> </p>

Microsoft has introduced the App Store in Windows 8 which have thousands of apps which are Paid Or Free. Also Microsoft has added the support of Xbox Live. Now you can also play Xbox games you PC. You can connect to Xbox live via your Xbos live ID.

<img class="alignnone size-full wp-image-1860" title="Screenshot (6)" src="http://askprateek.tk/wp-content/uploads/2011/11/Screenshot-6.png" alt="" width="700" height="393" />

Also if you have to download an app from app store you will need a Microsoft accountWe love the app store as it contains many useful apps also along with Entertainment apps like ESPN Live, WWE Universe, etc

> <p style="text-align: center;">
>   <strong>15. Windows 8 BSOD[Blue Screen of Death]</strong>
> </p>

Microsoft has replaced the classic BSOD which was full of text with a new redesigned BSOD in Windows 8 which doesn&#8217;t show advanced details but shows a sad face along with a line &#8220;*Your PC ran into a problem that it couldn&#8217;t handle, and now it needs to restart*&#8220;.The error code is also shown below the line and it suggests you to search for the error code online:![][4]

**PS:** If you don&#8217;t want to install Windows 8 in your system but want to test its new features, you can install it in a virtualization software. It&#8217;ll not install Windows 8 in your system and will not affect your already installed OS. It&#8217;ll install it virtually. Check out following article to learn how to install Windows 8 in Virtual Box or other virtualization software:

**<a title="How to install Windows 8 via VMware Workstation ?" href="http://www.askprateek.tk/how-to-install-windows-8-via-vmware-workstation/" target="_blank">How to Install Windows 8 in Virtual Box or VMware Workstation?</a>**

*Now thats it for all, hope you like our Review on Windows 8 Pro, we will soon update this article as soon we found a new Feature of Windows 8. Comments are welcome*

 [1]: http://askprateek.tk/wp-content/uploads/2011/11/Boot-Loader1.jpg
 [2]: http://www.askprateek.tk//download-best-windows-8-metroui-theme-for-windows-xp/ "Download Best Windows 8 MetroUI Theme for Windows XP."
 [3]: http://www.askprateek.tk//download-windows-8-metro-ui-theme-for-seve/ "Download Three Windows 8 Metro UI Themes for Windows Seven"
 [4]: https://fbcdn-sphotos-a-a.akamaihd.net/hphotos-ak-snc7/373781_267303729987700_463970037_n.jpg