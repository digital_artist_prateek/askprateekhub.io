---
title: '[ Dark Edition Version ] Customizing System Properties dailog Box [sysdm.cpl] in Windows XP'
author: Ask Prateek
layout: post
permalink: /dark-edition-version-customizing-system-properties-dailog-box-sysdm-cpl-in-windows-xp/
sfw_comment_form_password:
  - bhu55a7TSHjw
  - bhu55a7TSHjw
  - bhu55a7TSHjw
  - bhu55a7TSHjw
  - bhu55a7TSHjw
  - bhu55a7TSHjw
  - bhu55a7TSHjw
  - bhu55a7TSHjw
yourls_shorturl:
  - http://ask-pra.tk/l
  - http://ask-pra.tk/l
categories:
  - Resource Hacker
  - Windows XP
tags:
  - askpk123
  - box
  - cpl
  - dark
  - dialog
  - edition
  - for
  - professional
  - properties
  - sysdm
  - system
  - Xp
---
<!--more-->

> **NOTE:** If you are facing problems while saving the file after editing in resource hacker, then make sure you have disabled WFP (Windows File Protection) service using WFP Patcher.
> 
> Also if you are getting error “**Can’t create file…**“, that means you have edited and saved the file in past and there is a backup file which need to be deleted before saving this file again. Go to “**System32**” folder and you’ll see a file having “***sysdm_original.cpl***“. Delete it and try to save the file in resource hacker.

In this tutorial, we’ll tell you how to change the first tab “**General**” in System Properties dialog box of Windows XP. Following is a screenshot of the dialog box which you’ll get after following this tutorial and When You Click the Support Information Button Then you will see Right Dialog box:

![][1]

First Download All the BITMAPS and Files Needed:

> **<span style="text-decoration: underline;"><a title="Download All Resources" href="http://www.deviantart.com/download/312902596/windows_xp_dark_edition_v_7_system_properties_by_prateek_kumar-d56al6s.rar">Download Resources</a></span> | <span style="text-decoration: underline;"><a href="http://prateek-kumar.deviantart.com/art/Windows-XP-Dark-Edition-V-7-System-Properties-312902596" target="_blank">Mirror</a></span>**

  1. Now,Open **sysdm.cpl** in Resource Hacker
  2. Go to **BITMAP’S>>1** and Replace the **Bitmap** with** 1**.**bmp**  
    **BITMAP’S>>2** And Replace the **Bitmap **with** 2.bmp**  
    **BITMAP’S>>3 **and Replace the **Bitmap **with** 3.bmp**
  3. Now Go to **Dialog>>101>>1033** and paste the following code in the right side and Click **Compile Script.**

> <div>
>   101 DIALOGEX 32, 10, 261, 235
> </div>
> 
> <div>
>   STYLE DS_FIXEDSYS | DS_MODALFRAME | WS_POPUP | WS_VISIBLE | WS_CAPTION
> </div>
> 
> <div>
>   CAPTION &#8220;General&#8221;
> </div>
> 
> <div>
>   LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US
> </div>
> 
> <div>
>   FONT 8, &#8220;MS Shell Dlg&#8221;
> </div>
> 
> <div>
>   {
> </div>
> 
> <div>
>      CONTROL &#8220;System:&#8221;, 203, BUTTON, BS_GROUPBOX | WS_CHILD | WS_VISIBLE, 5, 87, 91, 63
> </div>
> 
> <div>
>      CONTROL 1, 51, STATIC, SS_BITMAP | SS_CENTERIMAGE | WS_CHILD | WS_VISIBLE, 65535, 65519, 262, 253
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 70, STATIC, SS_LEFT | WS_CHILD | WS_VISIBLE | WS_GROUP, 82, 189, 99, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 71, STATIC, SS_LEFT | WS_CHILD | WS_VISIBLE | WS_GROUP, 82, 197, 99, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 72, STATIC, SS_LEFT | WS_CHILD | WS_VISIBLE | WS_GROUP, 82, 203, 99, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 73, STATIC, SS_LEFT | WS_CHILD | WS_VISIBLE | WS_GROUP, 82, 211, 99, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 56, EDIT, ES_LEFT | ES_AUTOHSCROLL | ES_READONLY | WS_CHILD | WS_VISIBLE, 85, 191, 95, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 57, EDIT, ES_LEFT | ES_AUTOHSCROLL | ES_READONLY | WS_CHILD | WS_VISIBLE, 85, 199, 95, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 58, EDIT, ES_LEFT | ES_AUTOHSCROLL | ES_READONLY | WS_CHILD | WS_VISIBLE, 85, 207, 95, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 74, STATIC, SS_LEFT | WS_CHILD | WS_VISIBLE | WS_GROUP, 82, 110, 99, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 75, STATIC, SS_LEFT | WS_CHILD | WS_VISIBLE | WS_GROUP, 82, 118, 99, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 76, STATIC, SS_LEFT | WS_CHILD | WS_VISIBLE | WS_GROUP, 82, 126, 99, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 77, STATIC, SS_LEFT | WS_CHILD | WS_VISIBLE | WS_GROUP, 82, 134, 99, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 78, STATIC, SS_LEFT | WS_CHILD | WS_VISIBLE | WS_GROUP, 82, 142, 99, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 79, STATIC, SS_LEFT | WS_CHILD | WS_VISIBLE | WS_GROUP, 82, 147, 99, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 63, &#8220;Link Window&#8221;, 0x50000000, 85, 112, 95, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 64, &#8220;Link Window&#8221;, 0x50000000, 85, 120, 95, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 65, &#8220;Link Window&#8221;, 0x50000000, 85, 128, 95, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 66, &#8220;Link Window&#8221;, 0x50000000, 85, 136, 95, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 67, &#8220;Link Window&#8221;, 0x50000000, 85, 144, 95, 8
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 62, STATIC, SS_BITMAP | SS_CENTERIMAGE | WS_CHILD | WS_VISIBLE, 198, 13, 53, 49
> </div>
> 
> <div>
>      CONTROL &#8220;&Support Information&#8221;, 69, BUTTON, BS_PUSHBUTTON | WS_CHILD | WS_DISABLED | WS_TABSTOP, 89, 67, 80, 14
> </div>
> 
> <div>
>   }
> </div>
> 
> **  
> **

**  
**

4. Then go to **Dialog>>102>>1033 **and paste the following code in the Right Side and Click **Compile Script**.

> <div>
>   102 DIALOGEX 32, 10, 256, 256
> </div>
> 
> <div>
>   STYLE DS_FIXEDSYS | DS_MODALFRAME | WS_POPUP | WS_VISIBLE | WS_CAPTION | WS_SYSMENU
> </div>
> 
> <div>
>   CAPTION &#8220;Phone Support&#8221;
> </div>
> 
> <div>
>   LANGUAGE LANG_ENGLISH, SUBLANG_ENGLISH_US
> </div>
> 
> <div>
>   FONT 8, &#8220;MS Shell Dlg&#8221;
> </div>
> 
> <div>
>   {
> </div>
> 
> <div>
>      CONTROL 3, 51, STATIC, SS_BITMAP | SS_CENTERIMAGE | WS_CHILD | WS_VISIBLE, 65533, 65535, 263, 258
> </div>
> 
> <div>
>      CONTROL &#8220;&#8221;, 70, EDIT, ES_LEFT | ES_MULTILINE | ES_READONLY | WS_CHILD | WS_VISIBLE | WS_BORDER | WS_VSCROLL | WS_HSCROLL, 5, 69, 245, 160
> </div>
> 
> <div>
>      CONTROL &#8220;OK&#8221;, 1, BUTTON, BS_PUSHBUTTON | WS_CHILD | WS_VISIBLE | WS_TABSTOP, 197, 236, 53, 14
> </div>
> 
> <div>
>   }
> </div>

4. Notice the Support Information button and Intel Logo with My name Prateek. For this Download the 2 files  required **[OEMLogo.bmp and OEMinfo.ini]** and copy both files in **%windir%System 32.** Here  
%windir% means your Windows Folder. Both Files are Included in the download.

 [1]: http://fc08.deviantart.net/fs71/i/2012/188/6/c/windows_xp_dark_edition_v_7_system_properties_by_prateek_kumar-d56al6s.png