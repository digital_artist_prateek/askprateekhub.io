---
title: How to Customize the Command Prompt in Windows XP, Vista, 7 and 8
author: Ask Prateek
layout: post
permalink: /how-to-customize-the-command-prompt-in-windows-xp-vista-and-7/
jabber_published:
  - 1317988193
  - 1317988193
  - 1317988193
  - 1317988193
  - 1317988193
  - 1317988193
yourls_shorturl:
  - http://ask-pra.tk/1n
  - http://ask-pra.tk/1n
sfw_comment_form_password:
  - EgVAteSbDDhe
  - EgVAteSbDDhe
categories:
  - Windows 8
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - command
  - customize
  - prompt
  - seven
  - vista
  - windows
  - Xp
---
<!--more-->

> **UPDATE**: This Trick works perfectly on Windows 8

Here on The Windows Explorer we like to customize Windows by using different  software and In-build tools. Now we are posting a small article to customize Windows Command Prompt.

You can customize the appearance of a Command Prompt window, changing its size, font, and even colors. And you can save these settings independently for each shortcut that launches a Command Prompt so that you can make appropriate settings for different tasks.

To customize a Command Prompt window, you make settings in the Properties dialog box that you can reach in three ways:

  * Right-clicking on a shortcut for a Command Prompt provides the Properties option. Changes you make here affect all future Command Prompt sessions launched from that particular shortcut.
  * Clicking the Control menu icon in a Command Prompt window offers access to the Properties dialog box. (If Command Prompt is running in full-screen mode, press Alt+Enter to switch to window display.) Changes you make here affect the current session. When you leave the properties dialog box, you’ll be given the option of propagating your changes to the shortcut from which this session was launched. If you accept, all future sessions launched from that shortcut will also use the new settings.
  * Clicking the Control menu icon in a Command Prompt window and choosing Defaults from the Control menu lets you make changes that will not affect the current session but instead will affect all future sessions (except for those launched from a shortcut whose properties have been customized). These changes also affect future sessions in character-mode, MS-DOS-based applications that do not have a program-information file (PIF) and do not store their own settings.

Now you will see a Dialog Box showing the Properties of Command Prompt. Change them in a way you like. Here is a screenshot showing my settings:

<img class="size-full wp-image-1993 alignnone" title="Command Prompt Properties" src="http://www.askprateek.tk/wp-content/uploads/2011/10/Command-Prompt-Properties.png" alt="" width="386" height="475" />

<span style="text-decoration: underline; color: #000000;"><strong>After applying these colours settings We get something like this:</strong></span>

<img class="size-full wp-image-1994 alignnone" title="Command Prompt" src="http://www.askprateek.tk/wp-content/uploads/2011/10/Command-Prompt.png" alt="" width="600" height="348" />

> <span style="text-decoration: underline;"><span style="color: #000000;"><strong>Tweaking Edit Options</strong></span></span>  
> The Options tab offers a variety of options that affect how your Command Prompt window operates. Notably, you can enable or disable **QuickEdit Mode**. This option provides an easy way to copy text from (and paste text into) Command Prompt windows with a mouse. If you don’t select QuickEdit Mode, you can use commands on the Control menu for copying and pasting text. You can also enable or disable **Insert Mode**. This setting (which is on by default) allows you to insert text at the cursor position. Clearing the Insert Mode check box setting will allow you to overstrike characters instead.

<span style="text-decoration: underline;"><strong><span style="color: #000000; text-decoration: underline;">Note that when you use the third method, choosing Defaults, you also get the option to enable and disable the AutoComplete option, which is enabled by default. </span></strong></span>