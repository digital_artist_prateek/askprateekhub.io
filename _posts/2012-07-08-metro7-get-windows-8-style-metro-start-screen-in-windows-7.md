---
title: 'Metro7: Get Windows 8 Style Metro Start Screen In Windows 7'
author: Ask Prateek
layout: post
permalink: /metro7-get-windows-8-style-metro-start-screen-in-windows-7/
jabber_published:
  - 1341731073
  - 1341731073
  - 1341731073
  - 1341731073
  - 1341731073
  - 1341731073
  - 1341731073
  - 1341731073
  - 1341731073
  - 1341731073
  - 1341731073
  - 1341731073
email_notification:
  - 1341731074
  - 1341731074
  - 1341731074
  - 1341731074
  - 1341731074
  - 1341731074
  - 1341731074
  - 1341731074
  - 1341731074
  - 1341731074
  - 1341731074
  - 1341731074
sfw_comment_form_password:
  - X5YFTkiJY6HA
  - X5YFTkiJY6HA
  - X5YFTkiJY6HA
  - X5YFTkiJY6HA
yourls_shorturl:
  - http://ask-pra.tk/f
  - http://ask-pra.tk/f
  - http://ask-pra.tk/f
  - http://ask-pra.tk/f
categories:
  - Softwares
  - Windows Se7en
tags:
  - 7
  - 8
  - in
  - metro
  - screen
  - start
  - windows
---
<!--more-->

Since the release of the Windows 8 Developer’s Preview, there has been a lot of talk about the pros and cons of using the Metro UI. If you are one of those users who are thinking of sticking with Windows 7 or just cannot upgrade to Windows 8, you can still enjoy the Metro UI. **Metro7 **is a widget filled application that brings the Metro UI style interface to your Windows 7 desktop. It provides instant access to built-in widgets and a custom store from where you can install more applications. Obviously, the applications are not the ones that you would find in the original Windows 8 operating system, but some of them (like the weather app), come pretty close to providing you with the functionality of the real Metro applications.

After installation, Metro7 can be launched from a simple shortcut after which the Metro UI appears. The widgets of this UI provide time and date information, access to built-in metro store, weather application, and shortcuts to numerous websites like Facebook and Badoo. Clicking the arrow on the top right-side displays additional options, which allow you to pin any application as a metro tile, configure application settings and exit the metro interface.

<div id="single-paragraph-add">
</div>

<img src="http://cloud.addictivetips.com/wp-content/uploads/2011/12/Metro-Ui-For-Windows-7.jpg" alt="Metro Ui For Windows 7" width="660" height="404" border="0" />

To receive weather alerts, click the weather widget, enter your city name, click search and select your city’s name from the search result. Once done, you will get instant weather updates from the metro interface (as shown in the first screenshot). You can also select the time interval after which you would like to check for weather updates.

<img src="http://cloud.addictivetips.com/wp-content/uploads/2011/12/Select-City-for-weather-updates.jpg" alt="Select City for weather updates" width="662" height="379" border="0" />

Clicking the main weather widget (once it has been configured) displays a five day weather forecast for the selected city.

<img src="http://cloud.addictivetips.com/wp-content/uploads/2011/12/Forecast.png" alt="Forecast" width="660" height="346" border="0" />

Similarly, you can go to *Settings* and enable/disable full screen mode, widget animation, windows taskbar and set Metro7 to automatically start with system startup.

<img src="http://cloud.addictivetips.com/wp-content/uploads/2011/12/Settings.jpg" alt="Settings" width="662" height="375" border="0" />

To add more applications to the Metro UI, head over to the Metro Store and select an application.

<img src="http://cloud.addictivetips.com/wp-content/uploads/2011/12/Metro-Store.jpg" alt="Metro Store" width="660" height="516" border="0" />

Overall, Metro7 is a good application for emulating the Windows 8 MetroUI style interface on your Windows 7 desktop, however, it still seems to be under construction as some of the widgets simply lead to a blank page and many of the  website shortcuts on the main interface seem to be there to fill the main interface. Metro 7 has been developed for Windows 7.

> <span style="text-decoration: underline;"><strong><a href="http://www.metro7app.com/">Download Metro7</a></strong></span>