---
title: How to Manually check for updates in Google Chrome
author: Ask Prateek
layout: post
permalink: /how-to-manually-check-for-updates-in-google-chrome/
sfw_comment_form_password:
  - DVATk4uRJftO
  - DVATk4uRJftO
  - DVATk4uRJftO
  - DVATk4uRJftO
yourls_shorturl:
  - http://ask-pra.tk/1h
  - http://ask-pra.tk/1h
  - http://ask-pra.tk/1h
  - http://ask-pra.tk/1h
categories:
  - Internet
tags:
  - check
  - chrome
  - google
  - manually
  - new
  - update
  - version
  - windows
---
<!--more-->

Hello Everyone!

Today I am going to tell you how to **Update Google Chrome **manually. It’s very simple .

First click the Wrench menu and select** About Google Chrome**.

<img class="alignnone size-full wp-image-1904" title="Wrench-Menu" src="http://askprateek.tk/wp-content/uploads/2011/09/Wrench-Menu.png" alt="" width="393" height="469" />

Then Google Chrome will Check for Updates and install them Automatically if any update is available.

<img class="alignnone size-full wp-image-1908" title="Chrome Update" src="http://askprateek.tk/wp-content/uploads/2011/09/Chrome-Update.jpg" alt="" width="711" height="369" />

&nbsp;