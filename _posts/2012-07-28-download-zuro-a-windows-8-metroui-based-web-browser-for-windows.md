---
title: 'Download Zuro: A Windows 8 MetroUI Based Web Browser for Windows'
author: Ask Prateek
layout: post
permalink: /download-zuro-a-windows-8-metroui-based-web-browser-for-windows/
yourls_shorturl:
  - http://ask-pra.tk/33
jabber_published:
  - 1343489785
  - 1343489785
email_notification:
  - 1343489787
  - 1343489787
tagazine-media:
  - 'a:7:{s:7:"primary";s:55:"http://thewindexpl.files.wordpress.com/2012/07/zuro.png";s:6:"images";a:1:{s:55:"http://thewindexpl.files.wordpress.com/2012/07/zuro.png";a:6:{s:8:"file_url";s:55:"http://thewindexpl.files.wordpress.com/2012/07/zuro.png";s:5:"width";s:4:"1024";s:6:"height";s:3:"576";s:4:"type";s:5:"image";s:4:"area";s:6:"589824";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-28 15:36:24";}'
  - 'a:7:{s:7:"primary";s:55:"http://thewindexpl.files.wordpress.com/2012/07/zuro.png";s:6:"images";a:1:{s:55:"http://thewindexpl.files.wordpress.com/2012/07/zuro.png";a:6:{s:8:"file_url";s:55:"http://thewindexpl.files.wordpress.com/2012/07/zuro.png";s:5:"width";s:4:"1024";s:6:"height";s:3:"576";s:4:"type";s:5:"image";s:4:"area";s:6:"589824";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2012-07-28 15:36:24";}'
sfw_comment_form_password:
  - PnCvcBAez0gT
  - PnCvcBAez0gT
categories:
  - Softwares
tags:
  - 8
  - based
  - browser
  - download
  - metro
  - UI
  - web
  - windows
---
<!--more-->

Hello Everyone!

A you know that we love to share Windows 8 News wit you. Weather it is about a new GUI change or feature. We also posted about many Windows 8 Feature like Start Screen, Theme, Ribbon Explorer for Windows XP, Vista and Seven:

  * <a title="Pulmon 2.0: Get Windows 8 Live Tiles “Start Screen” in Windows XP, Vista and 7" href="http://www.askprateek.tk//pulmon-2-0-get-windows-8-live-tiles-start-screen-in-windows-7/" target="_blank">Pulmon 2.0: Get Windows 8 Live Tiles “Start Screen” in Windows XP, Vista and 7</a>
  * <a title="Download Windows 8 Transformation Pack for Xp, Vista, and Seven." href="http://www.askprateek.tk//download-windows-8-transformation-pack-for-xp-vista-and-seven/" target="_blank">Windows 8 Transformation Pack for Windows XP, Vista and 7.</a>
  * <a title="Download Best Windows 8 MetroUI Theme for Windows XP." href="http://www.askprateek.tk//download-best-windows-8-metroui-theme-for-windows-xp/" target="_blank">Download Windows 8 MetroUI Theme for Windows XP.</a>
  * <a title="Download Windows 8 RC Theme for Windows VISTA" href="http://www.askprateek.tk//download-windows-8-rc-theme-for-windows-vista/" target="_blank">Download Windows 8 MetroUI Theme for Windows Vista.</a>
  * <a title="Download Three Windows 8 Metro UI Themes for Windows Seven" href="http://www.askprateek.tk//download-windows-8-metro-ui-theme-for-seve/" target="_blank">Download Windows 8 MetroUI Theme for Windows 7.</a>

Now we are sharing Metro based WebBrowser created by my Friend **Paras Sidhu @<a href="http://downloadinformer.blogspot.in" target="_blank">DownloadInformer</a>. **He has done an excellent job by making this web Browser. Here is a screenshot of Zuro in action:

<img class="alignnone size-full wp-image-2290" alt="ZURO" src="http://www.askprateek.tk/wp-content/uploads/2012/07/ZURO-e1357921070522.jpg" width="700" height="394" />

<div>
  <strong> <span style="text-decoration: underline;">Features:-</span></strong>
</div>

  * Windows 8 Immersive UI and full Metro UI
  * Use low memory
  * Tab Control (Currently Limited)
  * Support Chat System
  * Support Site Icon, Change in Encryption Level, Change in Homepage
  * Very smart and easy to use
  * Support Tab preview
  * Support all extensions used in IE
  * Common Web Browser Tasks
  * Use Metro Homepages
  * Control on Size

**<span style="text-decoration: underline;">Problems :-</span>**

  * Sometimes hang to get info’s from websites
  * Restrictions on making more than 4 tabs
  * Script Errors
  * Has no Download Manager

You can download it from the following link:

> **[<span style="text-decoration: underline;">Download Zuro</span>][1] | [<span style="text-decoration: underline;">Mirror</span>][2]**

 [1]: http://dl.dropbox.com/u/14492668/Zuro%201.0.rar
 [2]: http://downloadinformer.blogspot.in/2011/08/zuro-windows-8-immersive-ui-and-metro.html "Author Site"