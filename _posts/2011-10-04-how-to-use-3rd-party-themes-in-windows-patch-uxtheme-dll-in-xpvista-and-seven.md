---
title: How to use 3rd Party Themes in Windows/ Patch UxTheme.dll in XP/Vista and Seven.
author: Ask Prateek
layout: post
permalink: /how-to-use-3rd-party-themes-in-windows-patch-uxtheme-dll-in-xpvista-and-seven/
jabber_published:
  - 1317740017
  - 1317740017
  - 1317740017
  - 1317740017
  - 1317740017
  - 1317740017
  - 1317740017
  - 1317740017
  - 1317740017
  - 1317740017
  - 1317740017
  - 1317740017
  - 1317740017
  - 1317740017
  - 1317740017
  - 1317740017
yourls_shorturl:
  - http://ask-pra.tk/23
  - http://ask-pra.tk/23
  - http://ask-pra.tk/23
  - http://ask-pra.tk/23
categories:
  - Softwares
  - Troubleshooting
tags:
  - 3rd
  - custom
  - party
  - patch
  - patcher
  - patches
  - theme
  - themes
  - tools
  - universal
  - use
  - uxtheme.dll
---
<!--more-->

Hello Everyone!

We have posted many 3rd Party Themes for windows user in the past. Here are some popular themes for you:

<span style="text-decoration:underline;"><strong><a title="Download Windows XP Dark Edition Rebirth Refix V7 Theme" href="http://www.askprateek.tk/2011/09/03/download-windows-xp-dark-edition-rebirth-refix-v7-theme/">Download Windows XP Dark Edition Rebirth Refix V7 Theme for XP.</a></strong></span>

<span style="text-decoration:underline;"><strong><a title="Download Windows 8 MetroUI Theme for Windows XP." href="http://www.askprateek.tk/2011/10/01/download-windows-8-metroui-theme-for-windows-xp/">Download Windows 8 MetroUI Theme for XP.</a></strong></span>

<span style="text-decoration:underline;"><strong><a title="Download Windows 8 Metro UI Theme for Windows Seven" href="http://www.askprateek.tk/2011/09/28/download-windows-8-metro-ui-theme-for-seve/">Download Windows 8 AeroLite Theme for Windows Seven.</a> </strong></span>

Now we a posting this tutorial about how to use these 3rd Party Themes for those users who don&#8217;t know how to use these themes. We will discuss diff. Methods for patching UxTheme.dll, you can use any one you like.

> <p style="text-align:center;">
>   <span style="color:#000000;"><strong>Method 1: Using Replacer</strong></span>
> </p>

You can patch UxTheme.dll manually by using Replacer and a Patched Uxtheme.dll file. You will need to download both the things.

<span style="text-decoration:underline;"><strong><a title="Download Replacer " href="http://www3.telus.net/_/replacer/" target="_blank">Download Replacer</a></strong></span>

We are posting some patched UxTheme.dll for some commonly used Windows Versions. If you don&#8217;t have any of these then you can download Uxtheme.dll form our &#8220;**<span style="text-decoration:underline;"><a title="UX-Theme" href="http://www.askprateek.tk/ux-theme/" target="_blank">Uxtheme</a></span>**&#8221; section. There you will find almost all versions in diff language.

> **[Windows XP SP1][1]                          [Windows Vista SP0][2]                               <a title="UX-Theme" href="http://www.askprateek.tk/ux-theme/" target="_blank">Windows 7</a>**
> 
> **[Windows XP SP2][3]                          [Windows Vista SP1][4]**
> 
> **[Windows XP SP3][5]                          [Windows Vista SP2][6] **
> 
> <span style="color:#000000;"><strong>Note:</strong> Above Patched files are of 32 bit Windows. If you are using 64-Bits Windows then you have to download it from our <span style="text-decoration:underline;"><strong><a title="UX-Theme" href="http://www.askprateek.tk/ux-theme/" target="_blank"><span style="color:#000000;text-decoration:underline;">Ux-Theme</span></a></strong></span> Section.</span>

After downloading Replacer and a patched Uxtheme.dll file for your Windows just follow these steps.

**1.** Run Replacer and drag you **original Uxtheme.dll file into Replacer Windows **and hit Enter key. Usually UxTheme.dll is Present in **<span style="color:#000000;">C:Windowssystem32. </span>**<span style="color:#000000;">This screenshot will help you more<img class="alignnone size-full wp-image-499" title="Step 1 to Rplace File" src="http://thewindexpl.files.wordpress.com/2011/10/step-1-to-rplace-file.jpg" alt="" width="600" height="288" /><br /> <strong></strong></span>

<span style="color:#000000;"><strong>2.</strong> Now you have to Drag the patched UxTheme.dll file which you have downloaded into this Window. Suppose you have saved Patched UxTheme.dll file on Desktop then after draging it into Replacer windows you will have something like this:<br /> </span>

<span style="color:#000000;"><img class="alignnone size-full wp-image-501" title="Step 2 to Replace File" src="http://thewindexpl.files.wordpress.com/2011/10/step-2-to-rplace-file.jpg" alt="" width="600" height="286" /><br /> </span>

<span style="color:#000000;">Now hit Enter and wait for the 3rd Window.<br /> <strong>3.</strong> Now you have to verify that you are Replacing correct file or not by Pressing </span>**&#8220;Y&#8221;**<span style="color:#000000;"> and then pressing Enter. If not then you can press</span>** &#8220;N&#8221;**<span style="color:#000000;"> to return back or  simply quit Replacer and start again.<br /> </span>

<span style="color:#000000;"><img class="alignnone size-full wp-image-502" title="Step 3 to Replace File" src="http://thewindexpl.files.wordpress.com/2011/10/step-3-to-rplace-file.jpg" alt="" width="600" height="287" /><br /> </span>

<span style="color:#000000;"> Now Restart your Windows and start using you custom Themes.</span>

> **Note:** Replacing system dll files can make for windows unstable so do it on your own Risk. We use this Method and it works fine if done correctly.If some thing goes Wrong then run your machine in safe mode. Now delete that Uxtheme.dll file from system32 folder and Rename **Uxtheme.backup** to **Uxtheme.dll**

&nbsp;

**  
<span style="text-decoration:underline;">Also check These free tools to Patch UxTheme.dll: </span>  
**

<span></span>

  * [UxStyle: A smal utility to patch UxTheme.dll.][7]
  * [VistaGlazz: Free tool to Patch Uxtheme.dll and AeroTheme.msstyle.][8]
  * [Universal Theme Patcher: Yet another tool to patch system files for Aero and Themes.][9]

<span></span>

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

&nbsp;

 [1]: http://www.withinwindows.com/uxthemes/Windows%20XP/SP1/6.0.2800.1106_EN.rar
 [2]: http://www.withinwindows.com/uxthemes/Windows%20Vista/SP0/6.0.6000.16386_XX.rar
 [3]: http://www.withinwindows.com/uxthemes/Windows%20XP/SP2/6.0.2900.2180_EN.rar
 [4]: http://www.withinwindows.com/uxthemes/Windows%20Vista/SP1/6.0.6001.18000_XX.rar
 [5]: http://www.withinwindows.com/uxthemes/Windows%20XP/SP3/6.0.2900.5512_EN.rar
 [6]: http://www.withinwindows.com/uxthemes/Windows%20Vista/SP2/6.0.6002.18005_XX.rar
 [7]: http://www.askprateek.tk/2011/10/04/uxstyle-a-small-utility-to-patch-uxtheme-dll-to-use-3rd-party-themes/ "UxStyle: A small utility to patch uxtheme.dll to use 3rd party Themes."
 [8]: http://www.askprateek.tk/2011/10/04/vista-glazz-a-free-tool-to-patch-uxtheme-dll-in-windows-xp-vista-and-seven/ "VistaGlazz: A free tool to patch UxTheme.dll in Windows XP, Vista and 7."
 [9]: http://www.askprateek.tk/2011/10/04/universal-theme-patcher-yet-another-tool-to-patch-system-files-to-use-themes-in-windows-xp-vista-and-7/ "Universal Theme Patcher: Yet another tool to patch system files to use themes in Windows XP, Vista and 7."