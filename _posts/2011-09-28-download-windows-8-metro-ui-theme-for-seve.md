---
title: Download Three Windows 8 Metro UI Themes for Windows Seven
author: Ask Prateek
layout: post
permalink: /download-windows-8-metro-ui-theme-for-seve/
jabber_published:
  - 1317220557
  - 1317220557
  - 1317220557
  - 1317220557
  - 1317220557
  - 1317220557
  - 1317220557
  - 1317220557
  - 1317220557
  - 1317220557
  - 1317220557
  - 1317220557
  - 1317220557
  - 1317220557
  - 1317220557
  - 1317220557
yourls_shorturl:
  - http://ask-pra.tk/x
  - http://ask-pra.tk/x
  - http://ask-pra.tk/x
  - http://ask-pra.tk/x
sfw_comment_form_password:
  - i7sM3zubpYrN
  - i7sM3zubpYrN
  - i7sM3zubpYrN
  - i7sM3zubpYrN
categories:
  - Visual Styles
  - Windows Se7en
tags:
  - 8
  - aero
  - for
  - lite
  - seven
  - theme
  - windows
---
<!--more-->

Recently we posted about the relese of Windows 8 and posted many articles on  it:

  * <a href="http://www.askprateek.tk/2011/09/23/download-windows-8-developer-preview-build-2/" rel="bookmark">[Direct and Torrent links]Download Windows 8 Developer Preview Build.</a>
  * <a href="http://www.askprateek.tk/2011/09/23/microsoft-releases-product-keys-for-windows-8-developer-preview/" rel="bookmark">Microsoft releases Product keys for Windows 8 Developer Preview.</a>
  * <a href="http://www.askprateek.tk/2011/09/23/what-are-the-system-requirement-of-windows-8/" rel="bookmark">What are the System Requirement of Windows 8?</a>

Now we are sharing  Aero Lite Windows 8 Themes for Windows Seven users.

> <p style="text-align: center;">
>   <strong>Theme 1 : AeroLite Theme by ThePanda-X</strong>
> </p>

With the Aero Lite elements leaked, hardcore Windows 8 fans went straight to work in attempts to replicate this new look in Windows 7. The theme is not yet 100% complete, but <a href="http://thepanda-x.deviantart.com/art/Aero-Lite-for-7-Update-3-200998224" target="_blank">ThePanda-X</a> from Deviantart has already incorporated many of the AeroStyle elements.

The Start Menu, context menus, breadcrumb, side-slider, progess bars, and taskbar have all been modified to look like Aero Lite.

<img src="http://th00.deviantart.net/fs70/PRE/i/2011/090/6/4/aero_lite_for_7___update_5_by_thepanda_x-d3bo380.png" alt="" width="700" height="370" />

<div>
  <blockquote>
    <p>
      <strong><a title="get it" href="http://www.deviantart.com/download/200998224/aero_lite_for_7___update_3_by_thepanda_x-d3bo380.zip">Download Windows 8 Theme For Windows Seven.</a> </strong>
    </p>
  </blockquote>
  
  <blockquote>
    <p style="text-align: center;">
      <strong>Theme 2: Aerolite Theme with Glass </strong>
    </p>
  </blockquote>
  
  <p>
    We are going to share another theme for Windows 7 created by <strong><a href="http://consume123.deviantart.com/art/Aero-Lite-Glass-209594866" target="_blank">consume123</a></strong> @ DA which provides Windows 8 look-like <strong>Aero Lite interface with Glass</strong>.
  </p>
  
  <p>
    <img src="http://th09.deviantart.net/fs70/PRE/i/2011/173/3/3/windows_8_7989_aero_for_7_1_01_by_consume123-d3gscfm.png" alt="" width="690" height="338" />
  </p>
  
  <p>
    You can Download the Theme from the Following Link:
  </p>
  
  <p>
    <span style="text-decoration: underline;"><strong><a href="http://www.deviantart.com/download/209594866/windows_8_7989_aero_for_7_1_01_by_consume123-d3gscfm.zip">Download Link </a></strong></span>
  </p>
  
  <blockquote>
    <p style="text-align: center;">
      <strong>Theme 3: Aero lite Theme by Sagorpirbd </strong>
    </p>
  </blockquote>
  
  <p>
    &#8220;<strong>Aero Lite</strong>&#8221; theme is a cool theme for Windows 7 created by &#8220;<strong><a href="http://sagorpirbd.deviantart.com/art/Aero-Lite-Beta-for-Windows-7-294638216" target="_blank">sagorpirbd</a></strong>&#8221; @ DA which brings Windows 8 &#8220;Aero Lite&#8221; look-n-feel to your Windows 7 Desktop.
  </p>
  
  <p>
    Once you apply the theme, you get a refreshing UI. Personally I liked its simplicity and clean look.
  </p>
  
  <p>
    <img class="alignnone size-full wp-image-1918" title="Windows 8 theme for 7" src="http://askprateek.tk/wp-content/uploads/2011/09/Windows-8-theme-for-7.jpg" alt="" width="673" height="421" />
  </p>
  
  <p>
    Here is the Download link:
  </p>
  
  <p>
    <a href="http://www.deviantart.com/download/294638216/aero_lite_beta_for_windows_7_by_sagorpirbd-d4vf4aw.rar"><span style="text-decoration: underline;"><strong>Download Windows 8 Aero Lite Theme for Windows Se7en</strong></span></a>
  </p>
  
  <p style="text-align: center;">
    <strong>How to install?</strong>
  </p>
  
  <p>
    First, Extract the file to <strong>%Windir%ResourcesThemes </strong>and Double click Aerolite.theme file.  Here %Windir% means your Windows Directory. Default is <strong>C:Windows.</strong>
  </p>
  
  <p>
    Before using this theme you need to patch your uxtheme.dll. These patched .dll file can be downloaded for our <span style="text-decoration: underline;"><strong><a title="UX-Theme" href="http://www.askprateek.tk/ux-theme/">uxtheme</a></strong></span> column and replace your existing .dll mile with the patched one.You will need Replacer for this work. That can be found in our <span style="text-decoration: underline;"><strong><a title="Downloads" href="http://www.askprateek.tk/downloads/">Downloads </a></strong></span>Section.
  </p>
  
  <p>
    If you don&#8217;t want to patch the file manually then you can also use <a title="Downloads" href="http://www.askprateek.tk/downloads/">Universal Theme Patcher in our DownloadSection</a> or you can use<strong> <a href="http://uxstyle.com/" target="_blank">uxstyle patcher</a></strong> by <em><a href="http://www.withinwindows.com" target="_blank">Rafael </a></em>
  </p>
</div>