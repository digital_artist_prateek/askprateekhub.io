---
title: Get Windows 8 Start Screen in Windows XP, Vista and 7 Using “Omnimo 4.1″ Rainmeter Skin Suite
author: Ask Prateek
layout: post
permalink: /get-windows-8-start-screen-in-windows-xp-vista-and-7-using-omnimo-4-1%e2%80%b3-rainmeter-skin-suite/
yourls_shorturl:
  - http://ask-pra.tk/39
  - http://ask-pra.tk/39
jabber_published:
  - 1319189523
  - 1319189523
  - 1319189523
  - 1319189523
  - 1319189523
  - 1319189523
  - 1319189523
  - 1319189523
  - 1319189523
  - 1319189523
  - 1319189523
  - 1319189523
tagazine-media:
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:6:"images";a:1:{s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:5:"width";s:3:"656";s:6:"height";s:3:"351";s:4:"type";s:5:"image";s:4:"area";s:6:"230256";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-10-21 09:31:59";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:6:"images";a:1:{s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:5:"width";s:3:"656";s:6:"height";s:3:"351";s:4:"type";s:5:"image";s:4:"area";s:6:"230256";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-10-21 09:31:59";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:6:"images";a:1:{s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:5:"width";s:3:"656";s:6:"height";s:3:"351";s:4:"type";s:5:"image";s:4:"area";s:6:"230256";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-10-21 09:31:59";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:6:"images";a:1:{s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:5:"width";s:3:"656";s:6:"height";s:3:"351";s:4:"type";s:5:"image";s:4:"area";s:6:"230256";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-10-21 09:31:59";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:6:"images";a:1:{s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:5:"width";s:3:"656";s:6:"height";s:3:"351";s:4:"type";s:5:"image";s:4:"area";s:6:"230256";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-10-21 09:31:59";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:6:"images";a:1:{s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:5:"width";s:3:"656";s:6:"height";s:3:"351";s:4:"type";s:5:"image";s:4:"area";s:6:"230256";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-10-21 09:31:59";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:6:"images";a:1:{s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:5:"width";s:3:"656";s:6:"height";s:3:"351";s:4:"type";s:5:"image";s:4:"area";s:6:"230256";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-10-21 09:31:59";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:6:"images";a:1:{s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:5:"width";s:3:"656";s:6:"height";s:3:"351";s:4:"type";s:5:"image";s:4:"area";s:6:"230256";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-10-21 09:31:59";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:6:"images";a:1:{s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:5:"width";s:3:"656";s:6:"height";s:3:"351";s:4:"type";s:5:"image";s:4:"area";s:6:"230256";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-10-21 09:31:59";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:6:"images";a:1:{s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:5:"width";s:3:"656";s:6:"height";s:3:"351";s:4:"type";s:5:"image";s:4:"area";s:6:"230256";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-10-21 09:31:59";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:6:"images";a:1:{s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:5:"width";s:3:"656";s:6:"height";s:3:"351";s:4:"type";s:5:"image";s:4:"area";s:6:"230256";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-10-21 09:31:59";}'
  - 'a:7:{s:7:"primary";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:6:"images";a:1:{s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";a:6:{s:8:"file_url";s:68:"http://thewindexpl.files.wordpress.com/2011/10/win-8-startscreen.png";s:5:"width";s:3:"656";s:6:"height";s:3:"351";s:4:"type";s:5:"image";s:4:"area";s:6:"230256";s:9:"file_path";s:0:"";}}s:6:"videos";a:0:{}s:11:"image_count";s:1:"1";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-10-21 09:31:59";}'
sfw_comment_form_password:
  - exD0dUJmF0KS
  - exD0dUJmF0KS
  - exD0dUJmF0KS
  - exD0dUJmF0KS
categories:
  - Softwares
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - 8
  - download
  - live
  - omnimo
  - rainmeter
  - seven
  - suite
  - tiles
  - vista
  - windows
  - Xp
---
<!--more-->

After the Release of Windows 8 Developer Preview, all the software Developer started making softwares of  Windows 8 like Startscreen, Theme, Live Tiles etc.

**Windows 8** features new &#8220;**Start Screen**&#8221; which looks similar to **Windows Phone 7** home screen. It basically shows live tiles of the Installed programs in Windows 8.

&#8220;**Omnimo 4.1**&#8221; is a Windows Phone 7 inspired multifunctional interactive desktop information center based on **Rainmeter**. It has been created by &#8220;**<a href="http://fediafedia.deviantart.com/art/Omnimo-4-1-for-Rainmeter-158707137" target="_blank">fediaFedia</a>**&#8221; @ DA. This suite will turn your desktop into a productive and attractive work area , delivering only the information you need. Every interactive tile gives you information at a glance, and can be easily customized to your needs.

![][1]

Now a new **version 4** has been released. In this release you&#8217;ll find that there had been many improvements in performance, visual interaction and the inner workings of the Project, as well as better customization abilities. One of the more important ones being the ability to replicate the **Windows 8 start screen** &#8211; you&#8217;ll have a screen in the intro offering to select either the traditional WP7 layout, or the new, fresh, Windows 8 one.

> <p style="text-align: center;">
>   <strong>How To Use:</strong>
> </p>

**1.** Simply download and install **Rainmeter** first using following link:

<span style="text-decoration: underline;"><strong><a href="http://rainmeter.googlecode.com/files/Rainmeter-2.0.exe" target="_blank">Download Rainmeter</a></strong></span>

**2.** Now download **Omnimo skin suite** using following link:

<span style="text-decoration: underline;"><strong><a href="http://www.deviantart.com/download/158707137/omnimo_4_1_for_rainmeter_by_fediafedia-d2mhn7l.zip" target="_blank">Download Omnimo</a></strong></span>

**3.** Double click the** Setup.rmskin** file, the extraction might take a minute or two, after that you&#8217;ll be guided by the Intro.

**4.** You can also download following 50+ panel pack to use with Omnimo:

<span style="text-decoration: underline;"><strong><a href="http://www.deviantart.com/download/213162336/full_50__panel_pack_for_omnimo_by_omnimoaddons-d3iwt40.rmskin" target="_blank">Download Omnimo Panel Pack</a></strong></span>

 [1]: http://th07.deviantart.net/fs71/PRE/f/2012/226/8/9/desktop_screenshot___nature_with_metro_touch_by_prateek_kumar-d5b1x3n.png