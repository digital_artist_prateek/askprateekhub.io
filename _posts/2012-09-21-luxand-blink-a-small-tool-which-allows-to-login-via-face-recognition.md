---
title: 'Luxand Blink! : A small tool which allows to login via Face Recognition'
author: Ask Prateek
layout: post
permalink: /luxand-blink-a-small-tool-which-allows-to-login-via-face-recognition/
yourls_shorturl:
  - http://ask-pra.tk/1
  - http://ask-pra.tk/1
categories:
  - Softwares
  - Windows 8
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - blink
  - face
  - login
  - luxand
  - recognition
  - windows
---
<!--more-->

<p style="text-align: left;">
  Have you ever imagined being able to log into your computer just by looking at the screen? This is more than just a fun gadget, however. Being able to log into your computer just by looking at it is not only faster, but it is also far more secure and convenient.
</p>

<p style="text-align: left;">
  With a traditional password login, anyone who knows the password such as by finding it out by covert means can simply log into your computer. Things such as fingerprint readers and other forms of biometric identification are practically impossible to circumvent. Many people don’t prefer fingerprint identification, however. Another form of biometric identification is that of iris scanning, but this is pretty much restricted to use in airports. It is highly expensive and requires very powerful hardware and expensive software. It is not practical for home use.
</p>

<p style="text-align: left;">
  We also posted about a great software which allows you to control Mouse Pointer with your Head or hand:
</p>

> <p style="text-align: left;">
>   <span style="text-decoration: underline;"><strong><a href="http://prat.tk/npointer-control-mouse-pointer-hand-head/">NPointer: Control your Mouse Pointer with your Hand or Head</a></strong></span>
> </p>

<p style="text-align: left;">
  Luxand Blink allows you to login to user account via Face Recognition. It&#8217;s a great software and does not need any BioMetric Hardware
</p>

![][1]

You can Download it from the following link:

<span style="text-decoration: underline;"><strong><a title="Download Link" href="http://luxand.com/download/LuxandBlinkProSetup.exe">Download Luxand Blink</a></strong></span>

<p style="text-align: left;">

 [1]: http://cdn.vikitech.netdna-cdn.com/wp-content/uploads/2010/09/LuxandBlinkSetup.png