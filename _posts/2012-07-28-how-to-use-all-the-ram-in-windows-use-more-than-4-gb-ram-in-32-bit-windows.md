---
title: How to use all the Ram in Windows/ Use more than 4 GB Ram in 32-Bit Windows?
author: Ask Prateek
layout: post
permalink: /how-to-use-all-the-ram-in-windows-use-more-than-4-gb-ram-in-32-bit-windows/
yourls_shorturl:
  - http://ask-pra.tk/2g
  - http://ask-pra.tk/2g
jabber_published:
  - 1343491615
  - 1343491615
  - 1343491615
  - 1343491615
  - 1343491615
  - 1343491615
  - 1343491615
  - 1343491615
  - 1343491615
  - 1343491615
  - 1343491615
  - 1343491615
  - 1343491615
  - 1343491615
  - 1343491615
  - 1343491615
email_notification:
  - 1343491618
  - 1343491618
  - 1343491618
  - 1343491618
  - 1343491618
  - 1343491618
  - 1343491618
  - 1343491618
  - 1343491618
  - 1343491618
  - 1343491618
  - 1343491618
  - 1343491618
  - 1343491618
  - 1343491618
  - 1343491618
sfw_comment_form_password:
  - 8tZnLBj57AHR
  - 8tZnLBj57AHR
  - 8tZnLBj57AHR
  - 8tZnLBj57AHR
categories:
  - Softwares
  - Troubleshooting
  - Windows 8
  - Windows Se7en
  - Windows Vista
  - Windows XP
tags:
  - 2g
  - 4gb
  - all
  - how
  - kernal
  - lock
  - more
  - ram
  - starter
  - than
  - the
  - to
  - unlock
  - unusable
  - use
  - vista
  - windows
  - Xp
---
<!--more-->

Hello Everyone!

Today we are going to share a small toll which allows you to use All the installed Ram in Windows. After using this tool you will be free from these Problems:

  * Unable to use more than 2GB Ram in Windows 7 Starter
  * In the 32-bit version of Windows 7 from 4 GB usually only 3.25 -3.5 GB of RAM are usable
  * Unable to use more than 4GB in 32-Bit Windows

<img src="http://www.unawave.de/medien/Windows-7/Tipps/32-Bit-RAM-Sperre/4GB-Computer-info-overwiev-Starter-unpatched.png" alt="Computer info overwiev Starter unpatched" width="290" height="210" /> <img src="http://www.unawave.de/medien/Windows-7/Tipps/32-Bit-RAM-Sperre/4GB-Computer-info-overwiev-unpatched.png" alt="Computer info overwiev unpatched" width="290" height="210" />

The small program &#8220;**4GB-RAMPatch.exe&#8221;** patches the kernel and removes the kernel lock:  
<img src="http://www.unawave.de/medien/Windows-7/Tipps/32-Bit-RAM-Sperre/4-GB-Ram-Kernel-Patch-en.png" alt="4 Gb RAM Kernel Patch" width="290" height="230" /> <img src="http://www.unawave.de/medien/Windows-7/Tipps/32-Bit-RAM-Sperre/bootmenu-en.png" alt="bootmenu" width="290" height="230" />

The program makes automatically a copy of the kernel file, then removes the lock and integrates the new kernel file as an extra boot menu entry in the Windows 7 boot menu. So then you have the option to start Windows 7, either as usual with the original kernel file or with the modified kernel file.

The patch program does not need to install &#8211; it runs as a &#8220;stand-alone&#8221; program; e.g. directly from a USB stick. And it installs no background program.

<span style="text-decoration:underline;"><span style="color:#000000;"><strong>You can Download it from the following link:</strong></span></span>

> <span style="text-decoration:underline;"><strong><a title="Download" href="http://www.mediafire.com/file/lcobb8os7lw2ew1/32-Bit_RamPatch_by_The_Windows_Explorer.zip" target="_blank">Download 32-bit Ram Patch</a> <span style="color:#000000;text-decoration:underline;">(Instructions are Included)</span></strong></span>

<span style="color:#000000;"><strong>Some Side Effects::&#8211;></strong></span>

  * Because Windows 7 observed the changes the kernel can not boot normally. For the Microsoft programmers who often times work with patched kernel Microsoft has built in a boot parameter &#8211; an additional switch: &#8220;testsigning = Yes&#8221;). So they can test their patched kernels. With this switch the patched kernel can start easily.
  * But this switch leads to the fact that on the desktop background, lower right corner (above the clock) a hint (&#8220;Watermark&#8221;) is displayed. **To remove this &#8220;watermark&#8221; you can use the button &#8220;Remove Watermark&#8221;**