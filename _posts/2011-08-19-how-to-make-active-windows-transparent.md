---
title: Windows Alpha Blender :Make Active Window in Windows Transparent.
author: Ask Prateek
layout: post
permalink: /how-to-make-active-windows-transparent/
sfw_comment_form_password:
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
  - Hub6ZBFdoKlU
yourls_shorturl:
  - http://ask-pra.tk/1p
  - http://ask-pra.tk/1p
  - http://ask-pra.tk/1p
  - http://ask-pra.tk/1p
categories:
  - Softwares
tags:
  - active
  - alpha
  - blender
  - make
  - seven
  - transparent
  - vista
  - windows
  - Xp
---
<!--more-->

Have you ever wanted to be able to see another opened window in the background while you are typing **without minimizing your active window**? This is only possible if you **apply transparency effect** on your active window.

This is exactly what **Widows Alpha Blender** does. Unzip it after the download completed and launch it immediately **without installation**. To **make your active window transparent**, just press and hold Ctrl + F12 until you get the desired transparency level. Press and hold Ctrl + F11 to do otherwise. The **shortcut keys are customizable**.

### Transparent windows in action…

![][1]

Note that you can’t make all opened windows transparent at once. The window that you can make transparent is only the active one. However, the **transparency effect** still remains even if you change focus to another window. To put it differently, you can make all of the opened window transparent, but you’ve got to do it one by one.

This freeware supports **Windows Vista and XP**.

> [Download Windows Alpha Blender][2] | <a href="http://www.vaultmate.com/freewaregifts.php" target="_blank">Mirror</a>

<span style="text-decoration: underline;"><strong>Also check</strong></span>

<span style="text-decoration: underline;"><strong> <a title="[UPDATE] TrueTransparency: Make the borders of Window in Windows XP Transparent" href="http://askprateek.tk/truetransparency-make-the-borders-of-window-in-windows-xp-transparent/">How to make active Borders of Windows Transparent via TrueTransparency</a></strong></span>

**[Windows 8 Transformation pack for Windows XP, Vista and Se7en][3]**

&nbsp;

 [1]: http://i470.photobucket.com/albums/rr70/box2009oct/AlphaBlender_Screen.jpg
 [2]: http://www.vaultmate.com/download/alphablender-installer.exe
 [3]: http://askprateek.tk/download-windows-8-transformation-pack-v4-for-windows/ "Download Windows 8 Transformation Pack V4 for Windows"