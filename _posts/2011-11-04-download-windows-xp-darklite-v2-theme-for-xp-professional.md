---
title: Download Windows XP DarkLite V2 Theme for XP Professional
author: Ask Prateek
layout: post
permalink: /download-windows-xp-darklite-v2-theme-for-xp-professional/
jabber_published:
  - 1320422289
  - 1320422289
  - 1320422289
  - 1320422289
  - 1320422289
  - 1320422289
  - 1320422289
  - 1320422289
email_notification:
  - 1320422293
  - 1320422293
  - 1320422293
  - 1320422293
  - 1320422293
  - 1320422293
  - 1320422293
  - 1320422293
yourls_shorturl:
  - http://ask-pra.tk/28
  - http://ask-pra.tk/28
categories:
  - Visual Styles
  - Windows XP
tags:
  - askpk123
  - blaitmore vs
  - darklite
  - theme
  - v2
  - windows
  - Xp
---
<!--more-->

> **Update :** If you want to completely change the GUI on Windows XP then you can also download **Windows XP DarkEdition icons** from the following **<a title="[DarkVersions] Download Windows XP DarkPK Icons for XP." href="http://www.askprateek.tk//darkversions-download-windows-xp-darkpk-icons-for-xp/" target="_blank">link</a>**

Here on The Windows Explorer we have shared many cool themes for Windows users. Some of the most popular themes of our website are:

<span style="text-decoration: underline;"><strong><a title="Download Windows XP Dark Edition Rebirth Refix V7 Theme" href="http://www.askprateek.tk//download-windows-xp-dark-edition-theme/" target="_blank">Download Windows XP Dark Edition Rebirth Refix V7 Theme for XP.</a></strong></span>

<span style="text-decoration: underline;"><strong><a title="Download the Best Windows Vista Theme “VistaVG Ultimate” for Windows XP" href="http://www.askprateek.tk//download-the-best-windows-vista-theme-vistavg-ultimate-for-windows-xp/" target="_blank">Download Best Windows Vista Theme for XP users.</a></strong></span>

<a title="Download Chelsea FC theme for Windows XP" href="http://www.askprateek.tk//download-chelsea-fc-theme-for-windows-xp/" target="_blank"><span style="text-decoration: underline;"><strong>Download Chelsea FC Theme for Windows XP. </strong></span></a>

This theme is created by** <a href="http://xyrax.deviantart.com/" target="_blank">Xyrax</a>** @DA. He has done an Excellent job by creating this Theme i.e. It is being used in Windows XP DarkLite V2.

<img class="alignnone" alt="" src="http://th09.deviantart.net/fs71/PRE/i/2011/303/b/2/windows_xp_darklite_v2_theme_by_prateekkumar-d4eg4ox.png" width="619" height="464" />

You can download this theme from the following link:

> [<span style="text-decoration: underline;"><strong>Download link </strong></span>][1]

Also Check

  * <a title="Transform Windows XP into Windows XP DarkEdition V7 without using Transformation Pack." href="http://www.askprateek.tk//transform-windows-xp-into-windows-xp-darkedition-v7-without-using-transformation-pack/" target="_blank">Transform Windows XP to XP Dark Edition without using Customization pack</a>
  * <a title="Download Windows XP DarkLite V2 Login Screen for XP" href="http://www.askprateek.tk//download-windows-xp-darklite-v2-login-screen-for-xp/" target="_blank">Download Windows XP DarkLite V2 Login Screen for XP</a>

 [1]: http://www.deviantart.com/download/130483232/Baltimore_VS_by_Xyrax.rar "Download Windows XP DarkLite V2 Theme"