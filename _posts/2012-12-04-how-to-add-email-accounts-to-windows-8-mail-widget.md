---
title: How to add Email Accounts to Windows 8 Mail Widget ?
author: Ask Prateek
layout: post
permalink: /how-to-add-email-accounts-to-windows-8-mail-widget/
categories:
  - Windows 8
tags:
  - 8
  - accounts
  - add
  - Email
  - mail
  - widget
  - windows
---
<!--more-->

Hello Everyone!

Many of you might be using Windows 8 now as Microsoft had sell more than 40 Million Windows 8 Licenses in the First Month.

For Those who are still using Windows 7, Vista OR XP and want to check that will it be Good for there PC or not, We have written 2 great Articles related to windows 8

  * [[Windows 8 Review] All about Microsoft new Cperating System Windows 8][1]
  * [[Windows 8 Features] All about Windows 8 New Additions][2]

After reading those articles you might be thinking of trying Windows 8, So here is the Download link of Windows 8 90 Days trial

<span style="text-decoration: underline;"><strong><a title="Download Windows 8 and Try it for 90 Days" href="http://www.askprateek.tk/download-windows-8-and-try-it-for-90-days/">Download Windows 8 for free [ 90 Days trial ]</a></strong></span>

Today we are here to teach you How to add E-Mail Accounts to Mail Widget of Windows 8.

<img class="alignnone size-full wp-image-2056" title="Mail Widget" src="http://www.askprateek.tk/wp-content/uploads/2012/12/Mail-Widget.png" alt="" width="248" height="120" />

**1.** First open mail widget then press [**Win] + C** or move your mouse to top Right Corner to access Charms Bar.

**2.** Now click on **Settings>>Accounts>>Add an Account.**

<img class="alignnone size-full wp-image-2057" title="3 Steps mail Account" src="http://www.askprateek.tk/wp-content/uploads/2012/12/3-Steps-mail-Account.png" alt="" width="700" height="196" />

**3.** Now Just click on the type of account you want to add like Gmail, Yahoo, Live etc and you are done.

You are facing any kind of Trouble while adding Account then here is a Video Tutorial showing how to do it:



<span style="text-decoration: underline;"><strong>Bonus Tip:</strong></span> You can also edit settings like Account name and also remove any existing account via clicking o the account name in the list shown in above screenshot. The third part.

*Now that it for now, hope this helps you and if yes then please share your Views via comment. If you are still facing any problem then feel free to comment *

 [1]: http://www.askprateek.tk/windows-8-review-all-about-mircosoft-new-operating-system-windows-8/ "[Windows 8 Review] All about Mircosoft new Operating System Windows 8."
 [2]: http://www.askprateek.tk/windows-8-features-all-about-new-additions-in-windows-8/ "[Windows 8 Features] All about new additions in Windows 8"