---
title: 'mStart Beta: Metro style based Start Menu for Windows Vista'
author: Ask Prateek
layout: post
permalink: /mstart-beta-metro-style-based-start-menu-for-windows-vista-2/
jabber_published:
  - 1319447161
  - 1319447161
yourls_shorturl:
  - http://ask-pra.tk/t
  - http://ask-pra.tk/t
categories:
  - Softwares
  - Windows Vista
tags:
  - menu
  - metro
  - mstart
  - vista
  - windows.alternative
---
<!--more-->

While browsing DeviantART we found mStart or MetroStart Menu application for Windows Vista users. This app will change the look and feel of Windows Vista start Menu.

This application is coded by &#8220;**[Solo-Dev][1]**&#8221; @ DA.

It is a Beta Version so it might have some Bugs and issues. Known Issues is the Orb may show up behind the real start menu button, if so, Ctrl+Alt+Del, Kill off Form3 in the Applications tab, and try to re-launch. there is also another Arrow Orb issue or two out there.

<img class="alignnone size-full wp-image-637" title="www.AskPK123.com" src="http://thewindexpl.files.wordpress.com/2011/10/mstart-vista.jpg" alt="" width="600" height="592" />

> **Features List:**

  * Metro style.
  * Ability to show apps in multiple folders.
  * Easy-to-use settings window.
  * Clicking the User Icon will start up Control Panel.
  * Right-Clicking a top-text link will open the actual folders.
  * Right-click Metro Start &#8220;Arrow&#8221; shows options.
  * Plenty of customizeable graphical options.
  * Drag and Drop &#8220;+&#8221; Target area.
  * Right-click an item name to add to fav link.
  * Circular display to show Loading time for items.

You can Download it from h following link

<span style="text-decoration:underline;"><strong><a href="http://www.deviantart.com/download/264001216/mstart_beta_download_by_solo_dev-d4d6gn4.zip">Download Link</a> </strong></span>| <span style="text-decoration:underline;"><strong><a href="http://fav.me/d4d6gn4" target="_blank">Mirror</a></strong></span>

 [1]: http://solo-dev.deviantart.com/