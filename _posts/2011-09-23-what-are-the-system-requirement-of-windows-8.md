---
title: What are the System Requirement of Windows 8?
author: Ask Prateek
layout: post
permalink: /what-are-the-system-requirement-of-windows-8/
jabber_published:
  - 1316792317
  - 1316792317
  - 1316792317
  - 1316792317
  - 1316792317
  - 1316792317
  - 1316792317
  - 1316792317
  - 1316792317
  - 1316792317
  - 1316792317
  - 1316792317
tagazine-media:
  - 'a:7:{s:7:"primary";s:0:"";s:6:"images";a:0:{}s:6:"videos";a:0:{}s:11:"image_count";s:1:"0";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-23 15:38:37";}'
  - 'a:7:{s:7:"primary";s:0:"";s:6:"images";a:0:{}s:6:"videos";a:0:{}s:11:"image_count";s:1:"0";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-23 15:38:37";}'
  - 'a:7:{s:7:"primary";s:0:"";s:6:"images";a:0:{}s:6:"videos";a:0:{}s:11:"image_count";s:1:"0";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-23 15:38:37";}'
  - 'a:7:{s:7:"primary";s:0:"";s:6:"images";a:0:{}s:6:"videos";a:0:{}s:11:"image_count";s:1:"0";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-23 15:38:37";}'
  - 'a:7:{s:7:"primary";s:0:"";s:6:"images";a:0:{}s:6:"videos";a:0:{}s:11:"image_count";s:1:"0";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-23 15:38:37";}'
  - 'a:7:{s:7:"primary";s:0:"";s:6:"images";a:0:{}s:6:"videos";a:0:{}s:11:"image_count";s:1:"0";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-23 15:38:37";}'
  - 'a:7:{s:7:"primary";s:0:"";s:6:"images";a:0:{}s:6:"videos";a:0:{}s:11:"image_count";s:1:"0";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-23 15:38:37";}'
  - 'a:7:{s:7:"primary";s:0:"";s:6:"images";a:0:{}s:6:"videos";a:0:{}s:11:"image_count";s:1:"0";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-23 15:38:37";}'
  - 'a:7:{s:7:"primary";s:0:"";s:6:"images";a:0:{}s:6:"videos";a:0:{}s:11:"image_count";s:1:"0";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-23 15:38:37";}'
  - 'a:7:{s:7:"primary";s:0:"";s:6:"images";a:0:{}s:6:"videos";a:0:{}s:11:"image_count";s:1:"0";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-23 15:38:37";}'
  - 'a:7:{s:7:"primary";s:0:"";s:6:"images";a:0:{}s:6:"videos";a:0:{}s:11:"image_count";s:1:"0";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-23 15:38:37";}'
  - 'a:7:{s:7:"primary";s:0:"";s:6:"images";a:0:{}s:6:"videos";a:0:{}s:11:"image_count";s:1:"0";s:6:"author";s:8:"27332830";s:7:"blog_id";s:8:"27781478";s:9:"mod_stamp";s:19:"2011-09-23 15:38:37";}'
yourls_shorturl:
  - http://ask-pra.tk/1l
  - http://ask-pra.tk/1l
  - http://ask-pra.tk/1l
  - http://ask-pra.tk/1l
categories:
  - Windows 8
tags:
  - 7
  - 8
  - as
  - hardware
  - requiremants
  - same
  - system
  - vista
  - windows
  - work
---
<!--more-->

> **Note:** Windows 8 Final Build is Released you can check **[Windows 8 Review Here][1]**

As you all know that Windows 8 Developer Preview in officially released by Microsoft and we posted Direct Download links and Torrent links for Windows 8. If you don&#8217;t khow about it then follow this Article:

<span style="text-decoration: underline;"><strong><a title="[Direct and Torrent links]Download Windows 8 Developer Preview Build." href="http://www.askprateek.tk/2011/09/23/download-windows-8-developer-preview-build-2/" target="_blank">[Direct and Torrent links]Download Windows 8 Developer Preview Build.</a> </strong></span>

But you want to know the system requirements of Windows 8. So, according to Microsoft Windows Developer Preview works great on the same hardware that powers Windows Vista and Windows 7:

  * 1 gigahertz (GHz) or faster 32-bit (x86) or 64-bit (x64) processor
  * 1 gigabyte (GB) RAM (32-bit) or 2 GB RAM (64-bit)
  * 16 GB available hard disk space (32-bit) or 20 GB (64-bit)
  * DirectX 9 graphics device with WDDM 1.0 or higher driver
  * Taking advantage of touch input requires a screen that supports multi-touch
  * To run Metro style Apps, you need a screen resolution of 1024 X 768 or greater

&nbsp;

&nbsp;

 [1]: http://askprateek.tk/windows-8-review-all-about-mircosoft-new-operating-system-windows-8/ "[Windows 8 Review] All about Mircosoft new Operating System Windows 8."