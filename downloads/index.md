---
title: Downloads
author: Ask Prateek
layout: page
sfw_comment_form_password:
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
  - Zv9uKX15P6Wb
Hide SexyBookmarks:
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
ljcustommenulinks-enabled:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
ljcustommenulinks-url:
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
  - 
sharing_disabled:
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
  - 1
socialize_text:
  - |
    <div id="presetListDiv">This article was posted by <a href="http://www.askpk123.tk">Prateek Kumar</a> in following section:  [post_list name="categories"]</div>
    <div>
    
    If you enjoyed this article, then <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Like us on Facebook</a> and <a title="Follow us on Twitter" href="http://twitter.com/AskPK123" target="_blank">Follow us on Twitter</a> so that you won't miss any of the great Article. Also you can checkout our <a title="Our DeviantART Gallery" href="http://prateek-kumar.deviantart.com/" target="_blank">DeviantART Gallery</a> and browse for some Art Work by Me.
    
    Thanks for Reading and It is requested to leave a comment if you want to Discuss something or simply say Thanks
    
    </div>
  - |
    <div id="presetListDiv">This article was posted by <a href="http://www.askpk123.tk">Prateek Kumar</a> in following section:  [post_list name="categories"]</div>
    <div>
    
    If you enjoyed this article, then <a title="Like us on Facebook" href="https://www.facebook.com/The.Windows.Explorer" target="_blank">Like us on Facebook</a> and <a title="Follow us on Twitter" href="http://twitter.com/AskPK123" target="_blank">Follow us on Twitter</a> so that you won't miss any of the great Article. Also you can checkout our <a title="Our DeviantART Gallery" href="http://prateek-kumar.deviantart.com/" target="_blank">DeviantART Gallery</a> and browse for some Art Work by Me.
    
    Thanks for Reading and It is requested to leave a comment if you want to Discuss something or simply say Thanks
    
    </div>
socialize:
  - 21
  - 21
Hide OgTags:
  - 0
---
## Collection if some useful tools which will help you in Customizing Windows.

> <p style="text-align: center;">
>   <strong>1. <strong>UxStyle</strong></strong>
> </p>

**UxStyle**&#8221; is a very small utility created by  &#8220;**Rafael**&#8221; @ <a href="http://www.withinwindows.com/2009/06/19/uxstyle-core-beta-bits-now-available/" target="_blank">Withinwindows</a>.

> UxStyle is a light-weight system service named Unsigned Themes, complimentary to the Themes service, and a kernel driver, sizing in at ~500k and ~17kb respectfully (beta builds). The service handles the enabling/disabling of custom theme support and the kernel driver handles patching. For 64-bit platforms, the kernel driver is signed with a digital certificate, as required by Microsoft.

Simply download and install the tool and get ready to enjoy 3rd party themes. **It works in Windows XP, Vista and 7**.

Note that **it doesn&#8217;t have any UI**. It&#8217;ll run as a service in background and will allow you to use 3rd party themes.

> <span style="text-decoration: underline;"><strong><a title="UxStyle: A small utility to patch uxtheme.dll to use 3rd party Themes." href="http://www.askprateek.tk/2011/10/04/uxstyle-a-small-utility-to-patch-uxtheme-dll-to-use-3rd-party-themes/">Download Link</a></strong></span>

> <p style="text-align: center;">
>   <strong>2. VistaGlazz</strong>
> </p>

Windows Vista and 7 also don’t allow 3rd party themes but you can use “VistaGlazz” to use them.

![][1]

> <span style="text-decoration: underline;"><strong><a title="VistaGlazz: A free tool to patch UxTheme.dll in Windows XP, Vista and 7." href="http://www.askprateek.tk/2011/10/04/vista-glazz-a-free-tool-to-patch-uxtheme-dll-in-windows-xp-vista-and-seven/">Download Link & How to Use</a></strong></span>

> <p style="text-align: center;">
>   <strong>3. Universal Theme Patcher</strong>
> </p>

“Universal Theme Patcher” is another excellent UxTheme Patcher which supports Windows XP SP2/SP3, Server 2003, Vista SP1/SP2, Server 2008 and Windows 7. It can be used in both 32-bit (x86) and 64-bit (x64) systems.

![][2]

> <span style="text-decoration: underline;"><strong><a title="Universal Theme Patcher: Yet another tool to patch system files to use themes in Windows XP, Vista and 7." href="http://www.askprateek.tk/2011/10/04/universal-theme-patcher-yet-another-tool-to-patch-system-files-to-use-themes-in-windows-xp-vista-and-7/">Download Link & How to Use</a></strong></span>

> <p style="text-align: center;">
>   <strong>4. Windows File Protection (WFP) Switcher</strong>
> </p>

Whenever you want to edit/delete/replace a system file in Windows XP, it doesn’t allow and shows a message that the file is already in use. Even if you replace or edit the file using some other method, Windows automatically replaces the edited file with the default one upon next reboot.  
It happens due to “Windows File Protection” (WFP) service which checks the system files periodically and replaces them with default ones.  
Windows File Protection Switcher lets you disable and re-enable WFP service so that you can easily modify system files.

> <span style="text-decoration: underline;"><strong><a href="http://fileforum.betanews.com/detail/Windows_File_Protection_Switcher/1106499902/1" target="_blank">Download here</a></strong></span>

> <p style="text-align: center;">
>   <strong>5. Replacer</strong>
> </p>

Replacer is another easy way to replace system files with your modified one. Suppose you downloaded a patched or customized system file and you need to replace existing file in your system with the new one.  
Using “Replacer”, you can do it very easily and quickly. Just drag-n-drop the default system file in its window and then drag-n-drop the new modified file. It’ll ask for confirmation, press “**Y**” and you have done. Restart your system and it’ll replace the default file with new one upon next reboot.  
PS: You can also type the complete file path in Replacer window instead of drag-n-drop the file.

> <span style="text-decoration: underline;"><strong><a title="How to use 3rd Party Themes in Windows/ Patch UxTheme.dll in XP/Vista and Seven." href="http://www.askprateek.tk/2011/10/04/how-to-use-3rd-party-themes-in-windows-patch-uxtheme-dll-in-xpvista-and-seven/" target="_blank">Download Link and How to use</a></strong></span>

> <p style="text-align: center;">
>   <strong>6. Unlocker</strong>
> </p>

Many times we face following ERROR messages while deleting or moving a system file in windows:  
=> Cannot delete file: Access is denied  
=> There has been a sharing violation.  
=> The source or destination file may be in use.  
=> The file is in use by another program or user.  
=> Make sure the disk is not full or write-protected and that the file is not currently in use.  
Unlocker is the solution of this problem. You can easily move/delete files without any problem even windows is running using unlocker.

<img class="alignnone size-full wp-image-2297" alt="Unlocker Assistant" src="http://www.askprateek.tk/wp-content/uploads/2011/08/Unlocker-Assistant-e1358066586697.png" width="600" height="437" />

> <span style="text-decoration: underline;"><strong><a href="http://www.askvg.com/download-unlocker-file-unlocking-utility-for-windows/" target="_blank">Download Link & How to Use</a></strong></span>

> <p style="text-align: center;">
>   <strong>7. Styler</strong>
> </p>

Styler is a customization utility for Windows XP. It can add drop shadow to your windows, change Clear Type font settings, change visual style’s color, change wallpaper and much more .  
Its most useful function is located in the “Styler Toolbar” which can entirely change the look and feel of Explorer toolbar. So you can change your toolbar to make it look like Windows Vista.

> <span style="text-decoration: underline;"><strong><a href="http://www.askvg.com/how-to-install-and-apply-styler-toolbar-skin-in-windows-xp-explorer/" target="_blank">Download Link & How to Use</a></strong></span>

> <p style="text-align: center;">
>   <strong>8. Resource Hacker</strong>
> </p>

Resource Hacker is a tool to edit Windows system files like .exe, .dll, .cpl, etc. You can edit these files and can change the look of various dialog boxes, menus and many other things in Windows.

<img class="alignnone  wp-image-2298" alt="Resource Hacker" src="http://www.askprateek.tk/wp-content/uploads/2011/08/Resource-Hacker.png" width="552" height="414" />

> <span style="text-decoration: underline;"><strong><a title="Download Resource Hacker" href="http://www.angusj.com/resourcehacker/" target="_blank">Download here</a></strong></span>

> <p style="text-align: center;">
>   <strong>9. Resource Tuner</strong>
> </p>

Resource Tuner is another resource editing tool like Resource hacker but it has some more advanced features which are not present in Resource Hacker. Resource Tuner can also show PNG files embedded in Windows Vista system files which can’t be seen using Resource Hacker.

> <span style="text-decoration: underline;"><strong><a href="http://www.askvg.com/resource-tuner-another-resource-editor-just-like-resource-hacker/" target="_blank">Download here</a></strong></span>

> <p style="text-align: center;">
>   <strong>10. Restorator 2007</strong>
> </p>

Restorator is an award-winning utility to edit windows resources in applications and their components, e.g. files with .exe, .dll, .res, .rc, .dcr, extension (see PE files and RES files).

> <span style="text-decoration: underline;"><strong><a href="http://www.bome.com/Restorator/download.html" target="_blank">Download here</a></strong></span>

> <p style="text-align: center;">
>   <strong>11. WinRAR</strong>
> </p>

WinRAR is a powerful archive manager. It can backup your data and reduce size of email attachments, decompress RAR, ZIP and other files downloaded from Internet and create new archives in RAR and ZIP file format.

<img class="alignnone size-full wp-image-2299" alt="WinRAR" src="http://www.askprateek.tk/wp-content/uploads/2011/08/WinRAR.png" width="671" height="465" />

> <span style="text-decoration: underline;"><strong><a href="http://www.rarlabs.com/download.htm" target="_blank">Download Here</a></strong></span>

> <p style="text-align: center;">
>   <strong>12. 7-Zip</strong>
> </p>

7-Zip is a file archiver with a high compression ratio. 7-Zip works in Windows 98/ME/NT/2000/XP/Vista/7. There is a port of the command line version to Linux/Unix.

> <span style="text-decoration: underline;"><strong><a href="http://www.7-zip.org/" target="_blank">Download Here</a></strong></span>

 [1]: http://1.bp.blogspot.com/-1V359mij7OU/T7opMWBo6UI/AAAAAAAAA2I/ju6X8lR0tk0/s1600/VistaGlazz2.png
 [2]: http://mytechquest.com/blog/wp-content/uploads/2011/08/Universal-Theme-Patcher-for-Windows.png